import 'package:flutter/material.dart';
import 'package:treatrip/ui/blog/blog_page.dart';
import 'package:treatrip/ui/post_webview.dart';
import 'package:treatrip/utils/utils.dart';

import '../provider/travel_banner_page_provider.dart';

class TravelArticleListTitle extends StatelessWidget {
  final TravelArticleListData data;

  const TravelArticleListTitle({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 8.0,
      ),
      child: GestureDetector(
        onTap: () {
          if (data.postUrl == null) {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (_) => BlogPage(postId: data.id!),
              ),
            );
          } else {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (_) => PostWebView(url: data.postUrl!),
              ),
            );
          }
        },
        child: Card(
          child: Column(
            children: [
              Image.network(
                data.imageUrl,
                fit: BoxFit.cover,
                height: 171.0,
                width: double.infinity,
              ),
              const SizedBox(height: 6.0),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                alignment: Alignment.centerLeft,
                child: Text(
                  Utils.fixTitle(data.title!),
                  style: const TextStyle(
                    color: Color(0xde000000),
                    fontWeight: FontWeight.w500,
                    fontFamily: "NotoSansCJKtc",
                    fontStyle: FontStyle.normal,
                    fontSize: 16.0,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              const SizedBox(height: 14.0),
              if (data.date != null)
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  alignment: Alignment.centerRight,
                  child: Text(
                    data.date!,
                    style: const TextStyle(
                        color: Color(0x61000000),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Roboto",
                        fontStyle: FontStyle.normal,
                        fontSize: 12.0),
                    textAlign: TextAlign.right,
                  ),
                ),
              const SizedBox(height: 18.0),
            ],
          ),
        ),
      ),
    );
  }
}
