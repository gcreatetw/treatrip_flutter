import 'package:flutter/material.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/res/resources.dart';
import '../ui/store/store_attraction_page.dart';

class StoreInformationCard extends StatelessWidget {
  final StoreInformationCardData data;

  const StoreInformationCard({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => const StoreAttractionPage(),
          ),
        );
      },
      child: Card(
        elevation: 2,
        margin: const EdgeInsets.all(2.0),
        child: Container(
          width: 124,
          height: 146,
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 12.0),
              CircleAvatar(
                backgroundColor: Colors.white,
                backgroundImage: NetworkImage(
                  data.imageUrl,
                ),
                radius: 28.0,
              ),
              const SizedBox(height: 2.0),
              Expanded(
                child: Center(
                  child: Text(
                    data.name,
                    style: const TextStyle(
                        color: Color(0xde000000),
                        fontWeight: FontWeight.w500,
                        fontFamily: "NotoSansCJKtc",
                        fontStyle: FontStyle.normal,
                        fontSize: 14.0),
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ),
              const SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    data.stars.toString(),
                    style: const TextStyle(
                      color: Color(0xffed7465),
                      fontWeight: FontWeight.w900,
                      fontFamily: "Roboto",
                      fontStyle: FontStyle.normal,
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(width: 4.0),
                  for (int index = 0; index < 5; index++)
                    if (data.stars - index > 1) ...[
                      const Icon(
                        Icons.star,
                        color: AppColors.starColor,
                        size: 10,
                      ),
                    ] else if (data.stars - index < 1 &&
                        data.stars - index > 0) ...[
                      const Icon(
                        Icons.star_half,
                        color: AppColors.starColor,
                        size: 10,
                      ),
                    ] else ...[
                      const Icon(
                        Icons.star,
                        color: Color(0x1f000000),
                        size: 10,
                      ),
                    ],
                ],
              ),
              const SizedBox(height: 12.0)
            ],
          ),
        ),
      ),
    );
  }
}
