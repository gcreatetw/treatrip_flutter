import 'package:flutter/material.dart';

Widget FloatBackButton(BuildContext context) {
  return InkWell(
    onTap: () {
      Navigator.of(context).pop();
    },
    child: Container(
      width: 24.0,
      height: 24.0,
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        color: Color.fromRGBO(0, 0, 0, 0.38),
      ),
      child: const Icon(
        Icons.arrow_back,
        size: 16.0,
        color: Colors.white,
      ),
    ),
  );
}
