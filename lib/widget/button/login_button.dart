import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

import 'app_base_button.dart';

class LoginButton extends AppBaseButton {
  final String text;
  final Widget icon;

  LoginButton(
    this.text, {
    Key? key,
    required VoidCallback? onPressed,
    required this.icon,
  }) : super(key: key, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      onPressed: onPressed,
      label: SizedBox(
        width: double.infinity,
        child: Center(
          child: Text(
            text,
            style: AppTextStyle.getDefStyle(
              color: AppColors.black38,
              fontSize: 16.0,
            ),
          ),
        ),
      ),
      icon: icon,
      style: style.login,
    );
  }
}
