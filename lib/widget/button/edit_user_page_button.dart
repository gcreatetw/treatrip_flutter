import 'package:flutter/material.dart';
import 'package:treatrip/ui/app_text.dart';

import 'app_base_button.dart';

class EditUserPageButton extends AppBaseButton {
  final String text;

  EditUserPageButton(
    this.text, {
    Key? key,
    required VoidCallback? onPressed,
  }) : super(key: key, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: AppText.primary(text),
    );
  }
}
