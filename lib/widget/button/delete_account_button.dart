import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/button/app_base_button.dart';

class DeleteAccountButton extends AppBaseButton {
  final String text;

  DeleteAccountButton(this.text, {Key? key, required VoidCallback? onPressed})
      : super(key: key, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: SizedBox(
        width: double.infinity,
        child: Center(
          child: Text(
            text,
            style: AppTextStyle.getDefStyle(
              color: AppColors.themeColor,
              fontSize: 16.0,
            ),
          ),
        ),
      ),
      style: style.login,
    );
  }
}
