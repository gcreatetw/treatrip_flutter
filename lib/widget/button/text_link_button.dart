import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

class TextLinkButton extends StatelessWidget {
  final String text;
  final GestureRecognizer onTap;

  const TextLinkButton(this.text, {Key? key, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
          text: text,
          style: const TextStyle(
            fontFamily: 'NotoSansCJKtc',
            color: AppColors.themeColor,
          ),
          recognizer: onTap),
    );
  }
}
