import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/widget/button/app_base_button.dart';

class PrimaryButton extends AppBaseButton {
  final String text;

  PrimaryButton(
    this.text, {
    Key? key,
    required VoidCallback? onPressed,
  }) : super(key: key, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: AppText.white(text),
      style: AppButtonStyle.circularPrimary(),
    );
  }
}
