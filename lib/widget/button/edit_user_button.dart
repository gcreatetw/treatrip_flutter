import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

import 'app_base_button.dart';

class EditUserButton extends AppBaseButton {
  final Widget icon;

  EditUserButton({
    Key? key,
    required VoidCallback? onPressed,
    required this.icon,
  }) : super(key: key, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return ElevatedButton(
      onPressed: onPressed,
      child: SizedBox(
        width: width,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              icon,
              Text(
                '編輯你的用戶資訊',
                style: AppTextStyle.getDefStyle(
                  color: AppColors.black38,
                  fontSize: 16.0,
                ),
              ),
            ],
          ),
        ),
      ),
      style: style.editUserInfo,
    );
  }
}
