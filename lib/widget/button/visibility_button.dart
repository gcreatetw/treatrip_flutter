import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/button/app_base_button.dart';

class VisibilityButton extends AppBaseButton {
  final bool visibility;

  VisibilityButton(
      {Key? key, required VoidCallback? onPressed, required this.visibility})
      : super(key: key, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onPressed,
      icon: Icon(!visibility ? Icons.visibility : Icons.visibility_off),
      color: AppColors.black38,
    );
  }
}
