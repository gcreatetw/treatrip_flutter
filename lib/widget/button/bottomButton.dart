import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';

import 'app_base_button.dart';

class BottomButton extends AppBaseButton {
  final String text;

  BottomButton(
    this.text, {
    Key? key,
    required VoidCallback? onPressed,
  }) : super(key: key, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: AppButtonStyle.bottomButton(context),
      child: AppText.white(text, fontSize: 13.0),
    );
  }
}
