import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

abstract class AppBaseButton extends StatelessWidget {
  final AppButtonStyle style = AppButtonStyle();
  final VoidCallback? onPressed;

  AppBaseButton({Key? key, this.onPressed}) : super(key: key);
}
