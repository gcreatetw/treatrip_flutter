import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:custom_info_window/custom_info_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:treatrip/res/resources.dart';

class GoogleMapWidget extends StatefulWidget {
  final bool searchMod;

  const GoogleMapWidget({
    Key? key,
    this.searchMod = false,
  }) : super(key: key);

  @override
  _GoogleMapWidgetState createState() => _GoogleMapWidgetState();
}

class _GoogleMapWidgetState extends State<GoogleMapWidget> {
  late BitmapDescriptor sourceIcon;

  final Set<Marker> _markers = <Marker>{};

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  final CustomInfoWindowController _customInfoWindowController =
      CustomInfoWindowController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _customInfoWindowController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        GoogleMap(
          mapType: MapType.normal,
          zoomControlsEnabled: false,
          zoomGesturesEnabled: false,
          scrollGesturesEnabled: false,
          initialCameraPosition: _kGooglePlex,
          markers: _markers,
          onMapCreated: (GoogleMapController controller) {
            _customInfoWindowController.googleMapController = controller;
            showPinsOnMap();
          },
          onTap: (position) {
            _customInfoWindowController.hideInfoWindow!();
          },
          onCameraMove: (position) {
            _customInfoWindowController.onCameraMove!();
          },
        ),
        CustomInfoWindow(
          controller: _customInfoWindowController,
          height: 40,
          offset: 35,
          width: 250,
        )
      ],
    );
  }

  Future<void> showPinsOnMap() async {
    double devicePixelRatio =
        Platform.isAndroid ? MediaQuery.of(context).devicePixelRatio : 1.0;
    BitmapDescriptor icon = await getMarkerImageFromUrl(
        'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
        targetWidth: 28 * devicePixelRatio.round());
    setState(() {
      _markers.add(
        Marker(
          markerId: const MarkerId('pin'),
          position: const LatLng(37.42796133580664, -122.085749655962),
          icon: icon,
          onTap: () {
            _customInfoWindowController.addInfoWindow!(
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Card(
                      color: Colors.white,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const SizedBox(width: 16.0, height: 40),
                          ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 180),
                            child: Text(
                              'Snail 蝸牛義大利餐廳 民生店',
                              style: TextStyle(
                                  color: Color(0xffed7465),
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "NotoSansCJKtc",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 13.0),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          const SizedBox(width: 8.0),
                          Icon(
                            Icons.arrow_forward_ios_outlined,
                            color: AppColors.themeColor,
                            size: 16.0,
                          ),
                          const SizedBox(width: 16.0),
                        ],
                      ),
                    ),
                  ],
                ),
                const LatLng(37.42796133580664, -122.085749655962));
          },
        ),
      );
    });
  }

  Future<BitmapDescriptor> getMarkerImageFromUrl(
    String url, {
    int? targetWidth,
    bool focus = false,
  }) async {
    final File markerImageFile = await DefaultCacheManager().getSingleFile(url);
    if (targetWidth != null) {
      return convertImageFileToBitmapDescriptor(markerImageFile,
          size: targetWidth);
    } else {
      Uint8List markerImageBytes = await markerImageFile.readAsBytes();
      return BitmapDescriptor.fromBytes(markerImageBytes);
    }
  }

  Future<BitmapDescriptor> convertImageFileToBitmapDescriptor(
    File imageFile, {
    int size = 80,
    bool addBorder = false,
    Color borderColor = Colors.white,
    double borderSize = 10,
    Color titleColor = Colors.transparent,
    Color titleBackgroundColor = AppColors.themeColor,
    bool focus = false,
  }) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    if (widget.searchMod && focus) {
    } else {
      size = (size * 2 / 3).round();
    }
    final double radius = size / 2;

    final borderPaint = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;

    canvas.drawCircle(Offset(radius + 2, radius + 2), radius + 2, borderPaint);

    Path borderPath = Path();

    borderPath.moveTo(0, radius + 2);
    borderPath.lineTo(radius * 2 + 4, radius + 2);
    borderPath.quadraticBezierTo(
        radius * 2 + 4, radius * 7 / 4 + 2, radius + 2, radius * 23 / 8 + 4);
    borderPath.quadraticBezierTo(0, radius * 7 / 4 + 2, 0, radius + 2);
    borderPath.close();

    canvas.drawPath(borderPath, borderPaint);

    final circlePaint = Paint()
      ..style = PaintingStyle.fill
      ..color = titleBackgroundColor;
    canvas.drawCircle(Offset(radius + 2, radius + 2), radius, circlePaint);

    Path trianglePath = Path();

    trianglePath.moveTo(2, radius + 2);
    trianglePath.lineTo(radius * 2 + 2, radius + 2);
    trianglePath.quadraticBezierTo(
        radius * 2 + 2, radius * 7 / 4 + 2, radius + 2, radius * 23 / 8 + 2);
    trianglePath.quadraticBezierTo(2, radius * 7 / 4 + 2, 2, radius + 2);
    trianglePath.close();

    canvas.drawPath(trianglePath, circlePaint);

    if (widget.searchMod && focus) {
      Path imagePath = Path()
        ..addOval(Rect.fromCircle(
            radius: radius - 5, center: Offset(radius + 2, radius + 2)));

      canvas.clipPath(imagePath);

      //paintImage
      final Uint8List imageUint8List = await imageFile.readAsBytes();
      final ui.Codec codec = await ui.instantiateImageCodec(imageUint8List);
      final ui.FrameInfo imageFI = await codec.getNextFrame();
      paintImage(
        canvas: canvas,
        rect: Rect.fromCircle(
            center: Offset(radius + 2, radius + 2), radius: radius),
        image: imageFI.image,
      );
    } else {
      final centerPaint = Paint()
        ..style = PaintingStyle.fill
        ..color = const Color.fromRGBO(129, 21, 19, 1);
      canvas.drawCircle(
          Offset(radius + 2, radius + 2), radius / 3, centerPaint);
    }

    final picture = pictureRecorder.endRecording();
    final image = await picture.toImage(
        (size + 10).round(), ((size * 23 / 16) + 10).round());
    final bytes = await image.toByteData(format: ui.ImageByteFormat.png);

    return BitmapDescriptor.fromBytes(bytes!.buffer.asUint8List());
  }
}
