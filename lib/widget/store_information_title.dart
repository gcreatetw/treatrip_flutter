import 'package:extended_image/extended_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/store/store_attraction_page.dart';
import 'package:url_launcher/url_launcher.dart';

class StoreInformationTitle extends StatelessWidget {
  final StoreInformationTitleData data;
  final bool clickable;
  final bool showStoreInformation;

  const StoreInformationTitle({
    Key? key,
    required this.data,
    this.clickable = false,
    this.showStoreInformation = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    double imageWidth = (width - 56) / 4;
    int imageCount = data.imageUrls.length <= 4 ? data.imageUrls.length : 4;
    String price = '';
    for (int i = 0; i < data.price; i++) {
      price += '\$';
    }

    return InkWell(
      splashColor: Colors.transparent,
      onTap: clickable
          ? () {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) => StoreAttractionPage(
                    postId: data.id,
                  ),
                ),
              );
            }
          : null,
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                data.title,
                style: const TextStyle(
                  color: Color(0xde000000),
                  fontWeight: FontWeight.w500,
                  fontFamily: "Roboto",
                  fontStyle: FontStyle.normal,
                  fontSize: 25.0,
                ),
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                data.address,
                style: const TextStyle(
                  color: Color(0x61000000),
                  fontWeight: FontWeight.w400,
                  fontFamily: "NotoSansCJKtc",
                  fontStyle: FontStyle.normal,
                  fontSize: 13.0,
                ),
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 13.0),
              SizedBox(
                height: imageWidth,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: imageCount,
                  itemBuilder: (_, int num) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: num == 0 ? 0 : 4, right: num == 3 ? 0 : 4),
                      child: ExtendedImage.network(
                        data.imageUrls[num],
                        width: imageWidth,
                        height: imageWidth,
                        fit: BoxFit.cover,
                        cache: false,
                        clearMemoryCacheWhenDispose: true,
                      ),
                    );
                  },
                ),
              ),
              const SizedBox(height: 10.0),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              data.star,
                              style: const TextStyle(
                                color: Color(0xffed7465),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Roboto",
                                fontStyle: FontStyle.normal,
                                fontSize: 12.0,
                              ),
                            ),
                            const SizedBox(width: 4.0),
                            for (int index = 0; index < 5; index++)
                              if (double.parse(data.star) - index > 1) ...[
                                const Icon(
                                  Icons.star,
                                  color: AppColors.starColor,
                                  size: 10,
                                ),
                              ] else if (double.parse(data.star) - index < 1 &&
                                  double.parse(data.star) - index > 0) ...[
                                const Icon(
                                  Icons.star_half,
                                  color: AppColors.starColor,
                                  size: 10,
                                ),
                              ] else ...[
                                const Icon(
                                  Icons.star,
                                  color: Color(0x1f000000),
                                  size: 10,
                                ),
                              ],
                            const SizedBox(width: 5.0),
                            Text(
                              '(${data.comments}) ${String.fromCharCode(0x00B7)} $price',
                              style: const TextStyle(
                                color: Color(0x61000000),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Roboto",
                                fontStyle: FontStyle.normal,
                                fontSize: 12.0,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          data.type,
                          style: const TextStyle(
                            color: Color(0x61000000),
                            fontWeight: FontWeight.w500,
                            fontFamily: "Roboto",
                            fontStyle: FontStyle.normal,
                            fontSize: 12.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      launch(
                          'https://www.google.com/maps/dir//${Uri.parse(data.address)}');
                    },
                    child: const Icon(
                      Icons.directions_outlined,
                      color: AppColors.themeColor,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 19.0),
              if (showStoreInformation)
                GestureDetector(
                  onTap: () {},
                  child: const Text(
                    '查看店家資訊',
                    style: TextStyle(
                      color: Color(0xffed7465),
                      fontWeight: FontWeight.w500,
                      fontFamily: "NotoSansCJKtc",
                      fontStyle: FontStyle.normal,
                      fontSize: 15.0,
                    ),
                  ),
                ),
              const SizedBox(height: 18.0)
            ],
          ),
        ),
      ),
    );
  }
}
