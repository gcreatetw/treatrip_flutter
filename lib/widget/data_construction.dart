
import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

class DataConstruction extends StatelessWidget {
  const DataConstruction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const <Widget>[
        Expanded(
          child: Center(
            child: Text(
              '資料建置中，敬請期待!!',
              style: TextStyle(
                color: AppColors.themeColor,
              ),
            ),
          ),
        ),
        Text(
          '趣趣',
          style: TextStyle(color: AppColors.themeColor),
        ),
        SizedBox(height: 9.0),
        Text(
          '© Copyright 2020 TRETRIP. All rights reserved.',
          style: TextStyle(
            color: Color(0x61000000),
            fontWeight: FontWeight.w400,
            fontFamily: "Roboto",
            fontStyle: FontStyle.normal,
            fontSize: 8.0,
          ),
        ),
        SizedBox(
          height: 24.0,
        ),
      ],
    );
  }
}
