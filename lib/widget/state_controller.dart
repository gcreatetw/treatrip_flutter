import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

enum GeneralState {
  loading,
  finish,
  error,
}

class StateController extends StatelessWidget {
  final Widget child;
  final GeneralState state;

  const StateController({
    Key? key,
    required this.state,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (state) {
      case GeneralState.finish:
        return child;
      case GeneralState.error:
        return const Center(
          child: Text('Something went wrong...'),
        );
      case GeneralState.loading:
      default:
        return const Center(
          child: CircularProgressIndicator(
            color: AppColors.themeColor,
          ),
        );
    }
  }
}
