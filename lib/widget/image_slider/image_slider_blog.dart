import 'dart:async';

import 'package:extended_text/extended_text.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/models/image_slider_data.dart';
import 'package:treatrip/ui/blog/blog_page.dart';
import 'package:treatrip/utils/utils.dart';

class ImageSliderBlog extends StatefulWidget {
  final List<ImageSliderBlogData> data;
  final double height;
  final Color dotSelectedColor;
  final Color dotColor;
  final double selectOpacity;
  final double unSelectedOpacity;
  final double dotSize;
  final double dotPadding;
  final bool? auto;
  final int? speed;
  final bool short;
  final bool? isCard;

  const ImageSliderBlog({
    Key? key,
    required this.height,
    required this.dotSelectedColor,
    required this.dotColor,
    required this.selectOpacity,
    required this.unSelectedOpacity,
    required this.dotSize,
    required this.dotPadding,
    this.auto,
    this.speed,
    required this.data,
    this.short = false,
    this.isCard = false,
  }) : super(key: key);

  @override
  _ImageSliderBlogState createState() => _ImageSliderBlogState();
}

class _ImageSliderBlogState extends State<ImageSliderBlog> {
  ///目前slider位置
  int currentIndex = 0;

  ///實際的位置
  ///設置1000確保可以往左滑
  int actualIndex = 1000;

  ///控制pageView位置
  ///initialPage : 初始位置與actualIndex相同
  PageController pageController = PageController(initialPage: 1000);

  @override
  void initState() {
    ///計時器
    if (widget.auto ?? false) {
      startTimer();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double imageWidth = (widget.height - 56) / 4;
    double bannerHeight = 0;
    if (widget.data[0].imageUrl != null) {
      bannerHeight = 171.0;
    }
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          ///imageSlider上方圖片
          SizedBox(
            height: widget.short
                ? 140 + imageWidth + bannerHeight
                : 190 + imageWidth + bannerHeight,
            child: _getPageView(imageWidth),
          ),

          ///下方點
          const SizedBox(height: 8.0),
          _getIndicator(),
        ],
      ),
    );
  }

  ///建立下方點
  Widget _getIndicator() {
    return Indicator(
      currentIndex: currentIndex,
      dotCount: widget.data.length,
      onItemTap: (index) {
        pageController.animateToPage(index,
            duration: const Duration(milliseconds: 300), curve: Curves.ease);
      },
      dotColor: widget.dotColor,
      dotSelectedColor: widget.dotSelectedColor,
      dotSize: widget.dotSize,
      dotPadding: widget.dotPadding,
      selectOpacity: widget.selectOpacity,
      unSelectedOpacity: widget.unSelectedOpacity,
      actualIndex: actualIndex,
    );
  }

  ///建立imageSlider畫面
  _getPageView(double imageWidth) {
    return PageView.builder(
      itemBuilder: (BuildContext context, int index) {
        int imageCount =
            widget.data[index % widget.data.length].imagePaths.length <= 4
                ? widget.data[index % widget.data.length].imagePaths.length
                : 4;
        return InkWell(
          splashColor: Colors.transparent,
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                //TODO postId from API
                builder: (_) => BlogPage(postId: 43381),
              ),
            );
          },
          child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: widget.isCard! ? 8.0 : 16.0),
            child: Card(
              margin: const EdgeInsets.only(bottom: 2),
              elevation: widget.isCard! ? 2 : 0,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.isCard == true) const SizedBox(height: 7.0),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: widget.isCard! ? 8.0 : 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          backgroundImage: NetworkImage(widget
                              .data[index % widget.data.length].profileUrl),
                          radius: 12.0,
                        ),
                        const SizedBox(width: 8.0),
                        Text(
                          widget.data[index % widget.data.length].name,
                          style: const TextStyle(
                              color: Color(0xde000000),
                              fontWeight: FontWeight.w500,
                              fontFamily: "NotoSansCJKtc",
                              fontStyle: FontStyle.normal,
                              fontSize: 15.0),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  if (widget.data[index % widget.data.length].title != null &&
                      widget.data[index % widget.data.length].imageUrl == null)
                    Text(
                      Utils.fixTitle(
                          widget.data[index % widget.data.length].title!),
                      style: const TextStyle(
                        color: Color(0xde000000),
                        fontWeight: FontWeight.w500,
                        fontFamily: "PingFangTC",
                        fontStyle: FontStyle.normal,
                        fontSize: 25.0,
                      ),
                    ),
                  if (widget.data[index % widget.data.length].imageUrl !=
                          null &&
                      widget.data[index % widget.data.length].title != null)
                    Stack(
                      children: [
                        Image.network(
                          widget.data[index % widget.data.length].imageUrl!,
                          width: double.maxFinite,
                          height: 171,
                          fit: BoxFit.fitWidth,
                        ),
                        Positioned(
                          left: 8.0,
                          bottom: 8.0,
                          child: Text(
                            Utils.fixTitle(
                                widget.data[index % widget.data.length].title!),
                            style: const TextStyle(
                              color: Color(0xdeffffff),
                              fontWeight: FontWeight.w700,
                              fontFamily: "NotoSansCJKtc",
                              fontStyle: FontStyle.normal,
                              fontSize: 21.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  if (widget.data[index % widget.data.length].address != null)
                    Text(
                      widget.data[index % widget.data.length].address!,
                      style: const TextStyle(
                        color: Color(0x61000000),
                        fontWeight: FontWeight.w400,
                        fontFamily: "NotoSansCJKtc",
                        fontStyle: FontStyle.normal,
                        fontSize: 13.0,
                      ),
                    ),
                  const SizedBox(height: 9.0),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: widget.isCard! ? 8.0 : 0),
                    child: ExtendedText(
                      Utils.fixTitle(
                          widget.data[index % widget.data.length].content),
                      maxLines: 3,
                      style: const TextStyle(
                          color: Color(0xde000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "NotoSansCJKtc",
                          fontStyle: FontStyle.normal,
                          fontSize: 15.0),
                      overflowWidget: const TextOverflowWidget(
                        position: TextOverflowPosition.end,
                        child: Text(
                          '...查看更多',
                          style: TextStyle(
                            color: Color(0x5c000000),
                            fontWeight: FontWeight.w400,
                            fontFamily: "NotoSansCJKtc",
                            fontStyle: FontStyle.normal,
                            fontSize: 15.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: widget.isCard! ? 8.0 : 0),
                    child: SizedBox(
                      height: imageWidth,
                      child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: imageCount,
                        itemBuilder: (_, int num) {
                          return Padding(
                            padding: EdgeInsets.only(
                                left: num == 0 ? 0 : 4,
                                right: num == 3 ? 0 : 4),
                            child: Image.network(
                              widget.data[index % widget.data.length]
                                  .imagePaths[num],
                              width: imageWidth,
                              height: imageWidth,
                              fit: BoxFit.cover,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
      onPageChanged: (index) {
        setState(() {
          currentIndex = index % widget.data.length;
          actualIndex = index;
        });
      },
      controller: pageController,
    );
  }

  void startTimer() {
    ///設定間隔時間
    Timer.periodic(Duration(milliseconds: widget.speed ?? 3000), (value) {
      actualIndex++;

      ///觸發輪播切換
      pageController.animateToPage(actualIndex,
          duration: const Duration(milliseconds: 300), curve: Curves.ease);
    });
  }
}

///下方點的外觀
class Indicator extends StatelessWidget {
  final int currentIndex;
  final int dotCount;
  final ValueChanged onItemTap;
  final Color dotColor;
  final Color dotSelectedColor;
  final double dotSize;
  final double dotPadding;
  final double selectOpacity;
  final double unSelectedOpacity;
  final int actualIndex;

  const Indicator({
    Key? key,
    required this.currentIndex,
    required this.dotCount,
    required this.onItemTap,
    required this.dotColor,
    required this.dotSelectedColor,
    required this.dotSize,
    required this.dotPadding,
    required this.selectOpacity,
    required this.unSelectedOpacity,
    required this.actualIndex,
  }) : super(key: key);

  Widget _renderItem(int index) {
    bool isSelected = currentIndex == index ? true : false;

    Color color = isSelected ? dotSelectedColor : dotColor;

    BoxShape boxShape = BoxShape.circle;

    double opacity = isSelected ? selectOpacity : unSelectedOpacity;

    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: dotPadding),
        child: Opacity(
          opacity: opacity,
          child: Container(
            width: dotSize,
            height: dotSize,
            decoration: BoxDecoration(
              shape: boxShape,
              color: color,
            ),
          ),
        ),
      ),
      onTap: () {
        onItemTap(actualIndex + index - currentIndex);
      },
    );
  }

  ///計算indicator寬度
  double getWidth() {
    return dotSize * (dotCount) + dotPadding * (dotCount * 2);
  }

  ///計算indicator高度
  double getHeight() {
    return dotSize;
  }

  ///建立indicator陣列
  Widget _getItems() {
    return SizedBox(
      child: ListView.builder(
        itemCount: dotCount,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return _renderItem(index);
        },
      ),
      width: getWidth(),
      height: getHeight(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ///背景
        Container(
          height: getHeight(),
          color: const Color.fromRGBO(0, 0, 0, 0),
        ),

        ///indicator
        Container(
          child: _getItems(),
          alignment: Alignment.center,
        )
      ],
    );
  }
}
