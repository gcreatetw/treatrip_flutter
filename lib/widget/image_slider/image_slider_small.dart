import 'dart:async';

import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/models/image_slider_data.dart';
import 'package:treatrip/ui/blog/blog_page.dart';
import 'package:treatrip/ui/post_webview.dart';
import 'package:treatrip/utils/utils.dart';

class ImageSliderSmall extends StatefulWidget {
  final String? title;
  final List<ImageSliderSmallData> data;
  final double height;
  final Color dotSelectedColor;
  final Color dotColor;
  final double selectOpacity;
  final double unSelectedOpacity;
  final double dotSize;
  final double dotPadding;
  final bool? auto;
  final int? speed;

  const ImageSliderSmall({
    Key? key,
    required this.height,
    required this.dotSelectedColor,
    required this.dotColor,
    required this.selectOpacity,
    required this.unSelectedOpacity,
    required this.dotSize,
    required this.dotPadding,
    this.auto,
    this.speed,
    required this.data,
    this.title,
  }) : super(key: key);

  @override
  _ImageSliderSmallState createState() => _ImageSliderSmallState();
}

class _ImageSliderSmallState extends State<ImageSliderSmall> {
  ///目前slider位置
  int currentIndex = 0;

  ///實際的位置
  ///設置1000確保可以往左滑
  int actualIndex = 1000;

  ///控制pageView位置
  ///initialPage : 初始位置與actualIndex相同
  PageController pageController = PageController(initialPage: 1000);

  @override
  void initState() {
    ///計時器
    if (widget.auto ?? false) {
      startTimer();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ///imageSlider上方圖片
          SizedBox(
            height: 235,
            child: _getPageView(),
          ),

          ///下方點
          _getIndicator(),
        ],
      ),
    );
  }

  ///建立下方點
  Widget _getIndicator() {
    return Indicator(
      currentIndex: currentIndex,
      dotCount: widget.data.length,
      onItemTap: (index) {
        pageController.animateToPage(index,
            duration: const Duration(milliseconds: 300), curve: Curves.ease);
      },
      dotColor: widget.dotColor,
      dotSelectedColor: widget.dotSelectedColor,
      dotSize: widget.dotSize,
      dotPadding: widget.dotPadding,
      selectOpacity: widget.selectOpacity,
      unSelectedOpacity: widget.unSelectedOpacity,
      actualIndex: actualIndex,
    );
  }

  ///建立imageSlider畫面
  _getPageView() {
    return PageView.builder(
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          splashColor: Colors.transparent,
          onTap: () {
            if (widget.data[currentIndex].link == null) {
              print('0623 = BlogPage');
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) =>
                      BlogPage(postId: widget.data[currentIndex].id),
                ),
              );
            } else {
              print('0623 = PostWebView');
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) =>
                      PostWebView(url: widget.data[currentIndex].link!),
                ),
              );
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: widget.height,
                  height: 171,
                  child: ExtendedImage.network(
                    widget.data.isEmpty ? '':
                    widget.data[index % widget.data.length].imgUrl,
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(height: 8.0),
                Text(
                  widget.data.isEmpty ? '':
                  Utils.fixTitle(widget.data[index % widget.data.length].text),
                  style: const TextStyle(
                    color: Color(0xde000000),
                    fontWeight: FontWeight.w500,
                    fontFamily: "NotoSansCJKtc",
                    fontStyle: FontStyle.normal,
                    fontSize: 15.0,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ],
            ),
          ),
        );
      },
      onPageChanged: (index) {
        setState(() {
          currentIndex = index % widget.data.length;
          actualIndex = index;
        });
      },
      controller: pageController,
    );
  }

  void startTimer() {
    ///設定間隔時間
    Timer.periodic(Duration(milliseconds: widget.speed ?? 3000), (value) {
      actualIndex++;

      ///觸發輪播切換
      pageController.animateToPage(actualIndex,
          duration: const Duration(milliseconds: 300), curve: Curves.ease);
    });
  }
}

///下方點的外觀
class Indicator extends StatelessWidget {
  final int currentIndex;
  final int dotCount;
  final ValueChanged onItemTap;
  final Color dotColor;
  final Color dotSelectedColor;
  final double dotSize;
  final double dotPadding;
  final double selectOpacity;
  final double unSelectedOpacity;
  final int actualIndex;

  const Indicator({
    Key? key,
    required this.currentIndex,
    required this.dotCount,
    required this.onItemTap,
    required this.dotColor,
    required this.dotSelectedColor,
    required this.dotSize,
    required this.dotPadding,
    required this.selectOpacity,
    required this.unSelectedOpacity,
    required this.actualIndex,
  }) : super(key: key);

  Widget _renderItem(int index) {
    bool isSelected = currentIndex == index ? true : false;

    Color color = isSelected ? dotSelectedColor : dotColor;

    BoxShape boxShape = BoxShape.circle;

    double padding = dotPadding;

    double opacity = isSelected ? selectOpacity : unSelectedOpacity;

    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.fromLTRB(padding, 0, padding, 0),
        child: Opacity(
          opacity: opacity,
          child: Container(
            width: dotSize,
            height: dotSize,
            decoration: BoxDecoration(
              shape: boxShape,
              color: color,
            ),
          ),
        ),
      ),
      onTap: () {
        onItemTap(actualIndex + index - currentIndex);
      },
    );
  }

  ///計算indicator寬度
  double getWidth() {
    return dotSize * (dotCount) + dotPadding * (dotCount * 2);
  }

  ///計算indicator高度
  double getHeight() {
    return dotSize;
  }

  ///建立indicator陣列
  Widget _getItems() {
    return SizedBox(
      child: ListView.builder(
        itemCount: dotCount,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return _renderItem(index);
        },
      ),
      width: getWidth(),
      height: getHeight(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ///背景
        Container(
          height: getHeight(),
          color: const Color.fromRGBO(0, 0, 0, 0),
        ),

        ///indicator
        Container(
          child: _getItems(),
          alignment: Alignment.center,
        )
      ],
    );
  }
}
