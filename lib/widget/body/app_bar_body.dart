import 'package:flutter/material.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/utils/preferences.dart';

class AppBarBody extends StatelessWidget {
  final String title;
  final Widget body;
  final Widget? leading;
  final List<Widget>? actions;

  const AppBarBody(
      {Key? key,
      required this.title,
      required this.body,
      this.actions,
      this.leading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus!.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          iconTheme: const IconThemeData(color: AppColors.themeColor),
          automaticallyImplyLeading:
              Preferences.getBool(Constants.isSkipLogin, true),
          title: AppText.carolinaCoral(title),
          backgroundColor: AppColors.white,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: actions,
          elevation: 0,
        ),
        body: body,
      ),
    );
  }
}
