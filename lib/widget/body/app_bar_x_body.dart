import 'package:flutter/material.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/utils/preferences.dart';

class AppBarXBody extends StatelessWidget {
  final String title;
  final Widget? body;
  final List<Widget>? actions;

  const AppBarXBody({Key? key, required this.title, this.body, this.actions})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus!.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          iconTheme: const IconThemeData(color: AppColors.themeColor),
          automaticallyImplyLeading:
              Preferences.getBool(Constants.isSkipLogin, true),
          title: AppText.carolinaCoral(title),
          backgroundColor: AppColors.white,
          leading: IconButton(
            icon: const Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: actions,
          elevation: 0,
        ),
        body: body,
      ),
    );
  }
}
