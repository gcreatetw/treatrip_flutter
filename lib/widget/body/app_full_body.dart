import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';

class AppFullBody extends StatelessWidget {
  final String title;
  final Widget body;
  List<Widget>? actions;

  AppFullBody({
    Key? key,
    required this.title,
    required this.body,
    this.actions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus!.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: AppText.carolinaCoral(title),
          backgroundColor: Colors.transparent,
          actions: actions,
          elevation: 0,
          centerTitle: false,
          // leading: IconButton(
          //   icon: Icon(
          //     Icons.add_sharp,
          //     color: AppColors.themeColor,
          //   ),
          //   onPressed: () {},
          // ),
        ),
        body: body,
      ),
    );
  }
}
