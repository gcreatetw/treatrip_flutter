import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/widget/state_controller.dart';

const double _kIconSize = 40.0;

class GeneralStateContent extends StatelessWidget {
  final GeneralState? state;
  final String? hint;
  final String? subHint;
  final Widget? child;
  final VoidCallback? onRetry;
  final bool useRefreshIndicator;
  final String? tag;
  final Widget? customIcon;
  final bool isShowCopyrightText;

  const GeneralStateContent({
    Key? key,
    this.state,
    this.child,
    this.hint,
    this.subHint,
    this.customIcon,
    this.onRetry,
    this.useRefreshIndicator = false,
    this.tag,
    this.isShowCopyrightText = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (state!) {
      case GeneralState.error:
        return InkWell(
          onTap: () {
            onRetry?.call();
          },
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AppText.themeColor(
                  hint ?? '錯誤',
                  textAlign: TextAlign.center,
                  fontSize: 17.0,
                ),
                const SizedBox(height: 39.0),
                customIcon ??
                    const Icon(
                      Icons.info_outlined,
                      size: _kIconSize,
                      color: AppColors.themeColor,
                    ),
              ],
            ),
          ),
        );
      case GeneralState.finish:
        return useRefreshIndicator
            ? RefreshIndicator(
                onRefresh: () async {
                  onRetry?.call();
                },
                child: child!,
              )
            : child!;
      case GeneralState.loading:
        return Container(
          color: AppColors.white,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Spacer(),
                SizedBox(
                  width: _kIconSize,
                  height: _kIconSize,
                  child: customIcon ??
                      const CircularProgressIndicator(
                        color: AppColors.themeColor,
                      ),
                ),
                const SizedBox(height: 39.0),
                AppText.themeColor(
                  hint ?? '載入中',
                  textAlign: TextAlign.center,
                  fontSize: 17.0,
                ),
                if (subHint != null) ...<Widget>[
                  const Spacer(),
                  AppText.themeColor(subHint!, fontSize: 24.0),
                ],
                const Spacer(),
                // if (isShowCopyrightText) ...<Widget>[
                //   const CopyRightCard(
                //     isLogoWhite: true,
                //   ),
                //   const SizedBox(height: 24.0),
                // ],
              ],
            ),
          ),
        );
    }
  }
}
