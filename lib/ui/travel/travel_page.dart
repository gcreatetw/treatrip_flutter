import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/banner_data.dart';
import 'package:treatrip/provider/travel_page_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/search/search_integration_page.dart';
import 'package:treatrip/ui/travel/travel_banner_page.dart';
import 'package:treatrip/widget/image_slider/image_slider_big.dart';
import 'package:treatrip/widget/image_slider/image_slider_blog.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/widget/store_information_title.dart';

import '../../widget/image_slider/image_slider_small.dart';
import '../explore/banner_page.dart';

class TravelPage extends StatefulWidget {
  const TravelPage({Key? key}) : super(key: key);

  @override
  _TravelPageState createState() => _TravelPageState();
}

class _TravelPageState extends State<TravelPage>   with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => false;
  TravelPageData travelPageData = TravelPageData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      travelPageData.fetchAll();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text(
          '趣趣出遊',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) => const SearchIntegrationPage(),
                ),
              );
            },
            child: const Icon(Icons.search_outlined),
          ),
          const SizedBox(width: 22.5),
          GestureDetector(
            onTap: () {
              // Map icon
            },
            child: const Icon(Icons.map_outlined),
          ),
          const SizedBox(width: 15.0)
        ],
      ),
      body: MultiProvider(
        providers: [ChangeNotifierProvider.value(value: travelPageData)],
        child: Consumer(
          builder: (context, TravelPageData data, _) {
            return StateController(
              state: data.state,
              child: ListView(
                  children: [
                    ImageSliderBig(
                      data: Provider.of<BannerData>(context).bannerTravelList,
                      linkData:
                          Provider.of<BannerData>(context).bannerTravelLinkList,
                      height: width,
                      dotSelectedColor: Colors.white,
                      dotColor: Colors.white,
                      selectOpacity: 1,
                      unSelectedOpacity: 0.5,
                      dotSize: 8.0,
                      dotPadding: 2.0,
                    ),
                    Container(color: Colors.white, height: 12.0),
                    bigText('推薦住宿'),
                    Container(color: Colors.white, height: 16.0),
                    // ImageSliderBlog(
                    //   height: width,
                    //   dotSelectedColor: AppColors.themeColor,
                    //   dotColor: Colors.black,
                    //   selectOpacity: 1,
                    //   unSelectedOpacity: 0.12,
                    //   dotSize: 8,
                    //   dotPadding: 4,
                    //   data: data.blogList,
                    //   short: true,
                    //   isCard: true,
                    // ),
                    // Container(color: Colors.white, height: 8.0),
                    ImageSliderSmall(
                      title: '主題冒險',
                      data: data.lodgingList,
                      height: width,
                      dotSelectedColor: AppColors.themeColor,
                      dotColor: Colors.black,
                      selectOpacity: 1,
                      unSelectedOpacity: 0.12,
                      dotSize: 8.0,
                      dotPadding: 4.0,
                    ),
                    Container(color: Colors.white, height: 12.0),
                    // Container(
                    //   padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    //   width: double.maxFinite,
                    //   color: Colors.white,
                    //   child: OutlinedButton(
                    //     onPressed: () {
                    //       Navigator.of(context).push(
                    //         MaterialPageRoute<void>(
                    //           builder: (_) => const BannerPage(
                    //               exploreType: ExploreType.periodLimit),
                    //         ),
                    //       );
                    //     },
                    //     style: OutlinedButton.styleFrom(
                    //         side: const BorderSide(color: AppColors.themeColor),
                    //         primary: AppColors.themeColor),
                    //     child: const Text(
                    //       '查看更多主題冒險',
                    //       style: TextStyle(
                    //           color: Color(0xffed7465),
                    //           fontWeight: FontWeight.w500,
                    //           fontFamily: "NotoSansCJKtc",
                    //           fontStyle: FontStyle.normal,
                    //           fontSize: 15.0),
                    //       textAlign: TextAlign.center,
                    //     ),
                    //   ),
                    // ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      width: double.maxFinite,
                      color: Colors.white,
                      child: OutlinedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute<void>(
                              builder: (_) => const TravelBannerPage(),
                            ),
                          );
                        },
                        style: OutlinedButton.styleFrom(
                          side: const BorderSide(
                            color: AppColors.themeColor,
                          ),
                          primary: AppColors.themeColor,
                        ),
                        child: const Text('查看更多推薦住宿',
                            style: TextStyle(
                                color: Color(0xffed7465),
                                fontWeight: FontWeight.w500,
                                fontFamily: "NotoSansCJKtc",
                                fontStyle: FontStyle.normal,
                                fontSize: 15.0),
                            textAlign: TextAlign.center),
                      ),
                    ),
                    Container(color: Colors.white, height: 12.0),
                    const SizedBox(height: 12.0),
                    for(final element in data.hotelList)
                      StoreInformationTitle(
                        data: element,
                        showStoreInformation: false,
                        clickable: true,
                      ),
                    // ListView.builder(
                    //   shrinkWrap: true,
                    //   physics: const NeverScrollableScrollPhysics(),
                    //   itemCount: data.hotelList.length,
                    //   itemBuilder: (context, int index) {
                    //     return StoreInformationTitle(
                    //       data: data.hotelList[index],
                    //       showStoreInformation: false,
                    //       clickable: true,
                    //     );
                    //   },
                    // ),
                    const SizedBox(height: 56.0),
                    Column(
                      children: const [
                        Text(
                          '趣趣',
                          style: TextStyle(color: AppColors.themeColor),
                        ),
                        SizedBox(height: 9.0),
                        Text(
                          '© Copyright 2020 TRETRIP. All rights reserved.',
                          style: TextStyle(
                            color: Color(0x61000000),
                            fontWeight: FontWeight.w400,
                            fontFamily: "Roboto",
                            fontStyle: FontStyle.normal,
                            fontSize: 8.0,
                          ),
                        ),
                        SizedBox(
                          height: 24.0,
                        ),
                      ],
                    )
                  ],
                ),
            );
          },
        ),
      ),
    );
  }

  Widget bigText(String text) {
    return Container(
      color: Colors.white,
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.only(left: 16.0),
      child: Text(text,
          style: const TextStyle(
              color: AppColors.themeColor,
              fontWeight: FontWeight.w700,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0)),
    );
  }
}
