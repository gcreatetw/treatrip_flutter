import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import '../../provider/travel_banner_page_provider.dart';
import '../../res/resources.dart';
import '../../utils/utils.dart';
import '../../widget/float_back_button.dart';
import '../../widget/state_controller.dart';
import '../../widget/travel_article_list_title.dart';

class TravelBannerPage extends StatefulWidget {
  final int? bannerId;
  //final List<ChildrenBean>? termsId;
  final String? exploreTaiwanImageUrl;
  final String? exploreTaiwanTitle;

  const TravelBannerPage({
    Key? key,
    this.bannerId,
    //this.termsId,
    this.exploreTaiwanImageUrl,
    this.exploreTaiwanTitle,
  }) : super(key: key);

  @override
  _TravelBannerPageState createState() => _TravelBannerPageState();
}

class _TravelBannerPageState extends State<TravelBannerPage> {
  TravelBannerPageProvider travelBannerPageProvider =
      TravelBannerPageProvider();

  int currentChip = 0;

  @override
  void initState() {
    Future<void>.microtask(() async {
      travelBannerPageProvider.fetch(
        bannerId: widget.bannerId,
        exploreTaiwanImageUrl: widget.exploreTaiwanImageUrl,
        exploreTaiwanTitle: widget.exploreTaiwanTitle,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    //final toolbarHeight = MediaQuery.of(context).padding.top;

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      appBar: appBar(),
      body: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider.value(value: travelBannerPageProvider),
        ],
        child: Consumer<TravelBannerPageProvider>(
          builder: (BuildContext context, TravelBannerPageProvider data, _) {
            return NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification scrollInfo) {
                  if (scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent) {
                    _loadMore();
                  }
                  return true;
                },
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if(data.bannerImageUrl != null && data.bannerImageUrl!.isNotEmpty)
                      Flexible(
                        fit: FlexFit.loose,
                        child: Stack(
                          children: [
                            Image.network(
                              data.bannerImageUrl ?? '',
                              fit: BoxFit.cover,
                              width: width,
                              height: width / 320 * 426,
                            ),
                            Positioned(
                              left: 16.0,
                              right: 16.0,
                              bottom: 16.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    Utils.fixTitle(data.title ?? ''),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "NotoSansCJKtc",
                                      fontStyle: FontStyle.normal,
                                      fontSize: 25.0,
                                    ),
                                  ),
                                  if (data.smallTitle != null)
                                    Text(
                                      Utils.fixTitle(data.smallTitle!),
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "NotoSansCJKtc",
                                        fontStyle: FontStyle.normal,
                                        fontSize: 17.0,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      //if (widget.exploreType != ExploreType.exploreTaiwan)
                      Container(color: Colors.white, height: 12.0),
                      StateController(
                        state: data.state,
                        child: Container(
                          color: Colors.white,
                          child: ListView.builder(
                            padding: EdgeInsets.zero,
                            shrinkWrap: true,
                            itemCount: data.articleList.length,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (_, int index) {
                              return TravelArticleListTitle(
                                data: data.articleList[index],
                              );
                            },
                          ),
                        ),
                      ),
                      const SizedBox(height: 56.0),
                      const Text(
                        '趣趣',
                        style: TextStyle(color: AppColors.themeColor),
                      ),
                      const SizedBox(height: 9.0),
                      const Text(
                        '© Copyright 2020 TRETRIP. All rights reserved.',
                        style: TextStyle(
                          color: Color(0x61000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto",
                          fontStyle: FontStyle.normal,
                          fontSize: 8.0,
                        ),
                      ),
                      const SizedBox(
                        height: 24.0,
                      ),
                    ],
                  ),
                ));
          },
        ),
      ),
    );
  }

  PreferredSizeWidget appBar() {
    String title = '';

    return AppBar(
      backgroundColor: AppColors.themeColor,
      title: Text(
        title,
        style: const TextStyle(
          color: Color(0xffffffff),
          fontWeight: FontWeight.w500,
          fontFamily: "NotoSansCJKtc",
          fontStyle: FontStyle.normal,
          fontSize: 25.0,
        ),
      ),
    );
  }

  void _loadMore() {
    travelBannerPageProvider.fetch(
      append: true,
    );
  }
}
