import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:treatrip/models/text_enum.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/google_map_widget.dart';

class CompleteInformationPage extends StatefulWidget {
  final String storeName;
  final String storeAddress;
  final String storeOpeningDay;
  final String storeIntroduction;
  final String payment;
  final String webUrl;
  final String contactPhoneNumber;
  final String reservePhoneNumber;
  final String menuUrl;
  final String reserveUrl;
  final List<String> businessHours;
  final LatLng latLng;

  const CompleteInformationPage({
    Key? key,
    required this.storeName,
    required this.storeAddress,
    required this.storeOpeningDay,
    required this.storeIntroduction,
    required this.payment,
    required this.webUrl,
    required this.contactPhoneNumber,
    required this.reservePhoneNumber,
    required this.menuUrl,
    required this.reserveUrl,
    required this.businessHours, required this.latLng,
  }) : super(key: key);

  @override
  _CompleteInformationPageState createState() =>
      _CompleteInformationPageState();
}

class _CompleteInformationPageState extends State<CompleteInformationPage> {
  final List<String> weekDays = [
    '星期日',
    '星期一',
    '星期二',
    '星期三',
    '星期四',
    '星期五',
    '星期六',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '完整資訊',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(color: Colors.white, height: 23.0),
            textBox(widget.storeName, TextType.normalBlack, null),
            Container(color: Colors.white, height: 3.0),
            textBox(widget.storeAddress, TextType.smallGrey, null),
            Container(color: Colors.white, height: 6.0),
            Container(
              width: double.maxFinite,
              height: 164,
              color: Colors.amberAccent,
              child: FlutterMap(
                options: MapOptions(
                  center: widget.latLng,
                  zoom: 15.0,
                ),
                layers: <LayerOptions>[
                  TileLayerOptions(
                    urlTemplate:
                        'https://mt0.google.com/vt/lyrs=m@221097413,transit&hl=zh-TW&x={x}&y={y}&z={z}',
                    subdomains: <String>['a', 'b', 'c'],
                    attributionBuilder: (_) {
                      return const Text('© Google Map');
                    },
                  ),
                  MarkerLayerOptions(
                    markers: <Marker>[
                      Marker(
                        width: 80.0,
                        height: 80.0,
                        point: widget.latLng,
                        builder: (_) =>  const Icon(
                          Icons.location_on,
                          color: Colors.redAccent,
                        ),
                      ),
                    ],
                      ),
                ],
              ),
            ),
            Container(color: Colors.white, height: 8.0),
            textBox('開幕日期:${widget.storeOpeningDay}', TextType.smallGrey, null),
            Container(color: Colors.white, height: 4.0),
            textBox(widget.storeIntroduction, TextType.smallGrey, null),
            Container(color: Colors.white, height: 16.0),
            textBox('支付方式', TextType.normalBlack, null),
            textBox(widget.payment, TextType.smallGrey, null),
            Container(color: Colors.white, height: 14.0),
            textBox('網站', TextType.normalBlack, null),
            textBox(widget.webUrl, TextType.smallGrey, null),
            Container(color: Colors.white, height: 14.0),
            textBox('連絡電話', TextType.normalBlack, null),
            textBox(widget.contactPhoneNumber, TextType.smallGrey, null),
            Container(color: Colors.white, height: 14.0),
            textBox('預約電話', TextType.normalBlack, null),
            textBox(widget.reservePhoneNumber, TextType.smallGrey, null),
            Container(color: Colors.white, height: 14.0),
            textBox('菜單網址', TextType.normalBlack, null),
            textBox(widget.menuUrl, TextType.smallGrey, null),
            Container(color: Colors.white, height: 14.0),
            textBox('預定網址', TextType.normalBlack, null),
            textBox(widget.reserveUrl, TextType.smallGrey, null),
            Container(color: Colors.white, height: 19.0),
            const SizedBox(height: 8.0),
            Container(color: Colors.white, height: 7.0),
            Container(
              color: Colors.white,
              child: Row(
                children: [
                  const SizedBox(width: 8.0),
                  Container(
                    width: 4.0,
                    height: 19,
                    color: AppColors.titleBlue,
                  ),
                  const SizedBox(width: 4.0),
                  const Text(
                    '營業時間',
                    style: TextStyle(
                      color: Color(0x61000000),
                      fontWeight: FontWeight.w400,
                      fontFamily: "NotoSansCJKtc",
                      fontStyle: FontStyle.normal,
                      fontSize: 14.0,
                    ),
                  ),
                ],
              ),
            ),
            Container(color: Colors.white, height: 8.0),
            Container(
              color: Colors.white,
              child: ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, int index) {
                  return businessDayCard(
                      widget.businessHours[index]
                          .substring(4, widget.businessHours[index].length),
                      weekDays[index],
                      index);
                },
                separatorBuilder: (context, int index) {
                  return const Divider(
                    height: 1,
                    color: Color.fromRGBO(0, 0, 0, 0.12),
                  );
                },
                itemCount: widget.businessHours.length,
              ),
            ),
            const SizedBox(height: 56.0),
            const Text(
              '趣趣',
              style: TextStyle(color: AppColors.themeColor),
            ),
            const SizedBox(height: 9.0),
            const Text(
              '© Copyright 2020 TRETRIP. All rights reserved.',
              style: TextStyle(
                color: Color(0x61000000),
                fontWeight: FontWeight.w400,
                fontFamily: "Roboto",
                fontStyle: FontStyle.normal,
                fontSize: 8.0,
              ),
            ),
            const SizedBox(
              height: 24.0,
            ),
          ],
        ),
      ),
    );
  }

  Widget textBox(String? text, TextType type, Widget? myText) {
    final Widget child;
    switch (type) {
      case TextType.normalBlack:
        child = Text(
          text!,
          style: const TextStyle(
            color: Color(0xde000000),
            fontWeight: FontWeight.w500,
            fontFamily: "NotoSansCJKtc",
            fontStyle: FontStyle.normal,
            fontSize: 15.0,
          ),
        );
        break;
      case TextType.smallGrey:
        child = Text(
          text!,
          style: const TextStyle(
            color: Color(0x61000000),
            fontWeight: FontWeight.w400,
            fontFamily: "NotoSansCJKtc",
            fontStyle: FontStyle.normal,
            fontSize: 13.0,
          ),
        );
        break;
      case TextType.custom:
        child = myText!;
        break;
    }
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      alignment: Alignment.centerLeft,
      child: child,
    );
  }

  Widget businessDayCard(String businessHours, String day, int index) {
    businessHours.replaceAll('．', String.fromCharCode(0x00B7));
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 16.0),
          alignment: Alignment.centerLeft,
          height: 48.0,
          width: 108.0,
          color: index == 0
              ? const Color.fromRGBO(237, 116, 101, 0.12)
              : const Color.fromRGBO(66, 147, 238, 0.12),
          child: Text(
            day,
            style: const TextStyle(
              color: Color(0xde000000),
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 13.0,
            ),
          ),
        ),
        const SizedBox(width: 16.0),
        Text(
          businessHours.replaceAll('．', ' ${String.fromCharCode(0x00B7)} '),
          style: const TextStyle(
            color: Color(0x61000000),
            fontWeight: FontWeight.w400,
            fontFamily: "Roboto",
            fontStyle: FontStyle.normal,
            fontSize: 12.0,
          ),
        ),
        const SizedBox(width: 16.0),
      ],
    );
  }
}
