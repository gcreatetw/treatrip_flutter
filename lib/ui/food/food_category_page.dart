import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/food_page_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/data_construction.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/widget/store_information_title.dart';

class FoodCategoryPage extends StatefulWidget {
  final String title;

  const FoodCategoryPage({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  _FoodCategoryPageState createState() => _FoodCategoryPageState();
}

class _FoodCategoryPageState extends State<FoodCategoryPage> {
  final FoodPageData foodPageData = FoodPageData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      foodPageData.fetch();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
        actions: [
          GestureDetector(
            onTap: () {},
            child: const Icon(Icons.map_outlined),
          ),
          const SizedBox(width: 15.0)
        ],
      ),
      body: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: foodPageData),
        ],
        child: Consumer(
          builder: (BuildContext context, FoodPageData data, _) {
            return StateController(
              state: data.state,
              child: data.storeList.isEmpty
                  ? const DataConstruction()
                  : SingleChildScrollView(
                      child: Column(
                        children: [
                          const SizedBox(height: 18.0),
                          ListView.builder(
                            padding: EdgeInsets.zero,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: data.storeList.length,
                            itemBuilder: (_, int index) {
                              return StoreInformationTitle(
                                data: data.storeList[index],
                                showStoreInformation: false,
                                clickable: true,
                              );
                            },
                          ),
                          const SizedBox(height: 56.0),
                          const Text(
                            '趣趣',
                            style: TextStyle(color: AppColors.themeColor),
                          ),
                          const SizedBox(height: 9.0),
                          const Text(
                            '© Copyright 2020 TRETRIP. All rights reserved.',
                            style: TextStyle(
                              color: Color(0x61000000),
                              fontWeight: FontWeight.w400,
                              fontFamily: "Roboto",
                              fontStyle: FontStyle.normal,
                              fontSize: 8.0,
                            ),
                          ),
                          const SizedBox(
                            height: 24.0,
                          ),
                        ],
                      ),
                    ),
            );
          },
        ),
      ),
    );
  }
}
