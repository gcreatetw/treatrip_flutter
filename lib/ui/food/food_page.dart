import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:treatrip/models/data/banner_data.dart';
import 'package:treatrip/models/data/food_page_data.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/food/food_category_page.dart';
import 'package:treatrip/ui/search/search_integration_page.dart';
import 'package:treatrip/widget/image_slider/image_slider_big.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/widget/store_information_title.dart';

class FoodPage extends StatefulWidget {
  const FoodPage({Key? key}) : super(key: key);

  @override
  _FoodPageState createState() => _FoodPageState();
}

class _FoodPageState extends State<FoodPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => false;
  final FoodPageData foodPageData = FoodPageData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      foodPageData.fetch();
      foodPageData.fetchGourmetData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text(
          '全台美食',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) => const SearchIntegrationPage(),
                ),
              );
            },
            child: const Icon(Icons.search_outlined),
          ),
          const SizedBox(width: 22.5),
          GestureDetector(
            onTap: () {},
            child: const Icon(Icons.map_outlined),
          ),
          const SizedBox(width: 15.0)
        ],
      ),
      body: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider.value(value: foodPageData)
        ],
        child: Consumer(
          builder: (BuildContext context, FoodPageData data, _) {
            return StateController(
              state: data.state,
              child: ListView(
                children: [
                  ImageSliderBig(
                    data: Provider.of<BannerData>(context).bannerFoodList,
                    linkData:
                        Provider.of<BannerData>(context).bannerFoodLinkList,
                    height: width,
                    dotSelectedColor: Colors.white,
                    dotColor: Colors.white,
                    selectOpacity: 1,
                    unSelectedOpacity: 0.5,
                    dotSize: 8.0,
                    dotPadding: 2.0,
                  ),
                  const SizedBox(height: 16.0),
                  bigText('精選分類'),
                  const SizedBox(height: 16.0),
                  Container(
                    height: 160,
                    alignment: Alignment.centerLeft,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: data.foodCategoryList.length,
                      itemBuilder: (_, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute<void>(
                                builder: (_) => FoodCategoryPage(
                                  title: data.foodCategoryList[index].title,
                                ),
                              ),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: index == 0 ? 16.0 : 4.0,
                                right: index == data.foodCategoryList.length - 1
                                    ? 16.0
                                    : 4.0),
                            child: Card(
                              margin: EdgeInsets.zero,
                              child: Stack(
                                children: [
                                  Image.network(
                                    data.foodCategoryList[index].imageUrl,
                                    fit: BoxFit.cover,
                                    width: 160,
                                    height: 160,
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    child: Container(
                                      padding:
                                          const EdgeInsets.only(left: 12.0),
                                      width: 160,
                                      alignment: Alignment.centerLeft,
                                      color: const Color.fromRGBO(0, 0, 0, 0.3),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const SizedBox(height: 2.0),
                                          Text(
                                            data.foodCategoryList[index].title,
                                            style: const TextStyle(
                                              color: Color(0xffffffff),
                                              fontWeight: FontWeight.w900,
                                              fontFamily: "NotoSansCJKtc",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                          Text(
                                            '${data.foodCategoryList[index].recommendCount} 個推薦',
                                            style: const TextStyle(
                                              color: Color(0xffffffff),
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "Roboto",
                                              fontStyle: FontStyle.normal,
                                              fontSize: 13.0,
                                            ),
                                          ),
                                          const SizedBox(height: 8.0)
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  bigText('附近推薦'),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.only(left: 16.0),
                    child: const Text(
                      '看看我們幫你搜出的優質店家吧！',
                      style: TextStyle(
                        color: Color(0x8a000000),
                        fontWeight: FontWeight.w400,
                        fontFamily: "NotoSansCJKtc",
                        fontStyle: FontStyle.normal,
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                  const SizedBox(height: 26),
                  for (final StoreInformationTitleData element
                      in data.storeList)
                    StoreInformationTitle(
                      data: element,
                      showStoreInformation: false,
                      clickable: true,
                    ),
                  // ListView.builder(
                  //   padding: const EdgeInsets.symmetric(vertical: 26.0),
                  //   shrinkWrap: true,
                  //   physics: const NeverScrollableScrollPhysics(),
                  //   itemCount: data.storeList.length,
                  //   itemBuilder: (_, int index) {
                  //     return ;
                  //   },
                  // ),
                  const SizedBox(height: 26),
                  Column(
                    children: const [
                      Text(
                        '趣趣',
                        style: TextStyle(color: AppColors.themeColor),
                      ),
                      SizedBox(height: 9.0),
                      Text(
                        '© Copyright 2020 TRETRIP. All rights reserved.',
                        style: TextStyle(
                          color: Color(0x61000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto",
                          fontStyle: FontStyle.normal,
                          fontSize: 8.0,
                        ),
                      ),
                      SizedBox(
                        height: 24.0,
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget bigText(String text) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.only(left: 16.0),
      child: Text(text,
          style: const TextStyle(
              color: AppColors.themeColor,
              fontWeight: FontWeight.w700,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0)),
    );
  }
}
