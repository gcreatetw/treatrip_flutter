import 'dart:async';
import 'package:flutter/material.dart';
import 'package:treatrip/source/default_img.dart';
import 'package:treatrip/ui/explore/explore_page.dart';

import 'main_page.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  late Timer _timer;
  int count = 3;

  startTime() async {
    //設置啟動圖生效時間
    var _duration = const Duration(seconds: 1);
    Timer(_duration, () {
      // 空等1秒之后再計時
      _timer = Timer.periodic(const Duration(milliseconds: 1000), (v) {
        count--;
        debugPrint('count : $count');
        if (count == 0) {
          navigationPage();
        } else {
          setState(() {});
        }
      });
    });
  }

  void navigationPage() {
    _timer.cancel();
    //Navigator.push(context, MaterialPageRoute(builder: (context) => const MyHomePage(title: '123'), maintainState: false));

    //跳轉並關閉當前頁面
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (context) => const MainPage(),
      ),
      (route) => route == null,
    );
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(DefaultImages.welcomePageBackground),
                      fit: BoxFit.fill)),
            ),
            SizedBox(
              width: 121,
              height: 220,
              child: Image.asset(DefaultImages.welcomeTreatripLogo),
            ),
          ],
        ),
      ),
    );
  }
}
