import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

class LightCoralCard extends StatelessWidget {
  final Widget child;

  const LightCoralCard({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Card(
        color: AppColors.lightCoral,
        elevation: 0,
        child: Padding(
            padding: const EdgeInsets.only(
              left: 8.0,
              right: 8.0,
              top: 16.0,
              bottom: 24.0,
            ),
            child: child),
      ),
    );
  }
}
