import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/provider/coupon_provider.dart';

import '../../res/resources.dart';
import '../app_text.dart';
import '../post_webview.dart';

class CouponCard extends StatelessWidget {
  final CouponDataModel data;

  const CouponCard({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 8.0,
        right: 8.0,
      ),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute<void>(
                builder: (_) => PostWebView(url: data.linkUrl)),
          );
        },
        child: Card(
          elevation: 3.0,
          child: Container(
            height: 72,
            child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 24.0,
                      backgroundImage: NetworkImage(data.imageUrl)),
                  const SizedBox(width: 12.5),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.couponTitle(data.title, fontSize: 16),
                        const SizedBox(height: 6.0),
                        AppText.couponSubTitle(data.content,
                            fontSize: 14,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                        //const SizedBox(height: 14.0),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
