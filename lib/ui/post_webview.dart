import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PostWebView extends StatefulWidget {
  final String url;

  const PostWebView({Key? key, required this.url}) : super(key: key);

  @override
  _PostWebViewState createState() => _PostWebViewState();
}

class _PostWebViewState extends State<PostWebView> {
  GeneralState state = GeneralState.loading;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.themeColor,
      ),
      body: Stack(
        children: [
          WebView(
            initialUrl: widget.url,
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (finish) {
              setState(() {
                state = GeneralState.finish;
              });
            },
          ),
          state == GeneralState.loading
              ? const Center(
                  child: CircularProgressIndicator(
                    color: AppColors.themeColor,
                  ),
                )
              : Stack(),
        ],
      ),
    );
  }
}
