import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

class AppBlockWidget {
  /// 某些區塊會有底色，其 margin 與 padding 都固定
  static Widget normal({
    required Widget child,
  }) {
    return Padding(
      padding: const EdgeInsets.all(AppDimens.generateMargin),
      child: child,
    );
  }
}
