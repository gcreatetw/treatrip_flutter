import 'dart:collection';
import 'dart:io';

import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/models/user_location.dart';
import 'package:treatrip/provider/app_provider.dart';
import 'package:treatrip/provider/user_provider.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/explore/explore_page.dart';
import 'package:treatrip/ui/food/food_page.dart';
import 'package:treatrip/ui/travel/travel_page.dart';
import 'package:treatrip/models/data/banner_data.dart';
import 'package:treatrip/ui/user/user_main_page.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/data/food_page_data.dart';
import 'account/login_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  UserLocation userLocation = UserLocation();

  late TabController controller;

  int currentIndex = 0;
  final int _memberPageIndex = 3;

  final ListQueue<int> _navigationQueue = ListQueue();
  BannerData bannerData = BannerData.init();
  FoodPageData foodPageData = FoodPageData.init();

  final pages = <Widget>[
    const ExplorePage(),
    const FoodPage(),
    const TravelPage(),
    const UserMainPage(),
  ];

  @override
  void initState() {
    versionCheck();
    controller = TabController(
      length: pages.length,
      vsync: this,
    );
    Future<void>.microtask(() async {
      bannerData.fetchBannerData();
      final bool canFetch = await userLocation.requestPermission();
      if (canFetch) {
        await userLocation.fetch();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_navigationQueue.isEmpty) return true;
        setState(() {
          _navigationQueue.removeLast();
          int position = _navigationQueue.isEmpty ? 0 : _navigationQueue.last;
          currentIndex = position;
        });
        return false;
      },
      child: Scaffold(
        body: MultiProvider(
          providers: [
            ChangeNotifierProvider.value(
              value: bannerData,
            ),
          ],
          child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: controller,
            children: pages,
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          unselectedItemColor: AppColors.black54,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              label: '',
              icon: Icon(Icons.travel_explore_outlined),
            ),
            BottomNavigationBarItem(
              label: '',
              icon: Icon(Icons.restaurant_menu_outlined),
            ),
            BottomNavigationBarItem(
              label: '',
              icon: Icon(Icons.edit_location_outlined),
            ),
            BottomNavigationBarItem(
              label: '',
              icon: Icon(Icons.account_circle_outlined),
            ),
          ],
          selectedItemColor: AppColors.themeColor,
          currentIndex: controller.index,
          onTap: onItemClick,
        ),
      ),
    );
  }

  //TODO else
  Future<void> onItemClick(int value) async {
    if (value != controller.index) {
      if (value == _memberPageIndex &&
          context.read<UserProvider>().isLogin == false) {
        bool? demo = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const LoginPage()));

        if (demo ?? false) {
          controller.animateTo(value);
        }
      } else {
        setState(() {
          controller.animateTo(value);
        });
        _navigationQueue.removeWhere((element) => element == value);
        _navigationQueue.addLast(value);
      }
    }
    setState(() {});
  }

  Future<void> versionCheck() async {
    await context.read<AppProvider>().getAppVersions();
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final String version = packageInfo.version;
    if (!context.read<AppProvider>().versions.contains(version)) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('更新'),
            content: const Text('新版本已釋出！'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('取消'),
              ),
              TextButton(
                onPressed: () {
                  if (Platform.isIOS) {
                    launch(
                        'https://apps.apple.com/us/app/%E8%B6%A3%E8%B6%A3-treatrip/id1564435182');
                  } else {
                    launch(
                        'https://play.google.com/store/apps/details?id=com.gcreate.treatrip');
                  }
                },
                child: const Text('更新'),
              )
            ],
          );
        },
      );
    }
  }
}
