import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:treatrip/provider/coupon_provider.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/card/coupon_card.dart';
import 'package:treatrip/ui/user/user_edit_page.dart';
import 'package:treatrip/widget/body/app_state_content.dart';
import 'package:treatrip/widget/button/edit_user_button.dart';
import 'package:image_picker/image_picker.dart';

import '../../provider/user_page_provider.dart';
import 'default_profile_photo_layout.dart';

class UserMainPage extends StatefulWidget {
  const UserMainPage({Key? key}) : super(key: key);

  @override
  State<UserMainPage> createState() => _UserMainPageState();
}

class _UserMainPageState extends State<UserMainPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => false;
  XFile? avatarImage;
  XFile? backgroundImage;

  CouponProvider couponData = CouponProvider();

  @override
  void initState() {
    Future<void>.microtask(() async {
      couponData.fetchCouponData();
      // userPageData.fetchUserData(
      //     phone: Preferences.getString(Constants.phone, ''),
      //     password: Preferences.getString(Constants.password, ''));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return MultiProvider(
      providers: <SingleChildWidget>[
        // ChangeNotifierProvider.value(value: userPageData),
        ChangeNotifierProvider.value(value: couponData),
      ],
      child: Consumer2<UserPageProvider, CouponProvider>(
        builder: (BuildContext context, UserPageProvider provider,
            CouponProvider coupon, _) {
          return GeneralStateContent(
            state: provider.state,
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    const SizedBox(
                      height: 200,
                      width: double.maxFinite,
                    ),
                    Column(
                      children: [
                        if (provider.userModel.backgroundImage != '')
                          ExtendedImage.network(
                            provider.userModel.backgroundImage,

                            // height: 144,
                            // width: double.infinity,
                            fit: BoxFit.cover,
                          )
                        else
                          Image.asset(
                            'assets/images/image_cover_pv.jpeg',
                            fit: BoxFit.cover,
                          ),
                        const SizedBox(height: 60)
                      ],
                    ),
                    if (provider.userModel.avatarPhoto == '')
                      const Positioned(
                        bottom: 0,
                        child: DefaultProfilePhotoLayout(
                          image: 'assets/images/ic_appbar_user_active.png',
                        ),
                      )
                    else
                      Positioned(
                        bottom: 0,
                        child: CircleAvatar(
                          radius: 68.0,
                          backgroundColor: AppColors.white,
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 64.0,
                            backgroundImage:
                                NetworkImage(provider.userModel.avatarPhoto),
                          ),
                        ),
                      ),
                  ],
                ),
                const SizedBox(height: 6),
                Center(
                  child:
                      AppText.userName(provider.userModel.name, fontSize: 25),
                ),
                const SizedBox(height: 28),
                Padding(
                  padding: const EdgeInsets.only(right: 16, left: 16),
                  child: EditUserButton(
                    onPressed: () => _toUserEditPage(context, provider),
                    icon: const Icon(
                      Icons.edit_outlined,
                      color: Colors.black38,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Container(height: 6, color: AppColors.editUser),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 11,
                      bottom: 11,
                      left: 16,
                    ),
                    child: AppText.black('優惠券', fontSize: 15),
                  ),
                ),
                for (final element in coupon.couponList)
                  GestureDetector(
                    child: CouponCard(
                      data: element,
                    ),
                  ),
                // ListView.builder(
                //   padding: const EdgeInsets.symmetric(vertical: 3.0),
                //   physics: const NeverScrollableScrollPhysics(),
                //   shrinkWrap: true,
                //   itemCount: coupon.couponList.length,
                //   itemBuilder: (_, int index) {
                //     return GestureDetector(
                //       child: CouponCard(
                //         data: coupon.couponList[index],
                //       ),
                //     );
                //   },
                // ),
                const CopyRightCard(),
              ],
            ),
          );
        },
      ),
    );
  }

  Future<void> _toUserEditPage(
      BuildContext context, UserPageProvider provider) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => UserEditPage(
          provider: provider,
        ),
      ),
    );
    // await provider.fetchUserData(
    //     phone: Preferences.getString(Constants.phone, ''),
    //     password: Preferences.getString(Constants.password, ''));
    setState(() {});
  }
}
