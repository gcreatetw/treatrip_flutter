import 'dart:io';

import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

class EditProfilePhotoLayout extends StatelessWidget {
  final String image;

  const EditProfilePhotoLayout({Key? key, required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 68.0,
      backgroundColor: AppColors.white,
      child: CircleAvatar(
        backgroundColor: Colors.white,
        radius: 64.0,
        backgroundImage: FileImage(File(image)),
      ),
    );
  }
}
