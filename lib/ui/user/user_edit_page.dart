import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/provider/user_page_provider.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_block_widget.dart';
import 'package:treatrip/ui/app_input.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/card/light_coral_card.dart';
import 'package:treatrip/ui/user/edit_profile_photo_layout.dart';
import 'package:treatrip/utils/preferences.dart';
import 'package:treatrip/utils/utils.dart';
import 'package:treatrip/widget/button/delete_account_button.dart';
import 'package:treatrip/widget/button/edit_user_page_button.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../../models/user_model.dart';
import '../../provider/update_user_data_provider.dart';
import 'default_profile_photo_layout.dart';
import 'delete_web_page.dart';
import 'net_profile_photo_layout.dart';

class UserEditPage extends StatefulWidget {
  final UserPageProvider provider;

  const UserEditPage({Key? key, required this.provider}) : super(key: key);

  @override
  State<UserEditPage> createState() => _UserEditPageState();
}

class _UserEditPageState extends State<UserEditPage> {
  bool isEditName = false;
  bool isEditAvatar = false;
  bool isEditBackground = false;

  XFile? avatarFile;
  XFile? backgroundFile;
  String? selectedImage;

  final UserModel userModel = UserModel();

  final TextEditingController _nickname = TextEditingController();
  UpdateUserDataProvider updateUserDataProvider = UpdateUserDataProvider();

  // UserPageProvider userPageData = UserPageProvider();

  @override
  void initState() {
    Future.microtask(() async {
      await widget.provider.fetchUserData(
          phone: Preferences.getString(Constants.phone, ''),
          password: Preferences.getString(Constants.password, ''));
    });
    super.initState();
  }

  //WillPopScope;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: updateUserDataProvider),
        ],
        child: Consumer<UpdateUserDataProvider>(builder: (
          BuildContext context,
          UpdateUserDataProvider provider,
          _,
        ) {
          return Scaffold(
            appBar: AppBar(
              title: const Text(
                '編輯用戶資訊',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    fontSize: 25.0),
              ),
              backgroundColor: AppColors.themeColor,
              actions: [
                if (isEditName != false)
                  GestureDetector(
                    onTap: () async {
                      await provider.updateData(
                        name: _nickname.text,
                      );
                      await widget.provider.fetchUserData(
                        phone: Preferences.getString(Constants.phone, ''),
                        password: Preferences.getString(Constants.password, ''),
                      );
                      setState(() {
                        isEditName = false;
                      });
                    },
                    child: const Icon(
                      Icons.check,
                      size: 36,
                    ),
                  ),
                const SizedBox(width: 27),
              ],
            ),
            body: Stack(
              children: [
                AppBlockWidget.normal(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        _phoneValidateCard(),
                        _nickNameCard(),
                        _thumbnailCard(context, widget.provider, provider),
                        _coverImageCard(context, widget.provider, provider),
                        _deleteAccountButton(context),
                        const CopyRightCard()
                      ],
                    ),
                  ),
                ),
                if (provider.state == GeneralState.loading)
                  Container(
                    width: double.maxFinite,
                    height: double.maxFinite,
                    color: Colors.white54,
                    child: const Center(
                      child: CircularProgressIndicator(
                        color: AppColors.themeColor,
                      ),
                    ),
                  )
              ],
            ),
          );
        }));
  }

  Widget _phoneValidateCard() {
    return LightCoralCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppText.black('手機驗證', fontSize: 16),
          const SizedBox(
            height: 23,
          ),
          Row(
            children: [
              AppText.normal(Preferences.getString(Constants.phone, '')),
              // AppText.normal('0916722533'),
              const Spacer(),
              AppText.normal('已驗證'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _nickNameCard() {
    return LightCoralCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              AppText.black('暱稱', fontSize: 16),
              const Spacer(),
              EditUserPageButton(
                '編輯',
                onPressed: () {
                  setState(() {
                    isEditName = true;
                  });
                },
              )
            ],
          ),
          const SizedBox(
            height: 23,
          ),
          AppInput.editNickname(
              controller: _nickname,
              enabled: isEditName,
              keyboardType: TextInputType.text,
              hintText: widget.provider.userModel.name,
              onChanged: (String text) {
                print(_nickname);
              })
        ],
      ),
    );
  }

  Widget _thumbnailCard(BuildContext context, UserPageProvider provider,
      UpdateUserDataProvider updateProvider) {
    return LightCoralCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              AppText.black('用戶照片', fontSize: 16),
              const Spacer(),
              EditUserPageButton(
                '編輯',
                onPressed: () {
                  isEditAvatar = true;
                  Utils.pickImageAndCrop(
                    context,
                    onSuccess: (List<XFile> pickImages) async {
                      avatarFile = pickImages[0];
                      await updateProvider.updateData(avatar: avatarFile);
                      await widget.provider.fetchUserData(
                        phone: Preferences.getString(Constants.phone, ''),
                        password: Preferences.getString(Constants.password, ''),
                      );
                      userModel.avatarPhoto = avatarFile!.path;
                      setState(() {});
                    },
                    isProfile: true,
                  );
                },
              )
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          if (isEditAvatar == true)
            EditProfilePhotoLayout(
              image: avatarFile?.path ?? '',
            )
          else if (isEditAvatar == false &&
              widget.provider.userModel.avatarPhoto != '')
            NetProfilePhotoLayout(
              image: widget.provider.userModel.avatarPhoto,
            )
          else
            const DefaultProfilePhotoLayout(
              image: 'assets/images/ic_appbar_user_active.png',
            )
        ],
      ),
    );
  }

  Widget _coverImageCard(BuildContext context, UserPageProvider provider,
      UpdateUserDataProvider updateProvider) {
    return LightCoralCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              AppText.black('封面照片', fontSize: 16),
              const Spacer(),
              EditUserPageButton(
                '編輯',
                onPressed: () {
                  Utils.pickImageAndCrop(
                    context,
                    onSuccess: (List<XFile> pickImages) async {
                      isEditBackground = true;
                      backgroundFile = pickImages[0];
                      await updateProvider.updateData(
                          background: backgroundFile);
                      await widget.provider.fetchUserData(
                        phone: Preferences.getString(Constants.phone, ''),
                        password: Preferences.getString(Constants.password, ''),
                      );
                      userModel.backgroundImage = backgroundFile!.path;
                      setState(() {});
                    },
                    isProfile: true,
                    isBackground: true,
                  );
                },
              )
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          if (isEditBackground == true)
            ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Image.file(
                io.File(backgroundFile!.path),
                height: 136,
                width: double.infinity,
                fit: BoxFit.fill,
              ),
            )
          else if (isEditBackground == false &&
              widget.provider.userModel.backgroundImage != '')
            ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Image.network(
                widget.provider.userModel.backgroundImage,
                height: 136,
                width: double.infinity,
                fit: BoxFit.fill,
              ),
            )
          else
            ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Image.asset(
                'assets/images/image_cover_pv.jpeg',
                height: 136,
                width: double.infinity,
                fit: BoxFit.fill,
              ),
            )
        ],
      ),
    );
  }

  Widget _deleteAccountButton(BuildContext context) {
    return LightCoralCard(
      child: DeleteAccountButton(
        '刪除帳號',
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => const DeleteAccountPage(
                url: 'https://www.treatrip.com/contacts/',
              ),
            ),
          );
          // Preferences.setString(Constants.cookie, '');
        },
      ),
    );
  }
}
