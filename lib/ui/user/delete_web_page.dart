import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DeleteAccountPage extends StatefulWidget {
  final String url;

  const DeleteAccountPage({Key? key, required this.url}) : super(key: key);

  @override
  State<DeleteAccountPage> createState() => _DeleteAccountPageState();
}

class _DeleteAccountPageState extends State<DeleteAccountPage> {
  GeneralState state = GeneralState.loading;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '刪除帳號',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
      ),
      body: Stack(
        children: [
          WebView(
            initialUrl: widget.url,
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (finish) {
              setState(() {
                state = GeneralState.finish;
              });
            },
          ),
          state == GeneralState.loading
              ? const Center(
                  child: CircularProgressIndicator(
                    color: AppColors.themeColor,
                  ),
                )
              : Stack(),
        ],
      ),
    );
  }
}
