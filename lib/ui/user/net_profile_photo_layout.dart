import 'package:flutter/material.dart';

import '../../res/resources.dart';

class NetProfilePhotoLayout extends StatelessWidget {
  final String image;

  const NetProfilePhotoLayout({Key? key, required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 68.0,
      backgroundColor: AppColors.white,
      child: CircleAvatar(
        backgroundColor: Colors.white,
        radius: 64.0,
        backgroundImage: NetworkImage(image),
      ),
    );
  }
}
