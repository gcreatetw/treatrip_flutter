import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

class AppText {
  static Text primary(
    String title, {
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    FontWeight? fontWeight,
    TextOverflow? overflow,
  }) {
    return normal(
      title,
      color: AppColors.themeColor,
      maxLines: maxLines,
      fontSize: fontSize,
      fontWeight: fontWeight,
      overflow: overflow,
    );
  }

  static Text white(
    String title, {
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    FontWeight? fontWeight,
    TextOverflow? overflow,
  }) {
    return normal(
      title,
      color: AppColors.white,
      maxLines: maxLines,
      fontSize: fontSize,
      fontWeight: fontWeight,
      overflow: overflow,
    );
  }

  static Text themeColor(
    String title, {
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    FontWeight? fontWeight,
    TextOverflow? overflow,
  }) {
    return normal(
      title,
      color: AppColors.themeColor,
      maxLines: maxLines,
      fontSize: fontSize,
      fontWeight: fontWeight,
      overflow: overflow,
    );
  }

  static Text carolinaCoral(
    String title, {
    TextAlign? textAlign,
  }) {
    return normal(
      title,
      fontSize: 24,
      fontFamily: 'Roboto-Bold',
      color: AppColors.themeColor,
      fontWeight: FontWeight.w700,
    );
  }

  static Text black(
    String title, {
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    TextOverflow? overflow,
  }) {
    return normal(
      title,
      color: AppColors.black,
      maxLines: maxLines,
      fontSize: fontSize,
      overflow: overflow,
    );
  }

  //Coupon
  static Text couponTitle(
    String title, {
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    TextOverflow? overflow,
    String fontFamily = 'NotoSansCJKtc-Medium',
  }) {
    return normal(
      title,
      color: AppColors.black,
      maxLines: maxLines,
      fontSize: fontSize,
      overflow: overflow,
    );
  }

  static Text couponSubTitle(
    String title, {
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    TextOverflow? overflow,
    String fontFamily = 'NotoSansCJKtc-Medium',
  }) {
    return normal(title,
        color: AppColors.themeColor,
        maxLines: maxLines,
        fontSize: fontSize,
        overflow: overflow,
        textAlign: textAlign);
  }

  static Text error(
    String title, {
    TextAlign? textAlign,
  }) {
    return normal(
      title,
      color: AppColors.themeColor,
      fontSize: AppDimens.fontErrorHint,
    );
  }

  static Text normalDisabled(String title) {
    return white(title);
  }

  static Text number(
    String title, {
    int? maxLines,
    Color color = AppColors.themeColor,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    FontWeight? fontWeight = FontWeight.w500,
    TextOverflow? overflow,
  }) {
    return normal(
      title,
      fontSize: fontSize,
      fontFamily: 'Roboto',
      color: color,
      fontWeight: fontWeight,
    );
  }

  static Text normal(
    String title, {
    Color color = AppColors.black54,
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    FontWeight? fontWeight = FontWeight.w500,
    TextOverflow? overflow,
    String fontFamily = 'NotoSansCJKtc',
  }) {
    return Text(
      title,
      maxLines: maxLines,
      textAlign: textAlign,
      style: TextStyle(
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
        overflow: overflow,
        fontFamily: fontFamily,
      ),
    );
  }

  static Text userName(
    String title, {
    Color color = AppColors.black87,
    int? maxLines,
    TextAlign? textAlign,
    double fontSize = AppDimens.fontNormal,
    FontWeight? fontWeight = FontWeight.w500,
    TextOverflow? overflow,
    String fontFamily = 'NotoSansCJKtc',
  }) {
    return Text(
      title,
      maxLines: maxLines,
      textAlign: textAlign,
      style: TextStyle(
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
        overflow: overflow,
        fontFamily: fontFamily,
      ),
    );
  }
}
