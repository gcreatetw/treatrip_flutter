import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:treatrip/res/resources.dart';

class AppInput {
  static Widget primary({
    TextEditingController? controller,
    TextInputType? keyboardType,
    TextAlign textAlign = TextAlign.start,
    String? hintText,
    bool obscureText = false,
    FocusNode? focusNode,
    FormFieldValidator<String>? validator,
    List<TextInputFormatter>? inputFormatters,
    VoidCallback? onEditPressed,
    ValueChanged<String>? onChanged,
    TextInputAction? textInputAction,
    VoidCallback? onEditingComplete,
    Widget? suffixIcon,
  }) {
    return TextFormField(
      controller: controller,
      keyboardType: keyboardType,
      textAlign: textAlign,
      obscureText: obscureText,
      onChanged: onChanged,
      focusNode: focusNode,
      validator: validator,
      inputFormatters: inputFormatters,
      textInputAction: textInputAction,
      onEditingComplete: onEditingComplete,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: const TextStyle(
          fontSize: AppDimens.fontHint,
          color: AppColors.black38,
        ),
        border: AppTextStyle.inputDefBorder,
        focusedBorder: AppTextStyle.inputDefBorder,
        contentPadding: const EdgeInsets.all(
          AppDimens.inputPadding,
        ),
        suffixIcon: suffixIcon,
      ),
    );
  }

  static Widget editNickname({
    bool enabled = false,
    TextEditingController? controller,
    TextInputType? keyboardType,
    TextAlign textAlign = TextAlign.start,
    String? hintText,
    final bool obscureText = false,
    FocusNode? focusNode,
    FormFieldValidator<String>? validator,
    List<TextInputFormatter>? inputFormatters,
    VoidCallback? onEditPressed,
    ValueChanged<String>? onChanged,
    TextInputAction? textInputAction,
    VoidCallback? onEditingComplete,
    Widget? suffixIcon,
    final Color? fillColor,
  }) {
    return TextFormField(
      enabled: enabled,
      controller: controller,
      keyboardType: keyboardType,
      textAlign: textAlign,
      obscureText: obscureText,
      onChanged: onChanged,
      focusNode: focusNode,
      validator: validator,
      inputFormatters: inputFormatters,
      textInputAction: textInputAction,
      onEditingComplete: onEditingComplete,
      decoration: InputDecoration(
          hintText: hintText,
          hintStyle: const TextStyle(
            fontSize: AppDimens.fontHint,
            color: AppColors.black,
          ),
          border: AppTextStyle.inputDefBorder,
          focusedBorder: AppTextStyle.inputDefBorder,
          contentPadding: const EdgeInsets.all(
            AppDimens.inputPadding,
          ),
          suffixIcon: suffixIcon,
          hoverColor: AppColors.themeColor,
          filled: true,
          fillColor: fillColor ??
              (enabled ? AppColors.transparent : AppColors.nameFieldColor)),
    );
  }
}
