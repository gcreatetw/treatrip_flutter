import 'package:flutter/material.dart';
import 'package:treatrip/models/data/store_attraction_data.dart';
import 'package:treatrip/res/resources.dart';

class StoreInformationPage extends StatefulWidget {
  final List<StoreInformationData> data;

  const StoreInformationPage({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  _StoreInformationPageState createState() => _StoreInformationPageState();
}

class _StoreInformationPageState extends State<StoreInformationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '店家資訊',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: Colors.white,
              child: ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, int index) {
                  return StoreFeatureCard(
                    data: widget.data[index],
                  );
                },
                separatorBuilder: (context, int index) {
                  return const Divider(
                    color: Color.fromRGBO(0, 0, 0, 0.12),
                  );
                },
                itemCount: widget.data.length,
              ),
            ),
            const SizedBox(height: 56.0),
            const Text(
              '趣趣',
              style: TextStyle(color: AppColors.themeColor),
            ),
            const SizedBox(height: 9.0),
            const Text(
              '© Copyright 2020 TRETRIP. All rights reserved.',
              style: TextStyle(
                color: Color(0x61000000),
                fontWeight: FontWeight.w400,
                fontFamily: "Roboto",
                fontStyle: FontStyle.normal,
                fontSize: 8.0,
              ),
            ),
            const SizedBox(
              height: 24.0,
            ),
          ],
        ),
      ),
    );
  }
}

class StoreFeatureCard extends StatelessWidget {
  final StoreInformationData data;

  const StoreFeatureCard({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 7.0),
          Text(
            data.title,
            style: const TextStyle(
              color: Color(0x61000000),
              fontWeight: FontWeight.w400,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 13.0,
            ),
          ),
          GridView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 4.0,
            ),
            physics: const NeverScrollableScrollPhysics(),
            itemCount: data.featureList.length,
            itemBuilder: (context, int index) {
              return Row(
                children: [
                  Icon(
                    data.featureList[index].has
                        ? Icons.check_outlined
                        : Icons.block_flipped,
                    color: data.featureList[index].has
                        ? Colors.black
                        : const Color.fromRGBO(0, 0, 0, 0.5),
                    size: 16.0,
                  ),
                  const SizedBox(width: 6.0),
                  Text(
                    data.featureList[index].name,
                    style: TextStyle(
                      color: data.featureList[index].has
                          ? Colors.black
                          : const Color.fromRGBO(0, 0, 0, 0.5),
                      fontWeight: FontWeight.w500,
                      fontFamily: "NotoSansCJKtc",
                      fontStyle: FontStyle.normal,
                      fontSize: 12.0,
                    ),
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
