import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/store_attraction_data.dart';
import 'package:treatrip/models/text_enum.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/food/complete_information_page.dart';
import 'package:treatrip/ui/store/store_image_page.dart';
import 'package:treatrip/ui/store/store_information_page.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/widget/store_information_card.dart';
import 'package:share_plus/share_plus.dart';
import 'package:latlong2/latlong.dart';
import 'package:url_launcher/url_launcher.dart';

class StoreAttractionPage extends StatefulWidget {
  final int? postId;

  const StoreAttractionPage({Key? key, this.postId}) : super(key: key);

  @override
  _StoreAttractionPageState createState() => _StoreAttractionPageState();
}

class _StoreAttractionPageState extends State<StoreAttractionPage> {
  final StoreAttractionData storeInformationData = StoreAttractionData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      //storeInformationData.fetch();
      storeInformationData.fetchDetailPageData(post_id: widget.postId ?? 0);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double toolBarHeight = MediaQuery.of(context).padding.top;
    final double imageWidth = (width - 122) / 3;
    String webLink = '';

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerTop,
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(top: 12),
        child: Row(
          children: [
            const SizedBox(width: 16),
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 24.0,
                height: 24.0,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: const Icon(
                  Icons.arrow_back,
                  size: 16.0,
                  color: AppColors.themeColor,
                ),
              ),
            ),
            const Expanded(child: SizedBox()),
            InkWell(
              onTap: () {
                //TODO 更換正式外連
                Share.share(webLink);
              },
              child: Container(
                width: 24.0,
                height: 24.0,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: const Icon(
                  Icons.share_outlined,
                  size: 16.0,
                  color: AppColors.themeColor,
                ),
              ),
            ),
            const SizedBox(width: 16)
          ],
        ),
      ),
      body: MultiProvider(
        providers: [ChangeNotifierProvider.value(value: storeInformationData)],
        child: Consumer(
          builder: (BuildContext context, StoreAttractionData data, _) {
            webLink = data.weblink;
            String price = '';
            for (int i = 0; i < data.storePrice; i++) {
              price += '\$';
            }
            return StateController(
              state: data.state,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Image.network(
                          data.storeBannerUrl,
                          width: width,
                          height: 374,
                          fit: BoxFit.cover,
                        ),
                        Positioned(
                          left: 16.0,
                          bottom: 8.0,
                          child: Row(
                            children: [
                              if (data.storeImageUrlList.length > 2)
                                for (int i = 1;
                                    i <
                                        (data.storeImageUrlList.length > 5
                                            ? 5
                                            : data.storeImageUrlList.length);
                                    i++)
                                  Container(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Image.network(
                                        data.storeImageUrlList[i].url),
                                    width: imageWidth,
                                    height: imageWidth,
                                  )
                            ],
                          ),
                        ),
                        Positioned(
                          right: 16.0,
                          bottom: 8.0,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) => StoreImagePage(
                                    imageTitleList: data.imageTitleList,
                                  ),
                                ),
                              );
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: const [
                                Icon(
                                  Icons.apps,
                                  color: Colors.white,
                                ),
                                Text(
                                  '查看全部',
                                  style: TextStyle(
                                    color: Color(0xffffffff),
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "NotoSansCJKtc",
                                    fontStyle: FontStyle.normal,
                                    fontSize: 10.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    Container(color: Colors.white, height: 10.0),
                    textBox(
                      null,
                      TextType.custom,
                      Text(
                        data.storeName,
                        style: const TextStyle(
                          color: Color(0xff000000),
                          fontWeight: FontWeight.w700,
                          fontFamily: "NotoSansCJKtc",
                          fontStyle: FontStyle.normal,
                          fontSize: 21.0,
                        ),
                      ),
                    ),
                    Container(color: Colors.white, height: 7.0),
                    Container(
                      color: Colors.white,
                      child: Row(
                        children: [
                          const SizedBox(width: 16.0),
                          Text(
                            data.storeStartCount,
                            style: const TextStyle(
                              color: Color(0xffed7465),
                              fontWeight: FontWeight.w500,
                              fontFamily: "Roboto",
                              fontStyle: FontStyle.normal,
                              fontSize: 12.0,
                            ),
                          ),
                          const SizedBox(width: 4.0),
                          // for (int index = 0; index < 5; index++)
                          //   if (double.parse(data.storeStartCount) - index >
                          //       1) ...[
                          //     const Icon(
                          //       Icons.star,
                          //       color: AppColors.starColor,
                          //       size: 10,
                          //     ),
                          //   ] else if (double.parse(data.storeStartCount) -
                          //               index <
                          //           1 &&
                          //       double.parse(data.storeStartCount) - index >
                          //           0) ...[
                          //     const Icon(
                          //       Icons.star_half,
                          //       color: AppColors.starColor,
                          //       size: 10,
                          //     ),
                          //   ] else ...[
                          //     const Icon(
                          //       Icons.star,
                          //       color: Color(0x1f000000),
                          //       size: 10,
                          //     ),
                          //   ],
                          const SizedBox(width: 5.0),
                          Text(
                            '(${data.storeCommentCount}) ${String.fromCharCode(0x00B7)} $price',
                            style: const TextStyle(
                              color: Color(0x61000000),
                              fontWeight: FontWeight.w500,
                              fontFamily: "Roboto",
                              fontStyle: FontStyle.normal,
                              fontSize: 12.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(color: Colors.white, height: 4.0),
                    textBox(data.storeAddress, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 4.0),
                    textBox(data.storeType, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 8.0),
                    textBox(
                      null,
                      TextType.custom,
                      Text(
                        data.storeOpenTime,
                        style: const TextStyle(
                          color: Color(0xde000000),
                          fontWeight: FontWeight.w500,
                          fontFamily: "NotoSansCJKtc",
                          fontStyle: FontStyle.normal,
                          fontSize: 13.0,
                        ),
                      ),
                    ),
                    Container(color: Colors.white, height: 10.0),
                    Container(
                      color: Colors.white,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const SizedBox(width: 16.0),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute<void>(
                                    builder: (_) => StoreInformationPage(
                                        data: data.storeInformationData),
                                  ),
                                );
                              },
                              child: const Text(
                                '查看店家資訊',
                                style: TextStyle(
                                  color: Color(0xff4293ee),
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "NotoSansCJKtc",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 15.0,
                                ),
                              ),
                            ),
                          ),
                          Text(
                            '${NumberFormat().format(data.storeViewed)} 個瀏覽',
                            style: const TextStyle(
                              color: Color(0x61000000),
                              fontWeight: FontWeight.w400,
                              fontFamily: "Roboto",
                              fontStyle: FontStyle.normal,
                              fontSize: 10.0,
                            ),
                          ),
                          const SizedBox(width: 16.0),
                        ],
                      ),
                    ),
                    Container(color: Colors.white, height: 12.0),
                    textBox('店家資訊', TextType.smallGrey, _),
                    Container(color: Colors.white, height: 5.0),
                    Container(
                      width: width,
                      height: 164,
                      child: ClipRRect(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(16.0),
                        ),
                        child: FlutterMap(
                          options: MapOptions(
                            center: data.latLng,
                            zoom: 15.0,
                          ),
                          layers: <LayerOptions>[
                            TileLayerOptions(
                              urlTemplate:
                                  'https://mt0.google.com/vt/lyrs=m@221097413,transit&hl=zh-TW&x={x}&y={y}&z={z}',
                              subdomains: <String>['a', 'b', 'c'],
                              attributionBuilder: (_) {
                                return const Text('© Google Map');
                              },
                            ),
                            MarkerLayerOptions(
                              markers: <Marker>[
                                Marker(
                                  width: 80.0,
                                  height: 80.0,
                                  point: data.latLng,
                                  builder: (_) => const Icon(
                                    Icons.location_on,
                                    color: Colors.redAccent,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(color: Colors.white, height: 2.0),
                    textBox(data.storeAddress, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 11.0),
                    Container(
                      color: Colors.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  _makePhoneCall(data.contactPhoneNumber);
                                },
                                child: Container(
                                  width: 40.0,
                                  height: 40.0,
                                  padding: const EdgeInsets.all(9.0),
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color.fromRGBO(0, 0, 0, 0.06),
                                  ),
                                  child: const Icon(
                                    Icons.phone_outlined,
                                    color: AppColors.themeColor,
                                  ),
                                ),
                              ),
                              const Text(
                                '聯絡店家',
                                style: TextStyle(
                                  color: Color(0xde000000),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "NotoSansCJKtc",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 11.0,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  launch(
                                      'https://www.google.com/maps/dir//${Uri.parse(data.storeAddress)}');
                                },
                                child: Container(
                                  width: 40.0,
                                  height: 40.0,
                                  padding: const EdgeInsets.all(9.0),
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color.fromRGBO(0, 0, 0, 0.06),
                                  ),
                                  child: const Icon(
                                    Icons.directions_outlined,
                                    color: AppColors.themeColor,
                                  ),
                                ),
                              ),
                              const Text(
                                '查看路線',
                                style: TextStyle(
                                  color: Color(0xde000000),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "NotoSansCJKtc",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 11.0,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(color: Colors.white, height: 16.0),
                    textBox('支付方式', TextType.normalBlack, _),
                    textBox(data.payment, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 14.0),
                    textBox('網站', TextType.normalBlack, _),
                    textBox(data.webUrl, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 14.0),
                    textBox('連絡電話', TextType.normalBlack, _),
                    textBox(data.contactPhoneNumber, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 14.0),
                    textBox('預約電話', TextType.normalBlack, _),
                    textBox(data.reservePhoneNumber, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 14.0),
                    textBox('菜單網址', TextType.normalBlack, _),
                    textBox(data.menuUrl, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 14.0),
                    textBox('預定網址', TextType.normalBlack, _),
                    textBox(data.reserveUrl, TextType.smallGrey, _),
                    Container(color: Colors.white, height: 31.0),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      width: double.maxFinite,
                      color: Colors.white,
                      child: OutlinedButton(
                        onPressed: () {
                          // Navigator.of(context).push(
                          //   MaterialPageRoute<void>(
                          //     builder: (_) => StoreAttractionPage(),
                          //   ),
                          // );
                          Navigator.of(context).push(
                            MaterialPageRoute<void>(
                              builder: (_) => CompleteInformationPage(
                                latLng: data.latLng,
                                storeName: data.storeName,
                                storeAddress: data.storeAddress,
                                storeOpeningDay: data.storeOpeningDay,
                                storeIntroduction: data.storeIntroduction,
                                payment: data.payment,
                                webUrl: data.webUrl,
                                contactPhoneNumber: data.contactPhoneNumber,
                                reservePhoneNumber: data.reservePhoneNumber,
                                menuUrl: data.menuUrl,
                                reserveUrl: data.reserveUrl,
                                businessHours: data.storeBusinessHours,
                              ),
                            ),
                          );
                        },
                        style: OutlinedButton.styleFrom(
                          side: const BorderSide(
                            color: AppColors.themeColor,
                          ),
                          primary: AppColors.themeColor,
                        ),
                        child: const Text('查看完整資訊',
                            style: TextStyle(
                                color: Color(0xffed7465),
                                fontWeight: FontWeight.w500,
                                fontFamily: "NotoSansCJKtc",
                                fontStyle: FontStyle.normal,
                                fontSize: 15.0),
                            textAlign: TextAlign.center),
                      ),
                    ),
                    Container(color: Colors.white, height: 8.0),
                    //TODO 先移除
                    // const SizedBox(height: 24.0),
                    // Padding(
                    //   padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       const Text(
                    //         '為你推薦',
                    //         style: TextStyle(
                    //           color: Color(0xde000000),
                    //           fontWeight: FontWeight.w500,
                    //           fontFamily: "NotoSansCJKtc",
                    //           fontStyle: FontStyle.normal,
                    //           fontSize: 15.0,
                    //         ),
                    //       ),
                    //       GestureDetector(
                    //         onTap: () {},
                    //         child: const Text(
                    //           '查看全部',
                    //           style: TextStyle(
                    //             color: Color(0xffed7465),
                    //             fontWeight: FontWeight.w500,
                    //             fontFamily: "NotoSansCJKtc",
                    //             fontStyle: FontStyle.normal,
                    //             fontSize: 13.0,
                    //           ),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    // const SizedBox(height: 13.0),
                    // SizedBox(
                    //   height: 154,
                    //   child: ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: data.recommendData.length,
                    //     itemBuilder: (_, int index) {
                    //       return Padding(
                    //           padding: EdgeInsets.only(
                    //               left: index == 0 ? 16.0 : 4.0,
                    //               right: index == data.recommendData.length - 1
                    //                   ? 16.0
                    //                   : 4.0),
                    //           child: StoreInformationCard(
                    //               data: data.recommendData[index]));
                    //     },
                    //   ),
                    // ),
                    const SizedBox(height: 56.0),
                    const Text(
                      '趣趣',
                      style: TextStyle(color: AppColors.themeColor),
                    ),
                    const SizedBox(height: 9.0),
                    const Text(
                      '© Copyright 2020 TRETRIP. All rights reserved.',
                      style: TextStyle(
                        color: Color(0x61000000),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Roboto",
                        fontStyle: FontStyle.normal,
                        fontSize: 8.0,
                      ),
                    ),
                    const SizedBox(
                      height: 24.0,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget textBox(String? text, TextType type, Widget? myText) {
    final Widget child;
    switch (type) {
      case TextType.normalBlack:
        child = Text(
          text!,
          style: const TextStyle(
            color: Color(0xde000000),
            fontWeight: FontWeight.w500,
            fontFamily: "NotoSansCJKtc",
            fontStyle: FontStyle.normal,
            fontSize: 15.0,
          ),
        );
        break;
      case TextType.smallGrey:
        child = Text(
          text!,
          style: const TextStyle(
            color: Color(0x61000000),
            fontWeight: FontWeight.w400,
            fontFamily: "NotoSansCJKtc",
            fontStyle: FontStyle.normal,
            fontSize: 13.0,
          ),
        );
        break;
      case TextType.custom:
        child = myText!;
        break;
    }
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      alignment: Alignment.centerLeft,
      child: child,
    );
  }

  Future<void> _makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }

//Future<void> _getWebLink(String link, Provider provider) async {}
}
