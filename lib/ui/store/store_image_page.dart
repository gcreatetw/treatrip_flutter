import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/store_attraction_data.dart';
import 'package:treatrip/models/data/store_image_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/data_construction.dart';
import 'package:treatrip/widget/state_controller.dart';

class StoreImagePage extends StatefulWidget {
  final List<ImageTitle> imageTitleList;

  const StoreImagePage({
    Key? key,
    required this.imageTitleList,
  }) : super(key: key);

  @override
  _StoreImagePageState createState() => _StoreImagePageState();
}

class _StoreImagePageState extends State<StoreImagePage> {
  StoreImageData storeImageData = StoreImageData.init();

  final ScrollController _controller = ScrollController();

  int currentIndex = 0;

  double nowOffset = 0.0;

  @override
  void initState() {
    Future<void>.microtask(() async {
      storeImageData.fetch();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '店家照片',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
      ),
      body: storeImageData.imageList.isEmpty
      //TODO 先隱藏
          ? const DataConstruction()
          : NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    elevation: 0,
                    backgroundColor: Colors.white,
                    pinned: true,
                    floating: false,
                    expandedHeight: 140,
                    collapsedHeight: 54,
                    toolbarHeight: 0.0,
                    flexibleSpace: ListView.builder(
                      controller: _controller,
                      scrollDirection: Axis.horizontal,
                      itemCount: widget.imageTitleList.length,
                      itemBuilder: (context, int index) {
                        double padding = 0;
                        if (currentIndex == index) {
                          padding = 3;
                        }
                        return GestureDetector(
                          onTap: () {
                            if (currentIndex != index) {
                              setState(() {
                                currentIndex = index;
                                storeImageData.fetch();

                                double indexOffsetRight =
                                    8.0 * (index + 2) + 98.0 * (index + 1);
                                double indexOffsetLeft =
                                    8.0 * (index) + 98.0 * (index);
                                nowOffset =
                                    Scrollable.of(context)!.position.pixels;

                                if (nowOffset + width < indexOffsetRight) {
                                  _controller.animateTo(
                                      8.0 * (index + 2) +
                                          98.0 * (index + 1) -
                                          width,
                                      duration:
                                          const Duration(milliseconds: 300),
                                      curve: Curves.ease);
                                } else if (nowOffset > indexOffsetLeft) {
                                  _controller.animateTo(
                                      8.0 * (index) + 98.0 * (index),
                                      duration:
                                          const Duration(milliseconds: 300),
                                      curve: Curves.ease);
                                }
                              });
                            }
                          },
                          child: Padding(
                            padding: EdgeInsets.only(
                              left: index == 0 ? 8.0 - padding : 4.0 - padding,
                              right: index == 4 ? 8.0 - padding : 4.0 - padding,
                              top: 8.0 - padding,
                              bottom: 8.0 - padding,
                            ),
                            child: Container(
                              decoration: currentIndex == index
                                  ? BoxDecoration(
                                      border: Border.all(
                                          color: Colors.blue, width: 3.0),
                                      borderRadius: BorderRadius.circular(4.0),
                                    )
                                  : null,
                              child: ImageCard(
                                imageUrl: widget.imageTitleList[index].imageUrl,
                                name: widget.imageTitleList[index].name,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SliverAppBar(
                    elevation: 0,
                    backgroundColor: Colors.white,
                    pinned: true,
                    floating: false,
                    expandedHeight: 1,
                    collapsedHeight: 1,
                    toolbarHeight: 0.0,
                    flexibleSpace: Container(
                      height: 1,
                      color: const Color.fromRGBO(0, 0, 0, 0.12),
                    ),
                  ),
                ];
              },
              body: MultiProvider(
                providers: [
                  ChangeNotifierProvider.value(value: storeImageData)
                ],
                child: Consumer(
                  builder: (context, StoreImageData data, _) {
                    return StateController(
                      state: data.state,
                      child: SingleChildScrollView(
                        child: Container(
                          color: Colors.white,
                          child: Column(
                            children: [
                              const SizedBox(height: 12.0),
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                    '${widget.imageTitleList[currentIndex].name}相片',
                                    style: const TextStyle(
                                        color: Color(0xde000000),
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "NotoSansCJKtc",
                                        fontStyle: FontStyle.normal,
                                        fontSize: 13.0)),
                              ),
                              const SizedBox(height: 12.0),
                              StaggeredGridView.countBuilder(
                                padding: const EdgeInsets.all(4.0),
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: data.imageList.length,
                                crossAxisCount: 2,
                                crossAxisSpacing: 4.0,
                                mainAxisSpacing: 4.0,
                                itemBuilder: (context, int index) {
                                  return Image.network(
                                    data.imageList[index],
                                    width: 100,
                                  );
                                },
                                staggeredTileBuilder: (index) =>
                                    const StaggeredTile.fit(1),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
    );
  }
}

class ImageCard extends StatelessWidget {
  final String imageUrl;
  final String name;

  const ImageCard({
    Key? key,
    required this.name,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(3.0),
          child: Image.network(
            imageUrl,
            fit: BoxFit.cover,
            width: 98,
            height: 140,
          ),
        ),
        Positioned(
          child: Text(
            name,
            style: const TextStyle(
              color: Color(0xffffffff),
              fontWeight: FontWeight.w700,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 13.0,
            ),
          ),
          left: 12.0,
          bottom: 9.0,
        ),
      ],
    );
  }
}
