import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/search_result_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/store/store_attraction_page.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/widget/store_information_title.dart';

import '../../models/data/food_page_data.dart';

enum SearchType {
  food,
  view,
  hotel,
}

class SearchResultPage extends StatefulWidget {
  final String query;

  const SearchResultPage({
    Key? key,
    required this.query,
  }) : super(key: key);

  @override
  _SearchResultPageState createState() => _SearchResultPageState();
}

class _SearchResultPageState extends State<SearchResultPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  SearchResultData searchResultData = SearchResultData.init();

  int currentIndex = 0;

  bool isAdvance = false;

  bool mapMod = false;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    Future<void>.microtask(() async {
      searchResultData.fetchSearchData();
    });
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          widget.query,
          style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
      ),
      body: Container(
        alignment: Alignment.topCenter,
        color: Colors.white,
        height: double.maxFinite,
        child: MultiProvider(
          providers: [
            ChangeNotifierProvider.value(
              value: searchResultData,
            ),
          ],
          child: Consumer(
            builder: (context, SearchResultData data, _) {
              return Stack(
                children: [
                  TabBarView(
                    physics:
                        isAdvance ? const NeverScrollableScrollPhysics() : null,
                    controller: _tabController,
                    children: [
                      advanceSearch(SearchType.food),
                      advanceSearch(SearchType.view),
                      advanceSearch(SearchType.hotel),
                    ],
                  ),
                  AnimatedSwitcher(
                    duration: const Duration(milliseconds: 200),
                    transitionBuilder:
                        (Widget child, Animation<double> animation) {
                      return SlideTransition(
                        position: animation.drive(
                          Tween(
                            begin: const Offset(0.0, -1.0),
                            end: Offset.zero,
                          ),
                        ),
                        child: child,
                      );
                    },
                    child: isAdvance ? chipGroup(width) : _tabBar,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Container get _tabBar => Container(
        key: const Key('tabBar'),
        color: Colors.white,
        height: 50.0,
        child: TabBar(
          controller: _tabController,
          indicatorColor: AppColors.themeColorFix,
          unselectedLabelColor: const Color.fromRGBO(0, 0, 0, 0.87),
          labelColor: AppColors.themeColor,
          tabs: const [
            Tab(
              text: '美食',
            ),
            Tab(
              text: '景點',
            ),
            Tab(
              text: '住宿',
            ),
          ],
        ),
      );

  Widget chipGroup(double width) {
    return Container(
      key: const Key('chipGroup'),
      color: mapMod ? Colors.transparent : Colors.white,
      child: Stack(
        children: [
          Container(
            height: 50.0,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(vertical: 6.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const SizedBox(width: 16.0),
                  ActionChip(
                    onPressed: () {},
                    label: const Icon(
                      Icons.tune_outlined,
                      size: 16.0,
                      color: Color.fromRGBO(0, 0, 0, 0.38),
                    ),
                    shape: const CircleBorder(),
                    backgroundColor: Colors.white,
                    elevation: 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: ChoiceChip(
                      onSelected: (select) {
                        // setState(() {
                        //   searchResultData.categoryList[0].selected =
                        //       searchResultData.categoryList[0].selected
                        //           ? false
                        //           : true;
                        // });
                      },
                      selected: searchResultData.categoryList[0].selected,
                      selectedColor: AppColors.themeColor,
                      label: Row(
                        children: [
                          Icon(
                            Icons.location_on_outlined,
                            size: 16.0,
                            color: searchResultData.categoryList[0].selected
                                ? Colors.white
                                : const Color.fromRGBO(0, 0, 0, 0.38),
                          ),
                          Text('新竹市',
                              style: TextStyle(
                                  color:
                                      searchResultData.categoryList[0].selected
                                          ? Colors.white
                                          : const Color.fromRGBO(0, 0, 0, 0.38),
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "NotoSansCJKtc",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 13.0)),
                          Icon(
                            Icons.arrow_drop_down,
                            size: 16.0,
                            color: searchResultData.categoryList[0].selected
                                ? Colors.white
                                : const Color.fromRGBO(0, 0, 0, 0.38),
                          )
                        ],
                      ),
                      backgroundColor: Colors.white,
                      elevation: 2,
                    ),
                  ),
                  for (int i = 1; i < searchResultData.categoryList.length; i++)
                    Padding(
                      padding: EdgeInsets.only(
                          right: i == searchResultData.categoryList.length - 1
                              ? 16.0
                              : 8.0),
                      child: ChoiceChip(
                        onSelected: (select) {
                          setState(() {
                            searchResultData.categoryList[i].selected =
                                searchResultData.categoryList[i].selected
                                    ? false
                                    : true;
                          });
                        },
                        selected: searchResultData.categoryList[i].selected,
                        selectedColor: AppColors.themeColor,
                        label: Row(
                          children: [
                            Text(
                              searchResultData.categoryList[i].title,
                              style: TextStyle(
                                color: searchResultData.categoryList[i].selected
                                    ? Colors.white
                                    : const Color.fromRGBO(0, 0, 0, 0.38),
                                fontWeight: FontWeight.w500,
                                fontFamily: "NotoSansCJKtc",
                                fontStyle: FontStyle.normal,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                        backgroundColor: Colors.white,
                        elevation: 2,
                      ),
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget searchResultList(SearchType searchType) {
    return Consumer(
      builder: (context, SearchResultData data, _) {
        List<SearchResultTitleData> list;

        switch (searchType) {
          case SearchType.food:
            list = data.foodList;
            break;
          case SearchType.view:
            list = data.viewList;
            break;
          case SearchType.hotel:
            list = data.hotelList;
            break;
        }

        return Column(
          children: [
            const SizedBox(
              height: 50.0,
            ),
            searchResultCount(list.length),
            Expanded(
              child: StateController(
                state: data.state,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: list.length,
                  itemBuilder: (context, int index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: SearchResultTitle(
                        data: list[index],
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget advanceSearch(SearchType searchType) {
    final double width = MediaQuery.of(context).size.width;

    return Consumer(
      builder: (context, SearchResultData data, _) {
        return StateController(
          state: data.state,
          child: AnimatedSwitcher(
            duration: const Duration(milliseconds: 200),
            transitionBuilder: (Widget child, Animation<double> animation) {
              return SlideTransition(
                position: animation.drive(
                  Tween(
                    begin: const Offset(0.0, 1.0),
                    end: const Offset(0.0, 0.0),
                  ),
                ),
                child: child,
              );
            },
            child: mapMod
                ? mapModList(width, searchType)
                : searchResultList(searchType),
          ),
        );
      },
    );
  }

  Widget mapModList(double width, SearchType searchType) {
    return Consumer(
      key: const Key('mapModList'),
      builder: (context, SearchResultData data, _) {
        List<SearchResultTitleData> list;

        switch (searchType) {
          case SearchType.food:
            list = data.foodList;
            break;
          case SearchType.view:
            list = data.viewList;
            break;
          case SearchType.hotel:
            list = data.hotelList;
            break;
        }

        return Container(
          alignment: Alignment.topCenter,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  key: const Key('map'),
                  width: width,
                  height: width,
                  color: Colors.white,
                  child: FlutterMap(
                    options: MapOptions(
                      center: LatLng(25.03, 121.53),
                      zoom: 15.0,
                    ),
                    layers: <LayerOptions>[
                      TileLayerOptions(
                        urlTemplate:
                            'https://mt0.google.com/vt/lyrs=m@221097413,transit&hl=zh-TW&x={x}&y={y}&z={z}',
                        subdomains: <String>['a', 'b', 'c'],
                        attributionBuilder: (_) {
                          return const Text('© Google Map');
                        },
                      ),
                      MarkerLayerOptions(
                          // markers: <Marker>[
                          //   if (provider.approximateLocationLatLng != null)
                          //     Marker(
                          //       width: 80.0,
                          //       height: 80.0,
                          //       point: provider.approximateLocationLatLng!,
                          //       builder: (_) => const Icon(
                          //         Icons.location_on,
                          //         color: AppColors.orangeRed,
                          //       ),
                          //     ),
                          // ],
                          ),
                    ],
                  ),
                ),
                searchResultCount(list.length),
                SizedBox(
                  height: 250,
                  child: PageView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: list.length,
                    itemBuilder: (context, int index) {
                      return StoreInformationTitle(
                        data: data.selectStoreData,
                        showStoreInformation: false,
                        clickable: true,
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget searchResultCount(int count) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      child: Row(
        children: [
          GestureDetector(
            onTap: isAdvance
                ? () {
                    setState(() {
                      isAdvance = false;
                      mapMod = false;
                    });
                  }
                : null,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (isAdvance)
                  const Icon(
                    Icons.arrow_back_ios_outlined,
                    color: AppColors.themeColor,
                    size: 16.0,
                  ),
                Text(
                  '${NumberFormat().format(count)} 筆搜尋結果',
                  style: TextStyle(
                    color: isAdvance
                        ? const Color(0xffed7465)
                        : const Color(0x61000000),
                    fontWeight: FontWeight.w500,
                    fontFamily: "NotoSansCJKtc",
                    fontStyle: FontStyle.normal,
                    fontSize: 13.0,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(),
          ),
          SizedBox(
            height: 24.0,
            child: GestureDetector(
              onTap: () {
                if (isAdvance) {
                  setState(() {
                    mapMod = mapMod ? false : true;
                  });
                } else {
                  setState(() {
                    isAdvance = true;
                  });
                }
              },
              child: isAdvance
                  ? Icon(
                      mapMod
                          ? Icons.format_list_bulleted_outlined
                          : Icons.map_outlined,
                      color: AppColors.themeColor,
                      size: 24.0,
                    )
                  : const Text(
                      '進階搜尋',
                      style: TextStyle(
                        color: AppColors.themeColor,
                        fontWeight: FontWeight.w500,
                        fontFamily: "NotoSansCJKtc",
                        fontStyle: FontStyle.normal,
                        fontSize: 14.0,
                      ),
                    ),
            ),
          )
        ],
      ),
    );
  }
}

class SearchResultTitle extends StatelessWidget {
  final SearchResultTitleData data;

  const SearchResultTitle({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String price = '';
    for (int i = 0; i < data.price; i++) {
      price += '\$';
    }

    //TODO 搜尋結果List
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (_) => StoreAttractionPage(
              postId: data.id,
            ),
          ),
        );
      },
      child: Row(
        children: [
          Image.network(
            data.imageUrl,
            height: 56.0,
            width: 56.0,
            fit: BoxFit.cover,
          ),
          const SizedBox(width: 8.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                data.storeName,
                style: const TextStyle(
                  color: Color(0xde000000),
                  fontWeight: FontWeight.w500,
                  fontFamily: "NotoSansCJKtc",
                  fontStyle: FontStyle.normal,
                  fontSize: 15.0,
                ),
              ),
              const SizedBox(height: 2.0),
              Row(
                children: [
                  Text(
                    data.stars.toString(),
                    style: const TextStyle(
                      color: Color(0xffed7465),
                      fontWeight: FontWeight.w500,
                      fontFamily: "Roboto",
                      fontStyle: FontStyle.normal,
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(width: 4.0),
                  for (int index = 0; index < 5; index++)
                    if (data.stars - index > 1) ...[
                      const Icon(
                        Icons.star,
                        color: AppColors.starColor,
                        size: 10,
                      ),
                    ] else if (data.stars - index < 1 &&
                        data.stars - index > 0) ...[
                      const Icon(
                        Icons.star_half,
                        color: AppColors.starColor,
                        size: 10,
                      ),
                    ] else ...[
                      const Icon(
                        Icons.star,
                        color: Color(0x1f000000),
                        size: 10,
                      ),
                    ],
                  const SizedBox(width: 5.0),
                  Text(
                    '(${data.comments}) ${String.fromCharCode(0x00B7)} $price',
                    style: const TextStyle(
                      color: Color(0x61000000),
                      fontWeight: FontWeight.w500,
                      fontFamily: "Roboto",
                      fontStyle: FontStyle.normal,
                      fontSize: 12.0,
                    ),
                  ),
                ],
              ),
              Text(
                data.storeType,
                style: const TextStyle(
                  color: Color(0x61000000),
                  fontWeight: FontWeight.w400,
                  fontFamily: "NotoSansCJKtc",
                  fontStyle: FontStyle.normal,
                  fontSize: 13.0,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
