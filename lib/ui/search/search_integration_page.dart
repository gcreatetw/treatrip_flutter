import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/keyword_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/search/search_result_page.dart';

class SearchIntegrationPage extends StatefulWidget {
  const SearchIntegrationPage({Key? key}) : super(key: key);

  @override
  _SearchIntegrationPageState createState() => _SearchIntegrationPageState();
}

class _SearchIntegrationPageState extends State<SearchIntegrationPage> {
  final TextEditingController _searchController = TextEditingController();

  final FocusNode _focusNode = FocusNode();

  final KeywordData keywordData = KeywordData.init();

  bool hasFocus = false;

  @override
  void initState() {
    Future<void>.microtask(() async {
      keywordData.fetchHotSearch();
      keywordData.fetchSearchHistory();
      _focusNode.addListener(() {
        _onFocusChange();
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          title: TextField(
            cursorWidth: 1.0,
            cursorColor: Colors.white,
            controller: _searchController,
            focusNode: _focusNode,
            textInputAction: TextInputAction.search,
            style: const TextStyle(
                color: Color(0xffffffff),
                fontWeight: FontWeight.w500,
                fontFamily: "NotoSansCJKtc",
                fontStyle: FontStyle.normal,
                fontSize: 19.0),
            decoration: const InputDecoration(
              hintText:
              // '輸入關鍵字搜尋'
              '搜尋功能建置中，敬請期待'
              ,
              border: InputBorder.none,
              hintStyle: TextStyle(
                color: Color(0x80ffffff),
                fontWeight: FontWeight.w500,
                fontFamily: "NotoSansCJKtc",
                fontStyle: FontStyle.normal,
                fontSize: 19.0,
              ),
            ),
            onChanged: (text) {
              setState(() {
                _searchController.text;
              });
            },
            onEditingComplete: () {
              onSearch(_searchController.text);
            },
          ),
          actions: [
            if (_searchController.text.isNotEmpty)
              GestureDetector(
                onTap: () {
                  setState(() {
                    _searchController.text = '';
                    FocusScope.of(context).requestFocus(FocusNode());
                  });
                },
                child: const Icon(Icons.clear_outlined),
              ),
            const SizedBox(width: 15.0)
          ],
          backgroundColor: AppColors.themeColor,
        ),
        body: MultiProvider(
          providers: [ChangeNotifierProvider.value(value: keywordData)],
          child: Consumer(
            builder: (context, KeywordData data, _) {
              return Container(
                color: Colors.white,
                height: double.maxFinite,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 11.0),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          _searchController.text.isEmpty ? '熱門搜尋' : '最近搜尋',
                          style: const TextStyle(
                            color: Color(0x8a000000),
                            fontWeight: FontWeight.w500,
                            fontFamily: "NotoSansCJKtc",
                            fontStyle: FontStyle.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                      const SizedBox(height: 8.0),
                      if (_searchController.text.isEmpty) ...[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Wrap(
                            children: [
                              for (String text in data.hotList)
                                searchClip(
                                  text: text,
                                ),
                            ],
                          ),
                        )
                      ] else ...[
                        for (String text in data.searchHistory)
                          autoSearchTitle(text: text)
                      ]
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget searchClip({required String text}) {
    return InkWell(
      onTap: () {
        onKeywordTap(text);
      },
      child: Container(
        margin: const EdgeInsets.all(4.0),
        padding: const EdgeInsets.symmetric(
          horizontal: 12.0,
          vertical: 6.0,
        ),
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color.fromRGBO(0, 0, 0, 0.12),
          ),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Text(
          text,
          style: const TextStyle(
            color: Color(0x61000000),
            fontWeight: FontWeight.w500,
            fontFamily: "NotoSansCJKtc",
            fontStyle: FontStyle.normal,
            fontSize: 13.0,
          ),
        ),
      ),
    );
  }

  Widget autoSearchTitle({required String text}) {
    return InkWell(
      onTap: () {
        onKeywordTap(text);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
          vertical: 14.0,
        ),
        child: Row(
          children: [
            const Icon(
              Icons.history_outlined,
              size: 16.0,
            ),
            const SizedBox(width: 16.0),
            Text(
              text,
              style: const TextStyle(
                color: Color(0xde000000),
                fontWeight: FontWeight.w500,
                fontFamily: "NotoSansCJKtc",
                fontStyle: FontStyle.normal,
                fontSize: 15.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onFocusChange() {
    setState(() {
      hasFocus = _focusNode.hasFocus;
    });
  }

  void onKeywordTap(String text) {
    setState(() {
      _searchController.text = text;
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  Future<void> onSearch(String text) async {
    if (_searchController.text.isNotEmpty) {
      Navigator.of(context).push(
        MaterialPageRoute<bool>(
          builder: (_) => SearchResultPage(query: text),
        ),
      );
      keywordData.fetchSearchHistory();
    } else {
      FocusScope.of(context).requestFocus(FocusNode());
    }
  }
}
