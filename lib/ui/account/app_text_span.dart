import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';

class AppTextSpan {
  static TextSpan spanCoral(String title) {
    return normal(title,
        fontSize: 16,
        fontFamily: 'Roboto',
        color: AppColors.themeColor,
        fontWeight: FontWeight.w700);
  }

  static TextSpan normal(
    String title, {
    Color color = AppColors.black54,
    int? maxLines,
    double fontSize = AppDimens.fontNormal,
    FontWeight fontWeight = FontWeight.w500,
    TextOverflow? overflow,
    String fontFamily = 'NotoSansCJKtc',
  }) {
    return TextSpan(
      text: title,
      style: TextStyle(
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
        overflow: overflow,
        fontFamily: fontFamily,
      ),
    );
  }
}
