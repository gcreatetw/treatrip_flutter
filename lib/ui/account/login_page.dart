import 'package:flutter/material.dart';
import 'package:treatrip/ui/account/sign_in/sign_in_page.dart';
import 'package:treatrip/ui/account/sign_up/sign_up_link_layout.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/main_page.dart';
import 'package:treatrip/widget/body/app_full_body.dart';
import 'package:treatrip/widget/button/login_button.dart';

import '../app_text.dart';

class LoginPage extends StatefulWidget {
  final String? title;
  const LoginPage({Key? key, this.title}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoginIng = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return AppFullBody(
      title: 'TREATRIP',
      actions: [
        TextButton(
          onPressed: () => _skipLoginPage(context),
          child: AppText.themeColor('略過'),
        )
      ],
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Center(
                child: Column(
                  children: [
                    Container(height: 128),
                    Image.asset('assets/images/treatrip_logo.png'),
                    Container(
                      height: 48,
                    ),
                    _buildLoginButton(context),
                    _buildReg(context),
                  ],
                ),
              ),
            ),
            const CopyRightCard(),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginButton(BuildContext childContext) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 24.0,
      ),
      child: LoginButton(
        '使用會員帳號登入',
        onPressed: () => _onTapLoginButton(childContext),
        icon: const Text('Icon'),
      ),
    );
  }

  Widget _buildReg(BuildContext childContext) {
    return const Padding(
      padding: EdgeInsets.all(24.0),
      child: SignUpLinkLayout(),
    );
  }

  Future<void> _onTapLoginButton(BuildContext childContext) async {
    bool? hasLogin = await Navigator.of(childContext).push(
      MaterialPageRoute<bool>(
        builder: (_) => const AccountSignInPage(),
      ),
    );
    if (hasLogin ?? false) {
      Navigator.of(childContext).pop(true);
    }
  }

  void _skipLoginPage(BuildContext childContext) {
    Navigator.of(childContext).pop();
  }

//TODO 略過
// void _openMainPage(BuildContext context) {
//   if (Preferences.getBool(Constants.isSkipLogin, false)) {
//     AutoRouter.of(context).pop();
//   } else {
//     Preferences.setBool(Constants.isSkipLogin, true);
//     AutoRouter.of(context).replace(
//       const MainRoute(),
//     );
//   }
// }
}
