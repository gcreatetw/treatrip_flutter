import 'package:flutter/material.dart';
import 'package:treatrip/ui/account/otp_validate/otp_notice_layout.dart';
import 'package:treatrip/ui/account/sign_up/sign_up_profile_page.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/widget/body/app_bar_body.dart';

import '../../app_block_widget.dart';
import '../otp_validate/otp_index_page.dart';
import '../otp_validate/otp_validate_state.dart';

class SignUpIndexPage extends StatefulWidget {
  const SignUpIndexPage({Key? key}) : super(key: key);

  @override
  State<SignUpIndexPage> createState() => _SignUpIndexPageState();
}

class _SignUpIndexPageState extends State<SignUpIndexPage> {
  @override
  Widget build(BuildContext context) {
    return AppBarBody(
      title: '建立帳號',
      body: AppBlockWidget.normal(
        child: Column(
          children: <Widget>[
            OptNoticeLayout(
              onPressed: () {
                _navigate(context);
              },
            ),
            const Spacer(),
            const CopyRightCard(),
          ],
        ),
      ),
    );
  }

  Future<void> _navigate(BuildContext childContext) async {
    final String account = await Navigator.of(childContext).push(
          MaterialPageRoute<String>(
            builder: (_) => const OtpIndexPage(
              type: OtpType.singUp,
            ),
          ),
        ) ??
        '';

    if (!mounted) return;
    if (account.isNotEmpty) {
      bool? success = await Navigator.of(childContext).push(
            MaterialPageRoute<bool>(
              builder: (_) => SignUpProfilePage(account: account),
            ),
          ) ??
          false;
      if (success) {
        if (!mounted) return;
        Navigator.of(context).pop(true);
      }
      // Navigator.of(context).pop();
    }
  }
}
