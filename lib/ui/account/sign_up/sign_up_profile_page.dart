import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/body/app_bar_body.dart';
import 'package:treatrip/widget/body/app_state_content.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../../../classification/settings_type.dart';
import '../../../provider/settings_provider.dart';
import '../../../widget/button/bottomButton.dart';
import '../password/edit_nick_name_layout.dart';
import '../password/edit_password_layout.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class SignUpProfilePage extends StatefulWidget {
  final String account;

  //final bool isForgot;

  const SignUpProfilePage({Key? key, required this.account}) : super(key: key);

  @override
  State<SignUpProfilePage> createState() => _SignUpProfilePageState();
}

class _SignUpProfilePageState extends State<SignUpProfilePage> {
  final SettingsProvider provider = SettingsProvider();
  late List<Widget> layouts;

  @override
  void initState() {
    layouts = <Widget>[
      EditPasswordLayout(provider: provider),
      EditNickNameLayout(provider: provider),
    ];

    provider.type = SettingsType.settingsPassword;
    provider.pageLength = layouts.length;
    provider.state = GeneralState.finish;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SettingsProvider>.value(
      value: provider,
      child: Consumer<SettingsProvider>(
        builder: (
          BuildContext childContext,
          SettingsProvider provider,
          __,
        ) {
          return AppBarBody(
            title: provider.title,
            body: GeneralStateContent(
              state: provider.state,
              hint: provider.message,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: PageView(
                      controller: provider.pageController,
                      physics: const NeverScrollableScrollPhysics(),
                      children: layouts,
                    ),
                  ),
                  SmoothPageIndicator(
                    controller: provider.pageController,
                    count: provider.pageLength,
                    effect: const WormEffect(
                      dotWidth: 10.0,
                      dotHeight: 10.0,
                      activeDotColor: AppColors.themeColor,
                      dotColor: AppColors.black38,
                    ),
                    onDotClicked: (_) {},
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  _bottomButton(
                    childContext,
                    provider,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _bottomButton(
    BuildContext childContext,
    SettingsProvider provider,
  ) {
    int currentIndex = 0;
    try {
      currentIndex = provider.pageController.page!.toInt() + 1;
    } catch (_) {
      currentIndex = 0;
    }

    // 順序搭配 layouts 物件
    switch (currentIndex) {
      case 0: //設定密碼跳到設定暱稱
        return ValueListenableBuilder<bool>(
          valueListenable: provider.isPasswordEmpty,
          builder: (_, bool isPasswordEmpty, __) {
            return isPasswordEmpty
                ? BottomButton('下一步', onPressed: null)
                : BottomButton(
                    '下一步',
                    onPressed: () {
                      provider.setPage(SettingsType.settingsNickName);
                      provider.pageController.nextPage(
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.easeOutQuint,
                      );
                    },
                  );
          },
        );
      case 1:
      //完成
      default:
        return ValueListenableBuilder<TextEditingValue>(
          valueListenable: provider.nickNameController,
          builder: (_, TextEditingValue value, __) {
            return value.text.isEmpty
                ? BottomButton(
                    '完成',
                    onPressed: null,
                  )
                : BottomButton(
                    '完成',
                    onPressed: () {
                      provider.doSignUp(
                        context,
                        account: widget.account,
                        onSuccess: () {
                          Navigator.of(context).pop(
                            true,
                          );
                        },
                      );
                    },
                  );
          },
        );
    }
  }

// Future<void> _toSignUpNamePage(BuildContext context) async {
//   bool? somebool = await Navigator.of(context).push(
//     MaterialPageRoute(
//       builder: (_) => SignUpNamePage(
//         account: 'account',
//       ),
//     ),
//   );
//   if (somebool == true) {
//     Navigator.of(context).pop();
//   }
// }
}
