import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/app_meta_data.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/utils/preferences.dart';
import 'package:treatrip/widget/body/app_bar_body.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ServicePolicyPage extends StatefulWidget {
  const ServicePolicyPage({Key? key}) : super(key: key);

  @override
  State<ServicePolicyPage> createState() => _ServicePolicyPageState();
}

class _ServicePolicyPageState extends State<ServicePolicyPage> {
  // InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
  //     crossPlatform: InAppWebViewOptions(
  //         useShouldOverrideUrlLoading: true,
  //         mediaPlaybackRequiresUserGesture: true),
  //     android: AndroidInAppWebViewOptions(
  //       useHybridComposition: true,
  //     ),
  //     ios: IOSInAppWebViewOptions(allowsInlineMediaPlayback: true));

  bool isAgree = false;

  @override
  Widget build(BuildContext context) {
    return AppBarBody(
      title: '平台服務協議',
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          //TODO if (!kIsWeb && (Platform.isAndroid || Platform.isIOS))
          const WebView(
            initialUrl:'https://www.treatrip.com/privacy/' ,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 48,
              decoration: const BoxDecoration(
                color: AppColors.themeColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(6.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Checkbox(
                    value: isAgree,
                    activeColor: AppColors.white,
                    checkColor: AppColors.themeColor,
                    onChanged: (bool? value) {
                      setState(() {
                        isAgree = value ?? false;
                      });
                    },
                  ),
                  AppText.white('我已同意服務條款'),
                  const Spacer(),
                  TextButton(
                    onPressed: isAgree
                        ? () async {
                            await Preferences.setBool(
                              Constants.hasBeenLogin,
                              true,
                            );
                            if (!mounted) return;
                            Navigator.of(context).pop(true);
                          }
                        : null,
                    child: AppText.white('下一步'),
                  ),
                  const SizedBox(width: 12.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
