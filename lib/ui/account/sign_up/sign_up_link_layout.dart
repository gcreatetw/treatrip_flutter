import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/account/sign_up/service_policy_page.dart';
import 'package:treatrip/ui/account/sign_up/sign_up_index_page.dart';
import 'package:treatrip/ui/app_block_widget.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/widget/button/text_link_button.dart';

class SignUpLinkLayout extends StatefulWidget {
  final bool isWhite;

  const SignUpLinkLayout({Key? key, this.isWhite = false}) : super(key: key);

  @override
  State<SignUpLinkLayout> createState() => _SignUpLinkLayoutState();
}

class _SignUpLinkLayoutState extends State<SignUpLinkLayout> {
  @override
  Widget build(BuildContext context) {
    final Color color =
        widget.isWhite ? AppColors.themeColor : AppColors.black54;
    return Align(
      child: AppBlockWidget.normal(
          child: SizedBox(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppText.normal(
              '還沒有帳號？',
              color: color,
            ),
            TextLinkButton(
              '註冊成為會員 ',
              onTap: TapGestureRecognizer()
                ..onTap = () async {
                  final bool confirm = await Navigator.of(context).push(
                        MaterialPageRoute<bool>(
                          builder: (_) => const ServicePolicyPage(),
                        ),
                      ) ??
                      false;
                  if (confirm) {
                    if (!mounted) return;
                    bool? success = await Navigator.of(context).push(
                          MaterialPageRoute<bool>(
                            builder: (_) => const SignUpIndexPage(),
                          ),
                        ) ??
                        false;
                    if (success) {
                      if (!mounted) return;
                      Navigator.of(context).pop(true);
                    }
                  }
                },
            ),
            AppText.normal(' 吧！', color: color),
          ],
        ),
      )),
    );
  }
}
