import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/provider/counter_provider.dart';
import 'package:treatrip/utils/string_utils.dart';
import 'package:treatrip/utils/validator.dart';

import 'otp_validate_state.dart';

//TODO 研究一下
class PhoneValidateProvider extends ChangeNotifier {
  final UserApi api = UserApi.instance;
  OtpValidateState state = OtpValidateState.enterInfo;
  late OtpType otpType;

  TextEditingController phoneController = TextEditingController();
  TextEditingController? validationCodeController;

  final FocusNode phoneFocusNode = FocusNode();

  String _phoneNumberErrorText = '';

  String _verificationId = '';

  CounterProvider counter = CounterProvider();

  String _validationCodeHint = '';

  int errorCount = 0;

  bool get overLimitCondition => errorCount >= 3;

  set errorText(String value) {
    _phoneNumberErrorText = value;
    notifyListeners();
  }

  String get errorText => _phoneNumberErrorText;

  set validationCodeHint(String value) {
    _validationCodeHint = value;
    notifyListeners();
  }

  String get validationCodeHint => _validationCodeHint;

  bool get isShowAppBar =>
      state == OtpValidateState.enterInfo ||
      state == OtpValidateState.enterCode ||
      state == OtpValidateState.error;

  PhoneValidateProvider({
    this.otpType = OtpType.singUp,
  });

  //AppBar 開頭
  String get title {
    switch (state) {
      case OtpValidateState.enterCode:
        return '手機驗證';
      case OtpValidateState.checkExistProcessing:
      case OtpValidateState.checkExistFinished:
      case OtpValidateState.enterInfo:
      case OtpValidateState.sendCodeProcessing:
      case OtpValidateState.sendCodeFinished:
      case OtpValidateState.confirmCodeProcessing:
      case OtpValidateState.finished:
      case OtpValidateState.error:
        return '手機驗證';
    }
  }

  String get message {
    switch (state) {
      case OtpValidateState.enterInfo:
        return '';
      case OtpValidateState.checkExistProcessing:
        return '確認帳號是否存在';
      case OtpValidateState.sendCodeProcessing:
        return '正在發送驗證碼';
      case OtpValidateState.checkExistFinished:
        return '';
      case OtpValidateState.sendCodeFinished:
        return '已發送';
      case OtpValidateState.enterCode:
        return '';
      case OtpValidateState.confirmCodeProcessing:
        return '正在驗證確認碼';
      case OtpValidateState.finished:
        return '完成';
      case OtpValidateState.error:
        return '您已暫時被停用，請稍後再試';
    }
  }

  void checkState() {
    if (overLimitCondition) {
      state = OtpValidateState.error;
      notifyListeners();
    }
  }

  //手機驗證階段dd
  void verify() {
    state = OtpValidateState.checkExistProcessing;
    notifyListeners();
    if (!Validator().isPhone(phoneController.text)) {
      errorText = '格式不符。';
      phoneController.clear();
      state = OtpValidateState.enterInfo;
      notifyListeners();
      return;
    }
    _checkExist();
  }

  Future<void> _checkExist() async {
    state = OtpValidateState.checkExistProcessing;
    notifyListeners();

    sendValidationCode();
  }

  Future<void> sendValidationCode() async {
    _verificationId = '';
    state = OtpValidateState.sendCodeProcessing;
    notifyListeners();

    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: StringUtils().parsePhone(phoneController.text),
      verificationCompleted: (PhoneAuthCredential credential) {
        debugPrint(credential.toString());
      },
      verificationFailed: (FirebaseAuthException e) {
        debugPrint(e.toString());
      },
      codeSent: (String verificationId, int? resendToken) async {
        _verificationId = verificationId;
        counter.start();
        state = OtpValidateState.sendCodeFinished;
        notifyListeners();
        await Future<void>.delayed(const Duration(microseconds: 500));
        state = OtpValidateState.enterCode;
        notifyListeners();
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  Future<void> confirmValidationCode({
    VoidCallback? onSuccess,
    VoidCallback? onFailed,
  }) async {
    if (validationCodeController == null) {
      return;
    }

    final String code = validationCodeController!.text;
    state = OtpValidateState.confirmCodeProcessing;
    notifyListeners();

    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: code,
      );

      final UserCredential result =
          await FirebaseAuth.instance.signInWithCredential(credential);
      final String idToken = await result.user!.getIdToken();

      onSuccess?.call();
    } catch (e) {
      errorCount++;
      if (overLimitCondition) {
        state = OtpValidateState.error;
        errorText = e.toString();
        notifyListeners();
      } else {
        _validationCodeHint = '無法驗證代碼，請再試一次';
        state = OtpValidateState.enterCode;
        notifyListeners();
      }
    }
  }

  void clearPhoneNumberErrorText() {
    errorText = '';
    notifyListeners();
  }
}
