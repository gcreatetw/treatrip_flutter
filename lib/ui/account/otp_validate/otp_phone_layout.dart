import 'package:flutter/material.dart';
import 'package:treatrip/ui/account/otp_validate/otp_vaildate_provider.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/card/light_coral_card.dart';
import 'package:treatrip/widget/button/primary_button.dart';

import '../../app_block_widget.dart';
import '../../app_input.dart';
import '../../app_text.dart';
import 'otp_validate_page_demo.dart';

class OtpPhoneLayout extends StatelessWidget {
  final VoidCallback? sendValidationCode;
  final PhoneValidateProvider provider;

  const OtpPhoneLayout(
      {Key? key, this.sendValidationCode, required this.provider})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        LightCoralCard(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AppBlockWidget.normal(
                child: AppText.normal('輸入手機號碼'),
              ),
              AppBlockWidget.normal(
                child: AppInput.primary(
                  controller: provider.phoneController,
                  focusNode: provider.phoneFocusNode,
                  keyboardType: TextInputType.phone,
                  hintText: '請輸入電話號碼',
                  onChanged: (String text) {
                    provider.clearPhoneNumberErrorText();
                  },
                  textInputAction: TextInputAction.send,
                  onEditingComplete: sendValidationCode,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: AppText.error(provider.errorText),
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child: ValueListenableBuilder<TextEditingValue>(
                    valueListenable: provider.phoneController,
                    builder: (_, TextEditingValue value, __) {
                      final bool isValid =
                          provider.phoneController.text.isNotEmpty;
                      return isValid
                          ? PrimaryButton(
                              '繼續',
                              onPressed: sendValidationCode,
                            )
                          : PrimaryButton(
                              '繼續',
                              onPressed: null,
                            );
                    },
                  ))
            ],
          ),
        ),
        const Spacer(),
        const CopyRightCard(),
      ],
    );
  }

  // void toOtpVailidatePage(BuildContext context) {
  //   Navigator.of(context)
  //       .push(MaterialPageRoute(builder: (_) => const OtpVailidatePage()));
  // }
}
