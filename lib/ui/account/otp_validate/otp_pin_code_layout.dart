import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/provider/counter_provider.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/account/sign_up/sign_up_profile_page.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/widget/button/primary_button.dart';

import '../../app_text.dart';
import '../app_text_span.dart';
import 'otp_re_send_page.dart';
import 'otp_vaildate_provider.dart';

class OtpPinCodeLayout extends StatelessWidget {
  final VoidCallback sendValidationCode;
  final VoidCallback? confirmValidationCode;
  final PhoneValidateProvider provider;

  const OtpPinCodeLayout(
      {Key? key,
      required this.provider,
      required this.sendValidationCode,
      this.confirmValidationCode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    provider.validationCodeController = TextEditingController();
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (provider.validationCodeHint.isNotEmpty)
            Center(
              child: AppText.white(provider.validationCodeHint),
            )
          else
            Text.rich(
              TextSpan(
                children: <TextSpan>[
                  AppTextSpan.normal('輸入傳送給 '),
                  AppTextSpan.spanCoral(provider.phoneController.text),
                  AppTextSpan.normal(' 的驗證碼'),
                ],
              ),
            ),
          const SizedBox(
            height: 16.0,
          ),
          //TODO controller: provider.validationCodeController,
          PinCodeTextField(
            appContext: context,
            controller: provider.validationCodeController,
            length: 6,
            onChanged: (_) {},
            cursorColor: AppColors.white,
            keyboardType: TextInputType.number,
            textStyle: AppTextStyle.getPinCode(),
            animationType: AnimationType.none,
            enableActiveFill: true,
            pinTheme: PinTheme(
              shape: PinCodeFieldShape.box,
              borderRadius: BorderRadius.circular(5),
              borderWidth: 0.0,
              fieldHeight: 40,
              fieldWidth: 40,
              activeColor: AppColors.pinCodeColor,
              inactiveColor: AppColors.pinCodeColor,
              activeFillColor: AppColors.pinCodeColor,
              inactiveFillColor: AppColors.pinCodeColor,
              selectedColor: AppColors.pinCodeColor,
              selectedFillColor: AppColors.pinCodeColor,
            ),
            onSubmitted: (_) => confirmValidationCode,
          ),
          const SizedBox(height: 16.0),
          GestureDetector(
            onTap: () async {
              final bool? success = await Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => ChangeNotifierProvider<CounterProvider>.value(
                    value: provider.counter,
                    child: OtpReSendPage(phone: provider.phoneController.text),
                  ),
                ),
              );
              if (success ?? false) {
                sendValidationCode();
              }
            },
            child: AppText.normal(
              '我沒有收到驗證碼',
              fontSize: 12.0,
              color: AppColors.themeColor,
            ),
          ),
          const SizedBox(height: 24.0),
          Align(
              alignment: Alignment.centerRight,
              child: ValueListenableBuilder<TextEditingValue>(
                valueListenable: provider.validationCodeController!,
                builder: (_, TextEditingValue value, __) {
                  print(value);
                  final bool isValid =
                      provider.validationCodeController!.text.length == 6;
                  return isValid
                      ? PrimaryButton('繼續', onPressed: confirmValidationCode)
                      : PrimaryButton('繼續', onPressed: null);
                },
              )),
          const Spacer(),
          const Align(
            alignment: Alignment.center,
            child: CopyRightCard(),
          ),
        ],
      ),
    );
  }

  //原本用在 PrimaryButton '繼續'
  void _toEditPasswordPage(BuildContext childContext) {
    Navigator.of(childContext).push(MaterialPageRoute(
        builder: (_) => const SignUpProfilePage(
              account: 'account',
            )));
  }
}
