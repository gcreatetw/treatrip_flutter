import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/card/light_coral_card.dart';
import 'package:treatrip/widget/button/primary_button.dart';

import '../../app_block_widget.dart';
import '../../app_text.dart';
import 'otp_index_page.dart';

class OptNoticeLayout extends StatelessWidget {
  final VoidCallback? onPressed;

  const OptNoticeLayout({Key? key, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LightCoralCard(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        AppText.normal(
          '請先驗證您的手機號碼',
          fontSize: AppDimens.fontContent,
        ),
        _buildSmsContent(),
        Align(
          alignment: Alignment.centerRight,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AppBlockWidget.normal(
                child: PrimaryButton(
                  '開始驗證',
                  onPressed: onPressed,
                ),
              ),
            ],
          ),
        ),
      ],
    ));
  }

  static Widget _buildSmsContent() {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 16.0,
      ),
      child: Row(
        children: <Widget>[
          Image.asset(
            'assets/images/ic_sms.png',
            width: 44.0,
          ),
          const SizedBox(
            width: 6.0,
          ),
          Expanded(
            child: AppText.normal(
              '只要透過簡單的SMS簡訊驗證，我們將以您的電話作為帳號。',
              overflow: TextOverflow.visible,
            ),
          )
        ],
      ),
    );
  }

  // void _onTapToPhoneLayout(BuildContext context) {
  //   Navigator.of(context).push(
  //     MaterialPageRoute(builder: (_) => OtpIndexPage()),
  //   );
  // }
}
