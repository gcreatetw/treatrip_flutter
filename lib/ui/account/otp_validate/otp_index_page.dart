import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/ui/account/otp_validate/otp_vaildate_provider.dart';
import 'package:treatrip/widget/body/app_state_content.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../../../models/data/constants.dart';
import '../../../res/resources.dart';
import '../../../utils/preferences.dart';
import 'otp_phone_layout.dart';
import 'otp_pin_code_layout.dart';
import 'otp_validate_state.dart';

class OtpIndexPage extends StatefulWidget {
  final OtpType type;
  const OtpIndexPage({Key? key, required this.type}) : super(key: key);

  @override
  State<OtpIndexPage> createState() => _OtpIndexPageState();
}

class _OtpIndexPageState extends State<OtpIndexPage> {
  late PhoneValidateProvider provider;

  @override
  void initState() {
    provider = PhoneValidateProvider();
    Future.microtask(() {
      provider.checkState();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<PhoneValidateProvider>.value(
      value: provider,
      child: Consumer<PhoneValidateProvider>(
        builder:
            (BuildContext childContext, PhoneValidateProvider provider, __) {
          return Scaffold(
            appBar: AppBar(
              iconTheme: const IconThemeData(color: AppColors.themeColor),
              automaticallyImplyLeading:
                  Preferences.getBool(Constants.isSkipLogin, true),
              title: Text(provider.title),
              backgroundColor: AppColors.white,
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              elevation: 0,
            ),
            body: _body(childContext, provider),
          );
        },
      ),
    );
    // return Container(
    //   child: const AppBarBody(
    //     title: '手機驗證',
    //   ),
    // );
  }

  Widget _body(BuildContext childContext, PhoneValidateProvider provider) {
    switch (provider.state) {
      case OtpValidateState.enterInfo:
      case OtpValidateState.checkExistFinished:
        return OtpPhoneLayout(
          provider: provider,
          sendValidationCode: () => _sendValidationCode(provider),
        );
      case OtpValidateState.sendCodeProcessing:
      case OtpValidateState.confirmCodeProcessing:
        return GeneralStateContent(
          state: GeneralState.loading,
          hint: provider.message,
          subHint: provider.state == OtpValidateState.confirmCodeProcessing &&
                  provider.errorCount > 0
              ? '錯誤${provider.errorCount}次'
              : null,
        );
      case OtpValidateState.enterCode:
        return OtpPinCodeLayout(
          provider: provider,
          confirmValidationCode: () {
            _confirmValidationCode(
              provider,
            );
          },
          sendValidationCode: () => _sendValidationCode(provider),
        );
      case OtpValidateState.checkExistProcessing:
      case OtpValidateState.sendCodeFinished:
      case OtpValidateState.finished:
        return GeneralStateContent(
          state: GeneralState.loading,
          hint: provider.message,
          //customIcon: Image.asset('assets/images/ic_status_done_96.png'),
        );
      case OtpValidateState.error:
        return GeneralStateContent(
          state: GeneralState.error,
          hint: provider.message,
        );
    }
  }

  void _sendValidationCode(
    PhoneValidateProvider provider,
  ) {
    provider.verify();
  }

  void _confirmValidationCode(
    PhoneValidateProvider provider,
  ) {
    provider.confirmValidationCode(
      onSuccess: () {
        Navigator.of(context).pop(
          provider.phoneController.text,
        );
      },
      onFailed: () {},
    );
  }
}
