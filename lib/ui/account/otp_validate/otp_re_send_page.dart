import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/widget/body/app_bar_x_body.dart';

import '../../app_text.dart';

class OtpReSendPage extends StatelessWidget {
  final String phone;

  const OtpReSendPage({Key? key, required this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBarXBody(
      title: '沒有收到驗證碼',
      body: Column(
        children: [
          const SizedBox(
            width: double.infinity,
            height: 47.0,
          ),
          AppText.normal('查看訊息中是否有內含驗證碼的簡訊', fontSize: 17.0),
          const SizedBox(height: 4.0),
          AppText.normal('請確認您的手機號碼為', fontSize: 17.0),
          const SizedBox(height: 4.0),
          AppText.carolinaCoral(phone),
          const SizedBox(height: 24.0),
          ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(AppColors.themeColor),
            ),
            onPressed: () => Navigator.of(context).pop(true),
            child: AppText.normalDisabled('再次傳送驗證碼'),
          )

          //TODO:
          // Consumer<CounterProvider>(
          //   builder: (_, CounterProvider counter, __) {
          //     return ElevatedButton(
          //       onPressed: counter.canReSend
          //           ? () {
          //               Navigator.of(context).pop(true);
          //             }
          //           : null,
          //       child: counter.canReSend
          //           ? AppText.normal('再次傳送驗證碼')
          //           : AppText.normalDisabled('於 ${counter.seconds} 秒後再次傳送驗證碼'),
          //     );
          //   },
          // ),
        ],
      ),
    );
  }
}
