enum OtpValidateState {
  enterInfo,
  checkExistProcessing,
  checkExistFinished,
  sendCodeProcessing,
  sendCodeFinished,
  enterCode,
  confirmCodeProcessing,
  finished,
  error,
}

enum OtpType {
  singUp,
  forgetPassword,
  editPassword,
}
