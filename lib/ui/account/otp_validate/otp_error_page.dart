import 'package:flutter/material.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/widget/body/app_bar_x_body.dart';

import '../../app_text.dart';

class OtpErrorPage extends StatefulWidget {
  const OtpErrorPage({Key? key}) : super(key: key);

  @override
  State<OtpErrorPage> createState() => _OtpErrorPageState();
}

class _OtpErrorPageState extends State<OtpErrorPage> {
  @override
  Widget build(BuildContext context) {
    return AppBarXBody(
      title: '驗證錯誤',
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Image.asset(
              'assets/images/ic_status_important_96.png',
              width: 80,
              height: 80,
              fit: BoxFit.fill,
            ),
            const SizedBox(height: 16),
            AppText.normal(
              '您已暫時被停用，請稍後再試',
              fontSize: AppDimens.fontTitle,
            ),
            const Spacer(),
            const CopyRightCard(),
          ],
        ),
      ),
    );
  }
}
