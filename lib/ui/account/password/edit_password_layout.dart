import 'package:flutter/material.dart';
import 'package:treatrip/ui/app_input.dart';
import 'package:treatrip/ui/app_text.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/card/light_coral_card.dart';
import 'package:treatrip/widget/button/visibility_button.dart';

import '../../../provider/settings_provider.dart';
import '../../app_block_widget.dart';

class EditPasswordLayout extends StatefulWidget {
  final SettingsProvider provider;

  const EditPasswordLayout({Key? key, required this.provider})
      : super(key: key);

  @override
  State<EditPasswordLayout> createState() => _EditPasswordLayoutState();
}

class _EditPasswordLayoutState extends State<EditPasswordLayout> {
  bool _visibility = false;

  @override
  Widget build(BuildContext context) {
    return AppBlockWidget.normal(
        child: Column(
      children: <Widget>[
        LightCoralCard(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AppText.normal('設定登入密碼'),
              const SizedBox(
                height: 16.0,
              ),
              AppInput.primary(
                hintText: '請輸入密碼',
                obscureText: _visibility,
                controller: widget.provider.passwordController,
                keyboardType: TextInputType.visiblePassword,
                focusNode: widget.provider.passwordFocusNode,
                textInputAction: TextInputAction.next,
                suffixIcon: VisibilityButton(
                  visibility: _visibility,
                  onPressed: () {
                    setState(() {
                      _visibility = !_visibility;
                    });
                  },
                ),
                onChanged: (String text) {
                  //print(text);
                  widget.provider.checkPasswordEmpty();
                },
              ),
              const SizedBox(height: 16.0),
              //TODO controller & focusNode & onChanged
              AppInput.primary(
                hintText: '請確認密碼',
                obscureText: true,
                controller: widget.provider.rePasswordController,
                focusNode: widget.provider.rePasswordFocusNode,
                keyboardType: TextInputType.visiblePassword,
                textInputAction: TextInputAction.send,
                onChanged: (String text) {
                  widget.provider.checkPasswordEmpty();
                },
              ),
              const SizedBox(height: 6.0),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: AppText.error(widget.provider.errorText),
              )
            ],
          ),
        ),
        const Spacer(),
        const CopyRightCard(),
      ],
    ));
  }
}
