import 'package:flutter/material.dart';
import 'package:treatrip/ui/account/otp_validate/otp_index_page.dart';
import 'package:treatrip/ui/account/otp_validate/otp_notice_layout.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/widget/body/app_bar_body.dart';
import 'package:treatrip/widget/body/app_bar_x_body.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../../app_block_widget.dart';
import '../otp_validate/otp_validate_state.dart';
import 'edit_password_page.dart';

class ForgetPasswordPage extends StatefulWidget {
  const ForgetPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgetPasswordPage> createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  @override
  Widget build(BuildContext context) {
    return AppBarXBody(
      title: '忘記密碼',
      body: AppBlockWidget.normal(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            OptNoticeLayout(onPressed: () {
              _navigate();
            }),
            const Spacer(),
            const Align(
              alignment: Alignment.bottomCenter,
              child: CopyRightCard(),
            ),
          ],
        ),
      ),
    );
  }

  //TODO 研究Futrure
  Future<void> _navigate() async {
    final String account = await Navigator.of(context).push(
          MaterialPageRoute<String>(
            builder: (_) => const OtpIndexPage(
              type: OtpType.forgetPassword,
            ),
          ),
        ) ??
        '';

    if (!mounted) return;
    if (account.isNotEmpty) {
      final GeneralState reset =
          await Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => EditPasswordPage(
                  account: account,
                ),
              )) ??
              GeneralState.error;

      if (!mounted) return;
      if (reset == GeneralState.finish) {
        Navigator.pop(context);
      }
    }
  }

  // void _toOtpIndexPage(BuildContext context) {
  //   Navigator.of(context).push(
  //     MaterialPageRoute<String>(
  //       builder: (_) => const OtpIndexPage(),
  //     ),
  //   );
  // }
}
