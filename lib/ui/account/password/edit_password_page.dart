import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/card/light_coral_card.dart';
import 'package:treatrip/widget/body/app_bar_body.dart';
import 'package:treatrip/widget/body/app_state_content.dart';
import 'package:treatrip/widget/button/bottomButton.dart';
import 'package:treatrip/widget/button/visibility_button.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../../../provider/settings_provider.dart';
import '../../app_block_widget.dart';
import '../../app_input.dart';
import '../../app_text.dart';

class EditPasswordPage extends StatefulWidget {
  final String account;

  const EditPasswordPage({Key? key, this.account = ''}) : super(key: key);

  @override
  State<EditPasswordPage> createState() => _EditPasswordPageState();
}

class _EditPasswordPageState extends State<EditPasswordPage> {
  late SettingsProvider provider;
  bool _visibility = false;

  @override
  void initState() {
    provider = SettingsProvider();
    Future<void>.microtask(() {
      provider.init();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SettingsProvider>.value(
      value: provider,
      child: Consumer<SettingsProvider>(
        builder: (BuildContext context, SettingsProvider provider, ___) {
          return Scaffold(
            appBar: AppBar(
              iconTheme: const IconThemeData(color: AppColors.themeColor),
              title: const Text(
                '重設密碼',
              ),
            ),
            body: _body(
              context,
              provider,
            ),
          );
        },
      ),
    );
  }

  Widget _body(BuildContext context, SettingsProvider provider) {
    switch (provider.state) {
      case GeneralState.loading:
        return const GeneralStateContent(
          state: GeneralState.loading,
          hint: '處理中，請稍候',
        );
      case GeneralState.error:
        return GeneralStateContent(
          state: GeneralState.error,
          hint: provider.errorText,
        );
      case GeneralState.finish:
        return Column(
          children: <Widget>[
            AppBlockWidget.normal(
              child: LightCoralCard(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    AppText.normal(
                      '設定新的密碼',
                      fontSize: AppDimens.fontContent,
                    ),
                    const SizedBox(height: 16.0),
                    AppInput.primary(
                      hintText: '請輸入密碼',
                      obscureText: !_visibility,
                      controller: provider.passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      focusNode: provider.passwordFocusNode,
                      textInputAction: TextInputAction.next,
                      suffixIcon: VisibilityButton(
                        visibility: _visibility,
                        onPressed: () {
                          setState(() {
                            _visibility = !_visibility;
                          });
                        },
                      ),
                      onChanged: (String text) {
                        setState(() {
                          provider.checkPasswordEmpty();
                        });
                      },
                    ),
                    const SizedBox(
                      height: 16.0,
                    ),
                    AppInput.primary(
                      hintText: '請確認密碼',
                      obscureText: true,
                      controller: provider.rePasswordController,
                      keyboardType: TextInputType.visiblePassword,
                      focusNode: provider.rePasswordFocusNode,
                      textInputAction: TextInputAction.send,
                      onChanged: (String text) {
                        setState(() {
                          provider.checkPasswordEmpty();
                        });
                      },
                    ),
                    const SizedBox(height: 6.0),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: AppText.error(provider.errorText),
                    ),
                  ],
                ),
              ),
            ),
            const Spacer(),
            const CopyRightCard(),
            const SizedBox(
              height: 12.0,
            ),
            BottomButton(
              '送出',
              onPressed: provider.isPasswordEmpty.value
                  ? null
                  : () {
                      provider.doForgetPassword(
                        account: widget.account,
                        onSuccess: () {
                          Navigator.of(context).pop(
                            GeneralState.finish,
                          );
                        },
                      );
                    },
            ),
          ],
        );
    }
  }
}
