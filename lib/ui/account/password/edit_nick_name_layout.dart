import 'package:flutter/material.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/card/light_coral_card.dart';

import '../../../provider/settings_provider.dart';
import '../../app_block_widget.dart';
import '../../app_input.dart';
import '../../app_text.dart';

class EditNickNameLayout extends StatelessWidget {
  final SettingsProvider provider;

  const EditNickNameLayout({
    Key? key,
    required this.provider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBlockWidget.normal(
      child: Column(
        children: [
          LightCoralCard(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppBlockWidget.normal(
                  child: AppText.normal(
                    '設定您的暱稱',
                  ),
                ),
                const SizedBox(height: 16.0),
                AppInput.primary(
                  controller: provider.nickNameController,
                  keyboardType: TextInputType.text,
                  focusNode: provider.nickNameFocusNode,
                  textInputAction: TextInputAction.send,
                ),
                const SizedBox(height: 6.0),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: AppText.error(
                    provider.nickNameErrorText,
                  ),
                ),
              ],
            ),
          ),
          const Spacer(),
          const CopyRightCard(),
        ],
      ),
    );
  }
}
