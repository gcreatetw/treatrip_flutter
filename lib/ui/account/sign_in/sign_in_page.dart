//使用會員帳號登入頁面

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/models/response/general_response.dart';
import 'package:treatrip/provider/user_page_provider.dart';
import 'package:treatrip/provider/user_provider.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/account/password/forget_password_page.dart';
import 'package:treatrip/ui/account/sign_up/sign_up_link_layout.dart';
import 'package:treatrip/ui/app_block_widget.dart';
import 'package:treatrip/ui/app_input.dart';
import 'package:treatrip/ui/card/copyright_card.dart';
import 'package:treatrip/ui/card/light_coral_card.dart';
import 'package:treatrip/utils/preferences.dart';
import 'package:treatrip/widget/body/app_bar_body.dart';
import 'package:treatrip/widget/body/app_state_content.dart';
import 'package:treatrip/widget/button/primary_button.dart';
import 'package:treatrip/widget/button/text_link_button.dart';
import 'package:treatrip/widget/button/visibility_button.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../../app_text.dart';

class AccountSignInPage extends StatefulWidget {
  const AccountSignInPage({Key? key}) : super(key: key);

  @override
  State<AccountSignInPage> createState() => _AccountSignInPageState();
}

class _AccountSignInPageState extends State<AccountSignInPage> {
  @override
  void dispose() {
    super.dispose();
  }

  bool _visibility = false;

  @override
  void initState() {
    Future<void>.microtask(() {
      context.read<UserProvider>().init();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBarBody(
        title: '使用會員帳號登入',
        body: Consumer<UserProvider>(
            builder: (BuildContext childContext, UserProvider provider, __) {
          return _body(childContext, provider);
        }));
  }

  Widget _body(BuildContext childContext, UserProvider provider) {
    switch (provider.state) {
      case GeneralState.finish:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            LightCoralCard(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AppBlockWidget.normal(
                    child: AppText.normal('輸入帳號相關資訊'),
                  ),
                  AppBlockWidget.normal(
                    child: AppInput.primary(
                      controller: provider.phoneController,
                      keyboardType: TextInputType.phone,
                      hintText: '請輸入電話號碼',
                      onChanged: (String text) {
                        provider.checkInput();
                      },
                    ),
                  ),
                  AppBlockWidget.normal(
                    child: AppInput.primary(
                      controller: provider.passwordController,
                      obscureText: !_visibility,
                      hintText: '請輸入密碼',
                      suffixIcon: VisibilityButton(
                          onPressed: () {
                            setState(() {
                              _visibility = !_visibility;
                            });
                          },
                          visibility: _visibility),
                      onChanged: (String text) {
                        provider.checkInput();
                      },
                    ),
                  ),
                  _buildErrorNotice(provider),
                  _buildForgetPassword(provider),
                ],
              ),
            ),
            const Spacer(),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildReg(),
                  const CopyRightCard(),
                ],
              ),
            )
          ],
        );
      case GeneralState.loading:
        return GeneralStateContent(
          state: GeneralState.loading,
          hint: provider.message,
          customIcon: const CircularProgressIndicator(
            color: AppColors.themeColor,
          ),
        );
      case GeneralState.error:
        return GeneralStateContent(
          state: GeneralState.error,
          hint: provider.message,
          customIcon: const Icon(Icons.title),
        );
    }
  }

  Widget _buildForgetPassword(UserProvider provider) {
    return Row(
      children: <Widget>[
        AppText.normal('您是否', color: AppColors.black38),
        TextLinkButton(
          '忘記密碼',
          onTap: TapGestureRecognizer()
            ..onTap = () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (_) => const ForgetPasswordPage()),
              );
            },
        ),
        AppText.normal('?', color: AppColors.black38),
        _buildLoginButton(context, provider),
      ],
    );
  }

  Widget _buildErrorNotice(UserProvider provider) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0),
      child: AppText.normal(provider.message,
          color: AppColors.tomato, fontSize: AppDimens.fontErrorHint),
    );
  }

  Widget _buildLoginButton(BuildContext childContext, UserProvider provider) {
    return Expanded(
      child: Align(
        alignment: Alignment.centerRight,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            AppBlockWidget.normal(
              child: PrimaryButton(
                '登入',
                onPressed: !provider.hasInput
                    ? null
                    : () {
                        provider.login((GeneralResponse e) {
                          debugPrint('登入e.message');
                        }, () {
                          print('登入pop');
                          context.read<UserPageProvider>().fetchUserData(
                                phone:
                                    Preferences.getString(Constants.phone, ''),
                                password: Preferences.getString(
                                    Constants.password, ''),
                              );
                          Navigator.of(childContext).pop(true);
                        });
                      },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildReg() {
    return const SignUpLinkLayout();
  }
}
