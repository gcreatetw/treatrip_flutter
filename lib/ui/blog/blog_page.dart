import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:treatrip/models/data/blog_page_data.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/utils/utils.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/widget/store_information_title.dart';
import '../post_webview.dart';

class BlogPage extends StatefulWidget {
  final String? title;
  final int postId;

  // final bool hasAuthor;

  const BlogPage({
    Key? key,
    // required this.hasAuthor,
    this.title,
    required this.postId,
  }) : super(key: key);

  @override
  _BlogPageState createState() => _BlogPageState();
}

class _BlogPageState extends State<BlogPage> {
  BlogPageData blogPageData = BlogPageData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      blogPageData.fetch(widget.postId);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.themeColor,
        title: Text(
          widget.title ?? '',
          style: const TextStyle(
            color: Color(0xffffffff),
            fontWeight: FontWeight.w500,
            fontFamily: "NotoSansCJKtc",
            fontStyle: FontStyle.normal,
            fontSize: 25.0,
          ),
        ),
      ),
      body: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider.value(value: blogPageData)
        ],
        child: Consumer(
          builder: (BuildContext context, BlogPageData data, _) {
            return StateController(
              state: data.state,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (data.bloggerName != null || data.profileUrl != null)
                      Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 8.0,
                          horizontal: 16.0,
                        ),
                        color: Colors.white,
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundImage: NetworkImage(data.profileUrl!),
                              backgroundColor: Colors.white,
                              radius: 20,
                            ),
                            const SizedBox(width: 8.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  data.bloggerName!,
                                  style: const TextStyle(
                                    color: Color(0xde000000),
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "NotoSansCJKtc",
                                    fontStyle: FontStyle.normal,
                                    fontSize: 15.0,
                                  ),
                                ),
                                Text(
                                  '${NumberFormat().format(data.follows)}個追蹤',
                                  style: TextStyle(
                                    color: const Color(0xff000000)
                                        .withOpacity(0.38),
                                    fontWeight: FontWeight.w400,
                                    fontFamily: "Roboto",
                                    fontStyle: FontStyle.normal,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Stack(
                        children: [
                          Image.network(
                            data.bannerUrl,
                            fit: BoxFit.cover,
                            width: width,
                            height: width,
                          ),
                          Positioned(
                            left: 16.0,
                            right: 16.0,
                            bottom: 16.0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  Utils.fixTitle(data.title),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: "NotoSansCJKtc",
                                    fontStyle: FontStyle.normal,
                                    fontSize: 25.0,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        data.date,
                                        style: const TextStyle(
                                          color: Color(0xdeffffff),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "NotoSansCJKtc",
                                          fontStyle: FontStyle.normal,
                                          fontSize: 15.0,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      '${NumberFormat().format(data.viewed)} 人氣數',
                                      style: const TextStyle(
                                        color: Color(0xdeffffff),
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "NotoSansCJKtc",
                                        fontStyle: FontStyle.normal,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(color: Colors.white, height: 16.0),
                    Container(
                      color: Colors.white,
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Html(
                        data: data.blogContents,
                        onLinkTap: (url, webContext, _, element) {
                          //print(url);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  PostWebView(url: url ?? '')));
                        },
                      ),
                    ),
                    Container(color: Colors.white, height: 12.0),
                    const SizedBox(height: 12.0),
                    Container(color: Colors.white, height: 12.0),
                    Container(
                      color: Colors.white,
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      alignment: Alignment.centerLeft,
                      child: const Text(
                        "店家資訊",
                        style: TextStyle(
                          color: Color(0x61000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "NotoSansCJKtc",
                          fontStyle: FontStyle.normal,
                          fontSize: 14.0,
                        ),
                      ),
                    ),
                    Container(color: Colors.white, height: 8.0),
                    StoreInformationTitle(
                      data: data.storeInformationData,
                      clickable: true,
                    ),
                    const SizedBox(height: 56.0),
                    const Text(
                      '趣趣',
                      style: TextStyle(color: AppColors.themeColor),
                    ),
                    const SizedBox(height: 9.0),
                    const Text(
                      '© Copyright 2020 TRETRIP. All rights reserved.',
                      style: TextStyle(
                        color: Color(0x61000000),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Roboto",
                        fontStyle: FontStyle.normal,
                        fontSize: 8.0,
                      ),
                    ),
                    const SizedBox(
                      height: 24.0,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
