import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:treatrip/models/explore_detail_page_data.dart';
import 'package:treatrip/models/response/explore_new_data_response.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/utils/utils.dart';
import 'package:treatrip/widget/article_list_title.dart';
import 'package:treatrip/widget/body/app_state_content.dart';
import 'package:treatrip/widget/float_back_button.dart';
import 'package:treatrip/widget/state_controller.dart';

class ExploreDetailPage extends StatefulWidget {
  final int? exploreId;
  final List<ChildrenBean>? termsId;
  final String? exploreTaiwanImageUrl;
  final String? exploreTaiwanTitle;

  const ExploreDetailPage({
    Key? key,
    this.exploreId,
    this.termsId,
    this.exploreTaiwanImageUrl,
    this.exploreTaiwanTitle,
  }) : super(key: key);

  @override
  State<ExploreDetailPage> createState() => _ExploreDetailPageState();
}

class _ExploreDetailPageState extends State<ExploreDetailPage> {
  ExploreDetailPageData exploreDetailPageData = ExploreDetailPageData();

  int currentChip = 0;

  @override
  void initState() {
    Future<void>.microtask(() async {
      exploreDetailPageData.fetch(
        exploreId: widget.exploreId,
        termsId: widget.termsId,
        exploreImageUrl: widget.exploreTaiwanImageUrl!,
        exploreTitle: widget.exploreTaiwanTitle!,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      floatingActionButton: FloatBackButton(context),
      body: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider.value(value: exploreDetailPageData),
        ],
        child: Consumer<ExploreDetailPageData>(
          builder: (BuildContext context, ExploreDetailPageData data, _) {
            return GeneralStateContent(
              state: data.state,
              child: NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification scrollInfo) {
                  if (scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent) {
                    _loadMore();
                  }
                  return true;
                },
                child: ListView(
                  // mainAxisSize: MainAxisSize.min,
                  padding: EdgeInsets.zero,
                  children: [
                    Stack(
                      children: [
                        Image.network(
                          data.explorePageImageUrl ?? '',
                          fit: BoxFit.cover,
                          width: width,
                          height: width / 320 * 426,
                        ),
                        Positioned(
                          left: 16.0,
                          right: 16.0,
                          bottom: 16.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Utils.fixTitle(data.explorePageTitle ?? ''),
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: "NotoSansCJKtc",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 25.0,
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      color: Colors.white,
                      child: Builder(
                        builder: (BuildContext context) {
                          int chipCount = widget.termsId!.length;
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 7.0, horizontal: 12.0),
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    for (int i = 0; i < chipCount + 1; i++)
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 4.0),
                                        child: ChoiceChip(
                                          label: Text(i == 0
                                              ? '全部'
                                              : widget.termsId![i - 1].Name!),
                                          labelStyle: TextStyle(
                                              color: currentChip == i
                                                  ? Colors.white
                                                  : const Color(0x61000000)),
                                          onSelected: (bool value) {
                                            _onChipSelect(i);
                                            if (i == 0) {
                                              data.fetch(
                                                termsId: widget.termsId,
                                                exploreImageUrl: widget
                                                    .exploreTaiwanImageUrl!,
                                                exploreTitle:
                                                    widget.exploreTaiwanTitle!,
                                                open: false,
                                              );
                                            } else {
                                              final List<ChildrenBean>
                                                  selectedTermId = [
                                                widget.termsId![i - 1],
                                              ];
                                              print(
                                                  '0621 = ${widget.termsId![i - 1].Id}');
                                              data.fetch(
                                                termsId: selectedTermId,
                                                exploreImageUrl: widget
                                                    .exploreTaiwanImageUrl!,
                                                exploreTitle:
                                                    widget.exploreTaiwanTitle!,
                                                open: false,
                                              );
                                            }
                                          },
                                          selected: currentChip == i,
                                          elevation: 2,
                                          selectedColor: AppColors.themeColor,
                                          backgroundColor: Colors.white,
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    for (final element in data.exploreArticleList)
                      ArticleListTitle(
                        data: element,
                      ),
                    // ListView.builder(
                    //       padding: EdgeInsets.zero,
                    //       shrinkWrap: true,
                    //       itemCount: data.exploreArticleList.length,
                    //       physics: const NeverScrollableScrollPhysics(),
                    //       itemBuilder: (_, int index) {
                    //         return ArticleListTitle(
                    //           data: data.exploreArticleList[index],
                    //         );
                    //       },
                    //     ),

                    if (data.loadingState == GeneralState.loading) ...[
                      const SizedBox(height: 16),
                      const Center(
                        child: CircularProgressIndicator(
                          color: AppColors.themeColor,
                        ),
                      )
                    ],
                    if (data.noMore) ...[
                      const SizedBox(height: 16),
                      const Center(
                        child: Text(
                          '沒有更多文章',
                          style: TextStyle(color: AppColors.themeColor),
                        ),
                      ),
                    ],
                    const SizedBox(height: 56.0),
                    Column(
                      children: const [
                        Text(
                          '趣趣',
                          style: TextStyle(color: AppColors.themeColor),
                        ),
                        SizedBox(height: 9.0),
                        Text(
                          '© Copyright 2020 TRETRIP. All rights reserved.',
                          style: TextStyle(
                            color: Color(0x61000000),
                            fontWeight: FontWeight.w400,
                            fontFamily: "Roboto",
                            fontStyle: FontStyle.normal,
                            fontSize: 8.0,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 24.0,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void _onChipSelect(int index) {
    setState(() {
      currentChip = index;
    });
  }

  void _loadMore() {
    exploreDetailPageData.fetch(
      append: true,
      termsId: currentChip == 0
          ? widget.termsId
          : [widget.termsId![currentChip - 1]],
      exploreId: widget.exploreId,
      exploreImageUrl: widget.exploreTaiwanImageUrl!,
      exploreTitle: widget.exploreTaiwanTitle!,
    );
  }
}
