import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:treatrip/models/data/banner_data.dart';
import 'package:treatrip/models/data/explore_page_data.dart';
import 'package:treatrip/models/image_slider_data.dart';
import 'package:treatrip/models/response/explore_new_data_response.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/source/default_img.dart';
import 'package:treatrip/ui/explore/banner_page.dart';
import 'package:treatrip/ui/search/search_integration_page.dart';
import 'package:treatrip/widget/image_slider/image_slider_big.dart';
import 'package:treatrip/widget/image_slider/image_slider_small.dart';
import 'package:treatrip/widget/state_controller.dart';

import 'explore_detail_page.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({Key? key}) : super(key: key);

  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => false;
  final imagePaths = <String>[
    DefaultImages.welcomePageBackground,
    DefaultImages.welcomeTreatripLogo,
  ];

  ExplorePageData explorePageData = ExplorePageData.init();

  @override
  void initState() {
    Future<void>.microtask(() async {
      explorePageData.fetchAll();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text(
          '探索趣趣',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0),
        ),
        backgroundColor: AppColors.themeColor,
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (_) => const SearchIntegrationPage(),
                ),
              );
            },
            child: const Icon(Icons.search_outlined),
          ),
          const SizedBox(width: 22.5),
          GestureDetector(
            onTap: () {},
            child: const Icon(Icons.map_outlined),
          ),
          const SizedBox(width: 15.0)
        ],
      ),
      body: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider.value(value: explorePageData)
        ],
        child: Consumer2<ExplorePageData, BannerData>(
          builder: (BuildContext context, ExplorePageData data,
              BannerData bannerData, _) {
            return StateController(
              state: data.state,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    ImageSliderBig(
                      data: bannerData.bannerList,
                      linkData: bannerData.bannerLinkList,
                      height: width,
                      dotSelectedColor: Colors.white,
                      dotColor: Colors.white,
                      selectOpacity: 1,
                      unSelectedOpacity: 0.5,
                      dotSize: 8.0,
                      dotPadding: 2.0,
                    ),
                    Container(color: Colors.white, height: 12.0),
                    bigText('期間限定'),
                    Container(color: Colors.white, height: 11.0),
                    ImageSliderSmall(
                      title: '期間限定',
                      data: data.periodList,
                      height: width,
                      dotSelectedColor: AppColors.themeColor,
                      dotColor: Colors.black,
                      selectOpacity: 1,
                      unSelectedOpacity: 0.12,
                      dotSize: 8.0,
                      dotPadding: 4.0,
                    ),
                    Container(color: Colors.white, height: 12.0),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      width: double.maxFinite,
                      color: Colors.white,
                      child: OutlinedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute<void>(
                              builder: (_) => const BannerPage(
                                  exploreType: ExploreType.periodLimit),
                            ),
                          );
                        },
                        style: OutlinedButton.styleFrom(
                          side: const BorderSide(
                            color: AppColors.themeColor,
                          ),
                          primary: AppColors.themeColor,
                        ),
                        child: const Text('查看更多期間限定',
                            style: TextStyle(
                                color: Color(0xffed7465),
                                fontWeight: FontWeight.w500,
                                fontFamily: "NotoSansCJKtc",
                                fontStyle: FontStyle.normal,
                                fontSize: 15.0),
                            textAlign: TextAlign.center),
                      ),
                    ),
                    Container(color: Colors.white, height: 12.0),
                    const SizedBox(height: 12.0),
                    Container(color: Colors.white, height: 12.0),
                    bigText('主題冒險'),
                    Container(color: Colors.white, height: 11.0),
                    ImageSliderSmall(
                      title: '主題冒險',
                      data: data.themeList,
                      height: width,
                      dotSelectedColor: AppColors.themeColor,
                      dotColor: Colors.black,
                      selectOpacity: 1,
                      unSelectedOpacity: 0.12,
                      dotSize: 8.0,
                      dotPadding: 4.0,
                    ),
                    Container(color: Colors.white, height: 12.0),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      width: double.maxFinite,
                      color: Colors.white,
                      child: OutlinedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute<void>(
                              builder: (_) => BannerPage(
                                termsId: [
                                  ChildrenBean(Id: 51, Name: ''),
                                ],
                                exploreType: ExploreType.themeAdventure,
                              ),
                            ),
                          );
                        },
                        style: OutlinedButton.styleFrom(
                            side: const BorderSide(color: AppColors.themeColor),
                            primary: AppColors.themeColor),
                        child: const Text(
                          '查看更多主題冒險',
                          style: TextStyle(
                              color: Color(0xffed7465),
                              fontWeight: FontWeight.w500,
                              fontFamily: "NotoSansCJKtc",
                              fontStyle: FontStyle.normal,
                              fontSize: 15.0),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    const SizedBox(height: 12.0),
                    Container(color: Colors.white, height: 12.0),
                    bigText('探索台灣'),
                    Container(color: Colors.white, height: 11.0),
                    Container(
                      color: Colors.white,
                      height: 200,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: data.exploreList.length,
                        itemBuilder: (_, int index) {
                          return ExploreCard(
                            data: data.exploreList[index],
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (_) => ExploreDetailPage(
                                    //exploreType: ExploreType.exploreTaiwan,
                                    exploreId: data.exploreList[index].id,
                                    termsId: data.exploreList[index].tags,
                                    exploreTaiwanImageUrl:
                                        data.exploreList[index].imgUrl,
                                    exploreTaiwanTitle:
                                        data.exploreList[index].title,
                                  ),
                                ),
                              );
                            },
                            index: index,
                          );
                        },
                      ),
                    ),
                    Container(color: Colors.white, height: 12.0),
                    const SizedBox(height: 56.0),
                    const Text(
                      '趣趣',
                      style: TextStyle(color: AppColors.themeColor),
                    ),
                    const SizedBox(height: 9.0),
                    const Text(
                      '© Copyright 2020 TRETRIP. All rights reserved.',
                      style: TextStyle(
                        color: Color(0x61000000),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Roboto",
                        fontStyle: FontStyle.normal,
                        fontSize: 8.0,
                      ),
                    ),
                    const SizedBox(
                      height: 24.0,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget bigText(String text) {
    return Container(
      color: Colors.white,
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.only(left: 16.0),
      child: Text(text,
          style: const TextStyle(
              color: AppColors.themeColor,
              fontWeight: FontWeight.w700,
              fontFamily: "NotoSansCJKtc",
              fontStyle: FontStyle.normal,
              fontSize: 25.0)),
    );
  }
}

class ExploreCard extends StatelessWidget {
  final ExploreCardData data;
  final VoidCallback onTap;
  final int index;

  const ExploreCard({
    Key? key,
    required this.onTap,
    required this.index,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String text = '';
    for (ChildrenBean tags in data.tags) {
      text = text + tags.Name! + '\n';
    }

    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.only(left: index == 0 ? 16.0 : 4, right: 4),
        child: Card(
          elevation: 2,
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
          child: Stack(
            alignment: Alignment.center,
            children: [
              SizedBox(
                width: 150.0,
                height: 200.0,
                child: ExtendedImage.network(
                  data.imgUrl,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                child: Text(
                  data.title,
                  style: const TextStyle(
                      color: Color(0xffffffff),
                      fontWeight: FontWeight.w500,
                      fontFamily: "NotoSansCJKtc",
                      fontStyle: FontStyle.normal,
                      fontSize: 17.0),
                ),
                left: 8.0,
                top: 8.0,
              ),
              Positioned(
                child: Text(
                  text,
                  style: const TextStyle(
                      color: Color(0xffffffff),
                      fontWeight: FontWeight.w500,
                      fontFamily: "NotoSansCJKtc",
                      fontStyle: FontStyle.normal,
                      fontSize: 13.0),
                ),
                left: 8.0,
                bottom: 8.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
