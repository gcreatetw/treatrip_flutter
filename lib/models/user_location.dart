import 'package:flutter/material.dart';
import 'package:location/location.dart';

class UserLocation extends ChangeNotifier{
  Location location = Location();

  double? lat;
  double? lng;

  UserLocation({
    this.lat,
    this.lng,
  });

  Future<void> fetch() async {
    final PermissionStatus permission = await location.hasPermission();
    if (permission == PermissionStatus.granted ||
        permission == PermissionStatus.grantedLimited) {
      final LocationData position = await location.getLocation();
      lat = position.latitude;
      lng = position.longitude;
      notifyListeners();
    }
  }

  Future<bool> requestPermission() async {
    final bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      return false;
    }
    PermissionStatus permission = await location.hasPermission();
    if (permission == PermissionStatus.denied) {
      permission = await location.requestPermission();
    }
    if (permission == PermissionStatus.granted ||
        permission == PermissionStatus.grantedLimited) {
      return true;
    }
    return false;
  }
}