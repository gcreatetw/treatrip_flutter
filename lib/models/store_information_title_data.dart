class StoreInformationTitleData {
  final int id;
  final String title;
  final String address;
  final List<String> imageUrls;
  final String star;
  final String comments;
  final int price;
  final String type;

  StoreInformationTitleData({
    required this.id,
    required this.title,
    required this.address,
    required this.imageUrls,
    required this.star,
    required this.comments,
    required this.price,
    required this.type,
  });
}

class StoreInformationCardData {
  final String imageUrl;
  final String name;
  final double stars;

  StoreInformationCardData({
    required this.imageUrl,
    required this.name,
    required this.stars,
  });
}
