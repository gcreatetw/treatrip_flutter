import 'package:treatrip/classification/validate_state.dart';

class UserModel {
  int userId;
  String phone;
  String name;
  String cookie;
  String cookie_name;
  String avatarPhoto;
  String avatarPhotoId;
  String backgroundImage;
  String backgroundImageRowId;
  ValidateState cellPhoneNumberState;

  UserModel({
    this.userId = 0,
    this.phone = '',
    this.name = '',
    this.cookie = '',
    this.cookie_name = '',
    this.avatarPhoto = '',
    this.avatarPhotoId = '',
    this.backgroundImage = '',
    this.backgroundImageRowId = '',
    this.cellPhoneNumberState = ValidateState.empty,
  });
}
