import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/response/discover_taiwan_response.dart';
import 'package:treatrip/models/response/explore/theme_adventure_data_response.dart';
import 'package:treatrip/models/response/explore_new_data_response.dart';
import 'package:treatrip/models/response/general_response.dart';
import 'package:treatrip/models/response/period_limit_data_response.dart';
import 'package:treatrip/models/response/post_list_response.dart';
import 'package:treatrip/ui/explore/banner_page.dart';
import 'package:treatrip/widget/state_controller.dart';

//part of 'package:treatrip/api/explore_api.dart';
//TODO 調整每次加載顯示數量
const int _postNumber = 5;
const int _page = 1;

class BannerPageData extends ChangeNotifier {
  String? bannerImageUrl;
  String? title;
  String? smallTitle;
  List<ChildrenBean>? chipData;
  List<ArticleListData> articleList = [];

  GeneralState state = GeneralState.loading;
  GeneralState loadingState = GeneralState.finish;

  bool noMore = false;

  int postNumber = _postNumber;
  int page = _page;

  bool canLoadMore = false;

  BannerPageData({
    this.bannerImageUrl,
    this.title,
    this.smallTitle,
    this.chipData,
    //this.articleList,
  });

  Future<void> fetch({
    required ExploreType exploreType,
    int? bannerId,
    List<ChildrenBean>? termsId,
    String? exploreTaiwanImageUrl,
    String? exploreTaiwanTitle,
    bool append = false,
  }) async {
    if (!append) {
      state = GeneralState.loading;
      articleList.clear();
      page = _page;
    } else if (canLoadMore) {
      page++;
      loadingState = GeneralState.loading;
      notifyListeners();
      canLoadMore = false;
      debugPrint('now page : $page');
    } else {
      return;
    }
    switch (exploreType) {
      case ExploreType.banner:
        // ExploreApi.instance.getBannerData(
        //   id: bannerId!,
        //   callback: GeneralCallback<BannerDataResponse>(
        //     onError: (e) {
        //       debugPrint(e.message);
        //     },
        //     onSuccess: (BannerDataResponse r) {
        //       final BannerDataBean data = r.data;
        //       bannerImageUrl = data.bannerImageUrl;
        //       title = data.bannerTitle;
        //       smallTitle = data.bannerSubTitle;
        //       chipData = null;
        //       for (Schedule element in data.schedule) {
        //         articleList.add(
        //           ArticleListData(
        //             imageUrl: element.postImageUrl,
        //             title: element.postTitle,
        //             date: element.postDate,
        //             id: int.parse(element.postId),
        //           ),
        //         );
        //       }
        //       state = GeneralState.finish;
        //       notifyListeners();
        //     },
        //   ),
        // );
        break;
      case ExploreType.periodLimit:
        ExploreApi.instance.getPeriodLimitData(
          page: page,
          per_page: postNumber,
          callback: GeneralCallback<PeriodLimitListResponse>(
            onError: (e) {
              loadingState = GeneralState.finish;
              notifyListeners();
              debugPrint(e.message);
            },
            onSuccess: (PeriodLimitListResponse r) {
              //final PeriodLimitDataBean data = r.data;
              bannerImageUrl = null;
              title = null;
              smallTitle = null;
              chipData = null;
              for (final element in r.periodLimitList) {
                articleList.add(
                  ArticleListData(
                    imageUrl: element.ImageUrl,
                    title: element.title,
                    date: null,
                    id: element.id,
                  ),
                );
              }
              canLoadMore = r.periodLimitList.length == postNumber;
              if(r.periodLimitList.length < postNumber){
                noMore = true;
              }
              state = GeneralState.finish;
              loadingState = GeneralState.finish;
              notifyListeners();
            },
          ),
        );
        break;
      case ExploreType.exploreTaiwan:
        final List<int> termsIdFix = [];
        for (ChildrenBean element in termsId!) {
          termsIdFix.add(element.Id);
        }
        ExploreApi.instance.getDiscoverTaiwanData(
          termsId: termsIdFix,
          postNumber: postNumber,
          page: page,
          callback: GeneralCallback<DiscoverTaiwanResponse>(
            onError: (e) {
              loadingState = GeneralState.finish;
              notifyListeners();
              debugPrint(e.message);
            },
            onSuccess: (DiscoverTaiwanResponse r) {
              final DiscoverTaiwanDataBean data = r.data;
              bannerImageUrl = exploreTaiwanImageUrl;
              title = exploreTaiwanTitle;
              smallTitle = null;
              chipData = termsId;
              for (DiscoverTaiwanPostList element
                  in data.discoverTaiwanPostList_scenic_spot) {
                articleList.add(
                  ArticleListData(
                    imageUrl: element.postImageUrl,
                    title: element.postTitle,
                    date: null,
                    id: element.postId,
                    postUrl: element.postPageUrl,
                  ),
                );
              }
              canLoadMore =
                  data.discoverTaiwanPostList_scenic_spot.length == postNumber;
              if(data.discoverTaiwanPostList_scenic_spot.length < postNumber){
                noMore = true;
              }
              state = GeneralState.finish;
              loadingState = GeneralState.finish;
              notifyListeners();
            },
          ),
        );
        break;
      case ExploreType.themeAdventure:
        // final List<int> termsIdFix = [];
        // for (ChildrenBean element in termsId!) {
        //   termsIdFix.add(element.Id);
        // }
        ExploreApi.instance.getThemeAdventureData(
          per_page: postNumber,
          page: page,
          callback: GeneralCallback<ThemeAdventureListResponse>(
            onError: (GeneralResponse e) {
              loadingState = GeneralState.finish;
              notifyListeners();
              debugPrint(e.message);
            },
            onSuccess: (ThemeAdventureListResponse r) {
              for (final element in r.themeAdventureList) {
                articleList.add(
                  ArticleListData(
                    imageUrl: element.ImageUrl,
                    id: element.id,
                    title: element.title,
                    postUrl: element.linkUrl,
                  ),
                );
              }
              //TODO 先拿掉否則會無限加載，James 06/22
              canLoadMore = r.themeAdventureList.length == postNumber;
              if(r.themeAdventureList.length < postNumber){
                noMore = true;
              }
              state = GeneralState.finish;
              loadingState = GeneralState.finish;
              notifyListeners();
            },
          ),
        );
        break;
    }
  }
}

class ArticleListData {
  final int? id;
  final String imageUrl;
  final String? title;
  final String? date;
  final String? postUrl;

  ArticleListData({
    this.id,
    required this.imageUrl,
    this.title,
    this.date,
    this.postUrl,
  });
}
