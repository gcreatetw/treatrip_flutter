import 'package:treatrip/models/response/general_response.dart';

typedef GeneralResponseCallback = void Function(GeneralResponse e);

class GeneralCallback<T> {
  final GeneralResponseCallback onError;
  final void Function(T r) onSuccess;

  GeneralCallback({
    required this.onError,
    required this.onSuccess,
  });
}
