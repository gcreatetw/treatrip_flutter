import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/response/explore_article_response.dart';
import 'package:treatrip/models/response/explore_new_data_response.dart';
import 'package:treatrip/widget/state_controller.dart';

import 'banner_page_data.dart';
import 'general_callback.dart';

const int _page = 1;

class ExploreDetailPageData extends ChangeNotifier {
  String? explorePageImageUrl;
  String? explorePageTitle;
  List<ChildrenBean>? chipData;
  List<ArticleListData> exploreArticleList = [];

  GeneralState state = GeneralState.loading;
  GeneralState loadingState = GeneralState.finish;

  bool noMore = false;

  int page = _page;
  bool canLoadMore = false;
  final List<int> termsIdArray = [];

  ExploreDetailPageData({
    this.explorePageImageUrl,
    this.explorePageTitle,
    this.chipData,
    //this.exploreArticleList,
  });

  Future<void> fetch({
    required List<ChildrenBean>? termsId,
    required String exploreImageUrl,
    required String exploreTitle,
    int? exploreId,
    bool append = false,
    bool open = true,
  }) async {
    if (!append) {
      if(open){
        state = GeneralState.loading;
      }else{
        loadingState = GeneralState.loading;
      }

      exploreArticleList.clear();
      page = _page;
    } else if (canLoadMore) {
      page++;
      loadingState = GeneralState.loading;
      notifyListeners();
      canLoadMore = false;
    } else {
      return;
    }
    termsIdArray.clear();
    for (ChildrenBean element in termsId!) {
      termsIdArray.add(element.Id);
    }

    print(exploreId);
    print(page);
    await ExploreApi.instance.getExploreArticleData(
      terms_id: termsIdArray.isEmpty ? [(exploreId ?? 0)] : termsIdArray,
      per_page: 5,
      page: page,
      callback: GeneralCallback<ExploreArticleResponse>(onError: (e) {
        state = GeneralState.finish;
        loadingState = GeneralState.finish;
        notifyListeners();
        debugPrint(e.message);
      }, onSuccess: (ExploreArticleResponse r) {
        //上方大圖＆相關資訊
        explorePageImageUrl = exploreImageUrl;
        explorePageTitle = exploreTitle;
        chipData = termsId;
        //分類文章區
        //exploreArticleList = [];
        for (final element in r.exploreArticleList) {
          exploreArticleList.add(ArticleListData(
            id: element.Id,
            imageUrl: element.ImageUrl ?? '',
            postUrl: element.linkUrl,
            title: element.Title,
          ));
        }
        canLoadMore =
            r.exploreArticleList.length == 5;
        if(r.exploreArticleList.length < 5){
          noMore = true;
        }
        state = GeneralState.finish;
        loadingState = GeneralState.finish;
        notifyListeners();
      }),
    );
  }
}
