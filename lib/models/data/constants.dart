import 'package:encrypt/encrypt.dart';

class Constants {
  static const bool dev = true;

  //TODO: 全能委託的工項先全部隱藏
  static const bool hideAllFix = true;

  // A
  static const String account = 'account';
  static const String authToken = 'token';
  static const String acctId = 'id';

  // C
  static const String cookie = 'cookie';

  // F
  static const String ffCollection = 'chats';
  static const String ffText = 'text';
  static const String ffType = 'type';
  static const String ffImage = 'image';
  static const String ffSendTime = 'sendTime';
  static const String ffSenderId = 'senderId';

  // H
  static const String hasBeenLogin = 'has_been_login';

  // I
  static const String isSkipLogin = 'is_skip_login';

  // L
  static const String loginTime = 'login_time';

  // N
  static const String name = 'name';

  // P
  static const String phone = 'phone';
  static const String password = 'password';

  // T
  static const String type = 'type';

  // U
  static const String userId = 'userId';

  //
  static final Key key = Key.fromUtf8('=5GYuqwX+V%ctn6ztx3*cd5tG%Rh_8J6');
  static final IV iv = IV.fromUtf8('B?X8bSS_sz#%vawc');
}
