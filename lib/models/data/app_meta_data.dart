import 'package:flutter/cupertino.dart';

class AppMetaData extends ChangeNotifier {
  String faqUrl = 'https://www.luckysolvers.com/faq/';
  String aboutUsUrl = 'https://www.luckysolvers.com/about/';
  String userTermUrl =
      'https://www.luckysolvers.com/%e4%bd%bf%e7%94%a8%e8%80%85%e5%b9%b3%e5%8f%b0%e5%8d%94%e8%ad%b0/';
  String servicePolicyUrl =
      'https://www.luckysolvers.com/%E4%BD%BF%E7%94%A8%E8%80%85%E5%B9%B3%E5%8F%B0%E5%8D%94%E8%AD%B0/';
  String privacyUrl = 'https://www.luckysolvers.com/privacy/';
  String masterServicePolicyUrl =
      'https://www.luckysolvers.com/%E5%B8%AB%E5%82%85%E5%B9%B3%E5%8F%B0%E5%8D%94%E8%AD%B0/';
  String contactUrl = 'https://www.luckysolvers.com/cooperation_proposal/';
  String deleteAccountUrl = 'https://www.luckysolvers.com/delete/';

  DateTime userTermUpdateDate = DateTime(2022);

  DateTime servicePolicyUpdateDate = DateTime(2022);

  AppMetaData();

  Future<void> fetch() async {
    //TODO: 讀取後端原資料
  }
}
