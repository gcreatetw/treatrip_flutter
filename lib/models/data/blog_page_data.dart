import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/response/article_info_data_response.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/utils/utils.dart';
import 'package:treatrip/widget/state_controller.dart';

enum BlogContentType {
  image,
  text,
}

class BlogPageData extends ChangeNotifier {
  String? profileUrl;
  String? bloggerName;
  int? follows;
  String bannerUrl;
  String title;
  String date;
  int viewed;
  String blogContents;
  StoreInformationTitleData storeInformationData;
  GeneralState state = GeneralState.loading;

  BlogPageData({
    this.profileUrl,
    this.bloggerName,
    this.follows,
    required this.bannerUrl,
    required this.title,
    required this.date,
    required this.viewed,
    required this.blogContents,
    required this.storeInformationData,
  });

  factory BlogPageData.init() {
    return BlogPageData(
      profileUrl: '',
      bloggerName: '',
      follows: 0,
      bannerUrl: '',
      title: '',
      date: '',
      viewed: 0,
      blogContents: '',
      storeInformationData: StoreInformationTitleData(
        id: 0,
        title: '',
        address: '',
        imageUrls: <String>[],
        star: '0',
        comments: '0',
        price: 0,
        type: '',
      ),
    );
  }

  Future<void> fetch(int postId) async {
    state = GeneralState.loading;
    notifyListeners();
    ExploreApi.instance.getArticleInfoData(
      postId: postId,
      callback: GeneralCallback<ArticleInfoDataResponse>(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (ArticleInfoDataResponse r) {
          ArticleInfoDataBean data = r.data;
          profileUrl = data.bloggerAvatar.isEmpty ? null : data.bloggerAvatar;
          bloggerName = data.bloggerName.isEmpty ? null : data.bloggerName;
          follows = data.bloggerFollowCount;
          bannerUrl = data.postImageUrl;
          title = data.postTitle;
          date = data.postDate;
          viewed = int.parse(data.postPapularityCount.toString());
          blogContents = Utils.fixHtml(data.postContent);
          //TODO from API
          storeInformationData = StoreInformationTitleData(
              id: 0,
              title: 'HOOTERS美式餐廳',
              address: '台北市松山區民生東路四段56巷3弄10號',
              imageUrls: <String>[
                'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
                'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
                'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
                'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
              ],
              star: '3.8',
              comments: '127',
              price: 3,
              type: '義大利菜');
          state = GeneralState.finish;
          notifyListeners();
        },
      ),
    );
  }
}
