import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../response/gourmet_lodging_list_response.dart';

enum SearchSort {
  gourmet,
  attractions,
  lodging,
}

class SearchResultData extends ChangeNotifier {
  final List<SearchResultTitleData> foodList;
  final List<SearchResultTitleData> viewList;
  final List<SearchResultTitleData> hotelList;
  List<ChipListData> categoryList;

  //TODO 改為list 且資料由上面list取得
  StoreInformationTitleData selectStoreData;
  GeneralState state = GeneralState.loading;

  SearchResultData({
    required this.foodList,
    required this.viewList,
    required this.hotelList,
    required this.categoryList,
    required this.selectStoreData,
  });

  factory SearchResultData.init() {
    return SearchResultData(
      foodList: [],
      viewList: [],
      hotelList: [],
      categoryList: [
        ChipListData(
          title: '',
          selected: false,
        )
      ],
      selectStoreData: StoreInformationTitleData(
        id: 0,
        title: '',
        address: '',
        imageUrls: <String>[],
        star: '0',
        comments: '0',
        price: 0,
        type: '',
      ),
    );
  }

  Future<void> fetchSearchData() async {
    state = GeneralState.loading;
    notifyListeners();

    await StoreApi.instance.getGourmetLodgingListData(
      terms_id: 0,
      city: '新竹市',
      lng: 0,
      lat: 0,
      per_page: 10,
      callback: GeneralCallback<GourmetLodgingListResponse>(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (GourmetLodgingListResponse r) {
          foodList.clear();
          viewList.clear();
          hotelList.clear();
          for (GourmetDataBean element in r.gourmetList) {
            final List<String> list = [];
            for (final imageElement in element.Images) {
              list.add(imageElement.url);
            }
            foodList.add(SearchResultTitleData(
                id: element.Id,
                imageUrl: list[0],
                storeName: element.Title,
                stars: 3.8,
                comments: 127,
                price: 3,
                storeType: element.Category));
          }
          for (AttractionsDataBean element in r.attractionsList) {
            final List<String> list = [];
            for (final imageElement in element.Images) {
              list.add(imageElement.url);
            }
            viewList.add(SearchResultTitleData(
                id: element.Id,
                imageUrl: list[0],
                storeName: element.Title,
                stars: 3.8,
                comments: 127,
                price: 3,
                storeType: element.Category));
          }
          for (LodgingDataBean element in r.lodgingList) {
            final List<String> list = [];
            for (final imageElement in element.Images) {
              list.add(imageElement.url);
            }
            hotelList.add(SearchResultTitleData(
                id: element.Id,
                imageUrl: list[0],
                storeName: element.Title,
                stars: 3.8,
                comments: 127,
                price: 3,
                storeType: element.Category));
          }
        },
      ),
    );
    state = GeneralState.finish;
    notifyListeners();
  }

  Future<void> fetch() async {
    state = GeneralState.loading;
    notifyListeners();
    await Future<void>.delayed(const Duration(milliseconds: 2000));

    // _loadFoodSample();
    //
    // _loadViewSample();
    //
    // _loadHotelSample();

    categoryList.addAll([
      ChipListData(
        title: '台菜餐廳',
        selected: false,
      ),
      ChipListData(
        title: '火鍋餐廳',
        selected: false,
      ),
      ChipListData(
        title: '火鍋餐廳',
        selected: false,
      ),
      ChipListData(
        title: '火鍋餐廳',
        selected: false,
      ),
    ]);

    selectStoreData = StoreInformationTitleData(
      id: 0,
      title: 'HOOTERS美式餐廳',
      address: '台北市松山區民生東路四段56巷3弄10號',
      imageUrls: <String>[
        'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
        'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
        'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
        'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      ],
      star: '3.8',
      comments: '127',
      price: 3,
      type: '義大利菜',
    );
    state = GeneralState.finish;
    notifyListeners();
  }

// void _loadFoodSample() {
//   foodList.clear();
//   foodList.addAll(<SearchResultTitleData>[
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//   ]);
// }

// void _loadViewSample() {
//   viewList.clear();
//   viewList.addAll(<SearchResultTitleData>[
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//   ]);
// }

// void _loadHotelSample() {
//   hotelList.clear();
//   hotelList.addAll(<SearchResultTitleData>[
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳'),
//     SearchResultTitleData(
//         imageUrl:
//             'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
//         storeName: '富錦樹台菜香檳',
//         stars: 3.8,
//         comments: 127,
//         price: 3,
//         storeType: '台菜餐廳',),
//   ]);
// }
}

class SearchResultTitleData {
  final int id;
  final String imageUrl;
  final String storeName;
  final double stars;
  final int comments;
  final int price;
  final String storeType;

  SearchResultTitleData({
    required this.id,
    required this.imageUrl,
    required this.storeName,
    required this.stars,
    required this.comments,
    required this.price,
    required this.storeType,
  });
}

class ChipListData {
  final String title;
  bool selected;

  ChipListData({
    required this.title,
    required this.selected,
  });
}
