import 'package:flutter/material.dart';
import 'package:treatrip/models/response/banner_response.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/api/app_api.dart';

import '../general_callback.dart';
import '../image_slider_data.dart';

class BannerData extends ChangeNotifier {
  List<String> bannerList;
  List<String> bannerLinkList;
  List<String> bannerFoodList;
  List<String> bannerFoodLinkList;
  List<String> bannerTravelList;
  List<String> bannerTravelLinkList;

  GeneralState state = GeneralState.loading;

  BannerData({
    required this.bannerList,
    required this.bannerLinkList,
    required this.bannerFoodList,
    required this.bannerFoodLinkList,
    required this.bannerTravelList,
    required this.bannerTravelLinkList,
  });

  factory BannerData.init() {
    return BannerData(
      bannerList: <String>[],
      bannerLinkList: <String>[],
      bannerFoodList: <String>[],
      bannerFoodLinkList: <String>[],
      bannerTravelList: <String>[],
      bannerTravelLinkList: <String>[],
    );
  }

  Future<void> fetchBannerData() async {
    state = GeneralState.loading;
    notifyListeners();
    ExploreApi.instance.getAllBannerData(
      callback: GeneralCallback(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (BannerResponse r) {
          bannerList.clear();
          for (HomeBean element in r.home) {
            bannerList.add(element.ImageUrl);
            bannerLinkList.add(element.linkUrl);
          }
          for (FoodBean element in r.food) {
            bannerFoodList.add(element.ImageUrl);
            bannerFoodLinkList.add(element.linkUrl);
          }
          for (TravelBean element in r.travel) {
            bannerTravelList.add(element.ImageUrl);
            bannerTravelLinkList.add(element.linkUrl);
          }
          state = GeneralState.finish;
          notifyListeners();
        },
      ),
    );
  }
}
