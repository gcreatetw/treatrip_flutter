import 'package:flutter/cupertino.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/response/gourmet_lodging_list_response.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../general_callback.dart';

class FoodPageData extends ChangeNotifier {
  List<FoodCategory> foodCategoryList;
  List<String> bannerImageList;
  List<StoreInformationTitleData> storeList;
  GeneralState state = GeneralState.loading;

  FoodPageData({
    required this.foodCategoryList,
    required this.bannerImageList,
    required this.storeList,
  });

  factory FoodPageData.init() {
    return FoodPageData(
      foodCategoryList: <FoodCategory>[],
      bannerImageList: <String>[],
      storeList: <StoreInformationTitleData>[],
    );
  }

  Future<void> fetch() async {
    state = GeneralState.loading;
    await Future<void>.delayed(const Duration(milliseconds: 500));
    _loadSampleSata();
    state = GeneralState.finish;
    notifyListeners();
  }

  Future<void> fetchGourmetData() async {
    state = GeneralState.loading;
    notifyListeners();

    await StoreApi.instance.getGourmetLodgingListData(
      terms_id: 0,
      city: '新竹市',
      lng: 0.0,
      lat: 0.0,
      per_page: 10,
      callback: GeneralCallback<GourmetLodgingListResponse>(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (GourmetLodgingListResponse r) {
          storeList.clear();
          for (GourmetDataBean element in r.gourmetList) {
            final List<String> list = [];
            for (final imageElement in element.Images) {
              list.add(imageElement.url);
            }
            storeList.add(StoreInformationTitleData(
              id: element.Id,
              title: element.Title,
              address: element.Address,
              imageUrls: list,
              star: element.Rating,
              comments: element.User_ratings_total,
              price: 3,
              type: element.Category,
            ));
            // if (element.Category == '旅遊') {
            //
            // }
          }
          for (LodgingDataBean element in r.lodgingList) {
            final List<String> list = [];
            for (final imageElement in element.Images) {
              list.add(imageElement.url);
            }
            storeList.add(StoreInformationTitleData(
              id: element.Id,
              title: element.Title,
              address: element.Address,
              imageUrls: list,
              star: element.Rating,
              comments: element.User_ratings_total,
              price: 3,
              type: element.Category,
            ));
          }
        },
      ),
    );
    state = GeneralState.finish;
    notifyListeners();
  }

  void _loadSampleSata() {
    foodCategoryList.clear();
    foodCategoryList.addAll(<FoodCategory>[
      FoodCategory(
          imageUrl:
              'https://pic.pimg.tw/jesi0908/1599108761-2343504893-g_n.jpg',
          title: '美式餐廳',
          recommendCount: 75),
      FoodCategory(
          imageUrl:
              'https://pic.pimg.tw/jesi0908/1599108761-2343504893-g_n.jpg',
          title: '義法料理',
          recommendCount: 28),
    ]);
    bannerImageList.clear();
    bannerImageList.addAll(<String>[
      'https://www.treatrip.com/wp-content/uploads/keelung-delicacy7.jpg',
      'https://www.treatrip.com/wp-content/uploads/keelung-delicacy7.jpg',
      'https://www.treatrip.com/wp-content/uploads/keelung-delicacy7.jpg',
      'https://www.treatrip.com/wp-content/uploads/keelung-delicacy7.jpg',
    ]);
  }
}

class FoodCategory {
  final String imageUrl;
  final String title;
  final int recommendCount;

  FoodCategory({
    required this.imageUrl,
    required this.title,
    required this.recommendCount,
  });
}
