import 'package:flutter/material.dart';
import 'package:treatrip/widget/state_controller.dart';

class StoreImageData extends ChangeNotifier{
  List<String> imageList;
  GeneralState state = GeneralState.loading;

  StoreImageData({
    required this.imageList,

  });

  factory StoreImageData.init() {
    return StoreImageData(
      imageList: [],
    );
  }

  Future<void> fetch()async {
    state = GeneralState.loading;
    await Future<void>.delayed(const Duration(milliseconds: 500));
    // _loadSampleData();
    state = GeneralState.finish;
    notifyListeners();
  }

  void _loadSampleData(){
    imageList.clear();
    imageList.addAll(<String>[
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
    ]);
  }
}
