import 'package:flutter/material.dart';

class KeywordData extends ChangeNotifier {
  List<String> hotList;
  List<String> searchHistory;

  KeywordData({
    required this.hotList,
    required this.searchHistory,
  });

  factory KeywordData.init() {
    return KeywordData(
      hotList: [],
      searchHistory: [],
    );
  }

  Future<void> fetchHotSearch() async {
    await Future<void>.delayed(const Duration(milliseconds: 300));
    _loadHotSample();
    notifyListeners();
  }

  Future<void> fetchSearchHistory() async {
    await Future<void>.delayed(const Duration(milliseconds: 300));
    _loadAutoSample();
    notifyListeners();
  }

  void _loadHotSample() {
    hotList.clear();
    hotList.addAll([
      // '九寨十鍋',
      // '八條老宅',
      // '麻辣鍋',
      // '商旅',
      // '樂麵屋',
      // '鬼金棒',
      '新竹市',
    ]);
  }

  void _loadAutoSample() {
    searchHistory.clear();
    searchHistory.addAll([
      // '牛排',
      // '法式料理',
      // '下午茶',
    ]);
  }
}
