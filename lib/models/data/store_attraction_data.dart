import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/image_slider_data.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/utils/location_util.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/models/response/gourmet_lodging_detail_response.dart';

class StoreAttractionData extends ChangeNotifier {
  String storeName;
  String storeBannerUrl;
  List<ImagesDetailBean> storeImageUrlList;
  String storeStartCount;
  String storeCommentCount;
  int storePrice;
  String storeAddress;
  String storeType;
  String storeOpenTime;
  int storeViewed;
  List<ImageSliderBlogData> bloggerPostList;
  String payment;
  String webUrl;
  String contactPhoneNumber;
  String reservePhoneNumber;
  String menuUrl;
  String reserveUrl;
  String storeOpeningDay;
  String storeIntroduction;
  List<String> storeBusinessHours;
  List<StoreInformationCardData> recommendData;
  List<StoreInformationData> storeInformationData;
  GeneralState state = GeneralState.loading;
  List<ImageTitle> imageTitleList;
  String weblink;
  LatLng latLng = LatLng(25.03, 121.53);

  StoreAttractionData({
    required this.storeName,
    required this.storeBannerUrl,
    required this.storeImageUrlList,
    required this.storeStartCount,
    required this.storeCommentCount,
    required this.storePrice,
    required this.storeAddress,
    required this.storeType,
    required this.storeOpenTime,
    required this.storeViewed,
    required this.bloggerPostList,
    required this.payment,
    required this.webUrl,
    required this.contactPhoneNumber,
    required this.reservePhoneNumber,
    required this.menuUrl,
    required this.reserveUrl,
    required this.storeOpeningDay,
    required this.storeIntroduction,
    required this.storeBusinessHours,
    required this.recommendData,
    required this.storeInformationData,
    required this.imageTitleList,
    required this.weblink,
  });

  factory StoreAttractionData.init() {
    return StoreAttractionData(
      storeName: '',
      storeBannerUrl: '',
      storeImageUrlList: <ImagesDetailBean>[],
      storeStartCount: '',
      storeCommentCount: '',
      storePrice: 0,
      storeAddress: '',
      storeType: '',
      storeOpenTime: '',
      storeViewed: 0,
      bloggerPostList: <ImageSliderBlogData>[],
      payment: '',
      webUrl: '',
      contactPhoneNumber: '',
      reservePhoneNumber: '',
      menuUrl: '',
      reserveUrl: '',
      weblink: '',
      recommendData: <StoreInformationCardData>[],
      storeInformationData: <StoreInformationData>[],
      storeBusinessHours: [],
      storeIntroduction: '',
      storeOpeningDay: '',
      imageTitleList: [],
    );
  }

  Future<void> fetch() async {
    state = GeneralState.loading;
    await Future<void>.delayed(const Duration(milliseconds: 500));
    //_loadSampleData();
    state = GeneralState.finish;
    notifyListeners();
  }

  //店家單一頁
  Future<void> fetchDetailPageData({
    required int post_id,
  }) async {
    state = GeneralState.loading;
    notifyListeners();

    await StoreApi.instance.getGourmetLodgingDetailData(
      post_id: post_id,
      callback: GeneralCallback<GourmetLodgingDetailResponse>(onError: (e) {
        state = GeneralState.finish;
        notifyListeners();
        debugPrint(e.message);
      }, onSuccess: (GourmetLodgingDetailResponse r) async {
        storeName = r.Title;

        storeBannerUrl =
            'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg';
        if(r.Images.isNotEmpty){
          storeBannerUrl = r.Images[0].url;
        }
        storeImageUrlList = r.Images;
        storeStartCount = r.Rating;
        storeCommentCount = r.User_ratings_total;
        storePrice = 3;
        storeAddress = r.Address;
        latLng = await LocationUtil.instance.getAddressLocation(storeAddress) ?? LatLng(25.03, 121.53);
        storeType = r.Category;
        storeOpenTime = '今日開放時間 11:30–14:00, 14:30–16:30, 17:30–21:30';
        bloggerPostList.addAll(<ImageSliderBlogData>[
          ImageSliderBlogData(
            profileUrl:
                'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
            name: '趣趣部落客˙Ronnie Morgan',
            content: '發現台北東區優質下午茶，犒賞自己趁現在發文內容有時候會很長，最多三行最多三行最多三行最多三行最多三行超過',
            imagePaths: [
              'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
              'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
              'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
              'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
            ],
          ),
        ]);
        payment = '現金．信用卡．簽帳金融卡';
        webUrl = r.website;
        contactPhoneNumber = r.phone;
        reservePhoneNumber = r.phone;
        menuUrl = 'www.shinyeh.com.tw/';
        reserveUrl = 'www.shinyeh.com.tw/';
        storeOpeningDay = '1999/4/15';
        storeIntroduction =
            '';
        storeBusinessHours.clear();
        storeBusinessHours = r.opentime ?? [];
        recommendData.clear();
        recommendData.addAll(<StoreInformationCardData>[
          StoreInformationCardData(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
              name: 'Domo De Sardegna義大利餐廳',
              stars: 3.8),
          StoreInformationCardData(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
              name: '食采集思(民生店)',
              stars: 3.8),
          StoreInformationCardData(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
              name: 'Domo De Sardegna義大利餐廳',
              stars: 3.8),
          StoreInformationCardData(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
              name: '食采集思(民生店)',
              stars: 3.8),
        ]);
        storeInformationData.clear();
        storeInformationData.addAll(<StoreInformationData>[
          StoreInformationData(
            title: '特色',
            featureList: <StoreFeatureData>[
              StoreFeatureData(has: true, name: '由女性經營'),
              StoreFeatureData(has: false, name: '室外雅座'),
            ],
          ),
          StoreInformationData(
            title: '付款方式',
            featureList: <StoreFeatureData>[
              StoreFeatureData(has: true, name: 'NFC行動支付'),
              StoreFeatureData(has: false, name: '簽帳金融卡'),
              StoreFeatureData(has: true, name: '信用卡'),
            ],
          ),
          StoreInformationData(
            title: '客層族群',
            featureList: <StoreFeatureData>[
              StoreFeatureData(has: true, name: 'LGBT'),
              StoreFeatureData(has: true, name: '適合闔家光臨'),
            ],
          ),
          StoreInformationData(
            title: '無障礙程度',
            featureList: <StoreFeatureData>[
              StoreFeatureData(has: true, name: '無障礙洗手間'),
              StoreFeatureData(has: true, name: '無障礙電梯'),
            ],
          ),
          StoreInformationData(
            title: '產品/服務',
            featureList: <StoreFeatureData>[
              StoreFeatureData(has: true, name: '優惠時段'),
              StoreFeatureData(has: true, name: '兒童菜單'),
              StoreFeatureData(has: true, name: '可單點咖啡'),
              StoreFeatureData(has: true, name: '吃到飽'),
              StoreFeatureData(has: true, name: '咖啡'),
              StoreFeatureData(has: true, name: '招牌菜套餐'),
              StoreFeatureData(has: true, name: '提供素食料理'),
            ],
          ),
        ]);
        imageTitleList.clear();
        imageTitleList.addAll(<ImageTitle>[
          ImageTitle(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
              name: '全部'),
          ImageTitle(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
              name: '產品'),
          ImageTitle(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
              name: '室內'),
          ImageTitle(
              imageUrl:
                  'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
              name: '全部'),
          // ImageTitle(
          //     imageUrl:
          //         'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
          //     name: '全部'),
        ]);
        weblink = r.post_url;
        state = GeneralState.finish;
        notifyListeners();
      }),
    );

  }
}

class StoreInformationData {
  final String title;
  final List<StoreFeatureData> featureList;

  StoreInformationData({
    required this.title,
    required this.featureList,
  });
}

class StoreFeatureData {
  final bool has;
  final String name;

  StoreFeatureData({
    required this.has,
    required this.name,
  });
}

class ImageTitle {
  final String imageUrl;
  final String name;

  ImageTitle({
    required this.imageUrl,
    required this.name,
  });
}
