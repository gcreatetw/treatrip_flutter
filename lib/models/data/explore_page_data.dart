import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/image_slider_data.dart';
import 'package:treatrip/models/response/explore_data_response.dart';
import 'package:treatrip/models/response/explore_new_data_response.dart';
import 'package:treatrip/models/response/general_response.dart';
import 'package:treatrip/models/response/post_list_response.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../response/explore/theme_adventure_data_response.dart';
import '../response/period_limit_data_response.dart';

class ExplorePageData extends ChangeNotifier {
  List<ImageSliderBigData> bannerList;

  List<ImageSliderSmallData> periodList;

  List<ImageSliderSmallData> themeList;

  List<ExploreCardData> exploreList;

  List<ImageSliderBlogData> travelList;

  List<ImageSliderBlogData> natureList;

  GeneralState state = GeneralState.loading;
  GeneralState periodState = GeneralState.loading;
  GeneralState adventureState = GeneralState.loading;

  ExplorePageData({
    required this.bannerList,
    required this.periodList,
    required this.themeList,
    required this.exploreList,
    required this.travelList,
    required this.natureList,
  });

  factory ExplorePageData.init() {
    return ExplorePageData(
      bannerList: <ImageSliderBigData>[],
      periodList: <ImageSliderSmallData>[],
      themeList: <ImageSliderSmallData>[],
      exploreList: <ExploreCardData>[],
      travelList: <ImageSliderBlogData>[],
      natureList: <ImageSliderBlogData>[],
    );
  }

  Future<void> fetchAll() async {
    state = GeneralState.loading;
    notifyListeners();
    await Future.wait([
      fetch(),
      fetchThemeAdventure(),
      fetchExploreFieldData(),
    ]);
    state = GeneralState.finish;
    notifyListeners();
  }

  Future<void> fetch() async {

    await ExploreApi.instance.getPeriodLimitData(
      page: 1,
      per_page: 5,
      callback: GeneralCallback(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (PeriodLimitListResponse r) {
          periodList.clear();
          for (GetPeriodLimitListResponse element in r.periodLimitList) {
            periodList.add(ImageSliderSmallData(
              text: element.title,
              imgUrl: element.ImageUrl,
              id: element.id,
            ));
          }

          //fetchExploreFieldData();

        },
      ),
    );
  }

  //主題冒險 (正式)
  Future<void> fetchThemeAdventure() async {

    await ExploreApi.instance.getThemeAdventureData(
      per_page: 5,
      page: 1,
      callback: GeneralCallback(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (ThemeAdventureListResponse r) {
          for (GetThemeAdventureDataResponse element in r.themeAdventureList) {
            themeList.add(ImageSliderSmallData(
              text: element.title,
              imgUrl: element.ImageUrl,
              id: element.id,
            ));
          }
        },
      ),
    );
  }

  //主題冒險 (old)
  // Future<void> fetchThemeAdventure() async {
  //   ExploreApi.instance.getPostListData(
  //     terms_id: [51],
  //     per_page: 5,
  //     callback: GeneralCallback<PostListResponse>(
  //       onError: (GeneralResponse e) {
  //         debugPrint(e.message);
  //       },
  //       onSuccess: (PostListResponse r) {
  //         for (final element in r.postList) {
  //           themeList.add(ImageSliderSmallData(
  //             id: element.Id,
  //             imgUrl: element.ImageUrl,
  //             text: element.Title,
  //             link: element.linkUrl,
  //           ));
  //         }
  //         state = GeneralState.finish;
  //         notifyListeners();
  //       },
  //     ),
  //   );
  // }

  Future<void> fetchExploreFieldData() async {


    await ExploreApi.instance.getExploreNewData(
      callback: GeneralCallback<ExploreNewDataResponse>(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (ExploreNewDataResponse r) {
          exploreList.clear();
          for (GetExploreDataResponse element in r.exploreDataList!) {
            exploreList.add(
              ExploreCardData(
                  imgUrl: element.ImageUrl,
                  title: element.Name,
                  tags: element.Children,
                  id: element.Id),
            );
          }
        },
      ),
    );
  }
}
