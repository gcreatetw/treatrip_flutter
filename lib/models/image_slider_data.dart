import 'package:treatrip/models/response/explore_new_data_response.dart';

class ImageSliderBigData {
  int id;
  String imgUrl;
  String bigText;
  String smallText;
  String bannerLink;

  ImageSliderBigData({
    required this.id,
    required this.imgUrl,
    required this.bigText,
    required this.smallText,
    required this.bannerLink,
  });
}

class ImageSliderSmallData {
  int id;
  String imgUrl;
  String text;
  String? link;

  ImageSliderSmallData({
    required this.id,
    required this.imgUrl,
    required this.text,
    this.link,
  });
}

class ExploreCardData {
  int id;
  String imgUrl;
  String title;
  List<ChildrenBean> tags;

  ExploreCardData({
    required this.id,
    required this.imgUrl,
    required this.title,
    required this.tags,
  });
}

class ExplorePostTags {
  int id;
  String name;

  ExplorePostTags({
    required this.id,
    required this.name,
  });
}

class ImageSliderBlogData {
  String profileUrl;
  String? imageUrl;
  String name;
  String? title;
  String? address;
  String content;
  List<String> imagePaths;

  ImageSliderBlogData({
    required this.profileUrl,
    required this.name,
    this.imageUrl,
    this.title,
    this.address,
    required this.content,
    required this.imagePaths,
  });
}
