// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_version_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppVersionResponse _$AppVersionResponseFromJson(Map<String, dynamic> json) =>
    AppVersionResponse(
      androidVersions: (json['Android_version'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      iosVersions: (json['IOS_version'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$AppVersionResponseToJson(AppVersionResponse instance) =>
    <String, dynamic>{
      'Android_version': instance.androidVersions,
      'IOS_version': instance.iosVersions,
    };
