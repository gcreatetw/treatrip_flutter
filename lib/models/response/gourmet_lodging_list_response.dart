//全台美食（住宿）列表
import 'package:json_annotation/json_annotation.dart';
part 'gourmet_lodging_list_response.g.dart';

@JsonSerializable()
class GourmetLodgingListResponse {
  @JsonKey(name: 'food')
  List<GourmetDataBean> gourmetList;
  @JsonKey(name: 'attractions')
  List<AttractionsDataBean> attractionsList;
  @JsonKey(name: 'stay')
  List<LodgingDataBean> lodgingList;

  GourmetLodgingListResponse({
    required this.gourmetList,
    required this.attractionsList,
    required this.lodgingList,
  });

  factory GourmetLodgingListResponse.fromJson(Map<String, dynamic> json) =>
      _$GourmetLodgingListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GourmetLodgingListResponseToJson(this);
}

@JsonSerializable()
class GourmetDataBean {
  int Id;
  String Category;
  String Title;
  String Address;
  List<ImagesBean> Images;
  String Rating;
  String User_ratings_total;

  GourmetDataBean({
    required this.Id,
    required this.Category,
    required this.Title,
    required this.Address,
    required this.Images,
    required this.Rating,
    required this.User_ratings_total,
  });

  factory GourmetDataBean.fromJson(Map<String, dynamic> json) =>
      _$GourmetDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$GourmetDataBeanToJson(this);
}

@JsonSerializable()
class AttractionsDataBean {
  int Id;
  String Category;
  String Title;
  String Address;
  List<ImagesBean> Images;
  String Rating;
  String User_ratings_total;

  AttractionsDataBean({
    required this.Id,
    required this.Category,
    required this.Title,
    required this.Address,
    required this.Images,
    required this.Rating,
    required this.User_ratings_total,
  });

  factory AttractionsDataBean.fromJson(Map<String, dynamic> json) =>
      _$AttractionsDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$AttractionsDataBeanToJson(this);
}

@JsonSerializable()
class LodgingDataBean {
  int Id;
  String Category;
  String Title;
  String Address;
  List<ImagesBean> Images;
  String Rating;
  String User_ratings_total;

  LodgingDataBean({
    required this.Id,
    required this.Category,
    required this.Title,
    required this.Address,
    required this.Images,
    required this.Rating,
    required this.User_ratings_total,
  });

  factory LodgingDataBean.fromJson(Map<String, dynamic> json) =>
      _$LodgingDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$LodgingDataBeanToJson(this);
}

@JsonSerializable()
class ImagesBean {
  List<String> html_attributions;
  String url;

  ImagesBean({required this.html_attributions, required this.url});

  factory ImagesBean.fromJson(Map<String, dynamic> json) =>
      _$ImagesBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ImagesBeanToJson(this);
}
