import 'package:json_annotation/json_annotation.dart';

part 'banner_response.g.dart';

@JsonSerializable()
class BannerResponse {
  List<HomeBean> home;
  List<FoodBean> food;
  List<TravelBean> travel;

  BannerResponse({
    required this.home,
    required this.food,
    required this.travel,
  });

  factory BannerResponse.fromJson(Map<String, dynamic> json) =>
      _$BannerResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BannerResponseToJson(this);
}

@JsonSerializable()
class HomeBean {
  String ImageUrl;
  String linkUrl;

  HomeBean({
    required this.ImageUrl,
    required this.linkUrl,
  });

  factory HomeBean.fromJson(Map<String, dynamic> json) =>
      _$HomeBeanFromJson(json);

  Map<String, dynamic> toJson() => _$HomeBeanToJson(this);
}

@JsonSerializable()
class FoodBean {
  String ImageUrl;
  String linkUrl;

  FoodBean({
    required this.ImageUrl,
    required this.linkUrl,
  });

  factory FoodBean.fromJson(Map<String, dynamic> json) =>
      _$FoodBeanFromJson(json);

  Map<String, dynamic> toJson() => _$FoodBeanToJson(this);
}

@JsonSerializable()
class TravelBean {
  String ImageUrl;
  String linkUrl;

  TravelBean({
    required this.ImageUrl,
    required this.linkUrl,
  });

  factory TravelBean.fromJson(Map<String, dynamic> json) =>
      _$TravelBeanFromJson(json);

  Map<String, dynamic> toJson() => _$TravelBeanToJson(this);
}
