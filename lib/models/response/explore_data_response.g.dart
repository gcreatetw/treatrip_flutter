// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'explore_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExploreDataResponse _$ExploreDataResponseFromJson(Map<String, dynamic> json) =>
    ExploreDataResponse(
      error: json['error'] as int,
      data: ExploreDataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ExploreDataResponseToJson(
        ExploreDataResponse instance) =>
    <String, dynamic>{
      'error': instance.error,
      'data': instance.data,
    };

ExploreDataBean _$ExploreDataBeanFromJson(Map<String, dynamic> json) =>
    ExploreDataBean(
      banner: (json['banner'] as List<dynamic>)
          .map((e) => BannerDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      activityTimeInterval: (json['activityTimeInterval'] as List<dynamic>)
          .map((e) => ActivityTimeInterval.fromJson(e as Map<String, dynamic>))
          .toList(),
      themeAdventure: (json['themeAdventure'] as List<dynamic>)
          .map((e) => ThemeAdventure.fromJson(e as Map<String, dynamic>))
          .toList(),
      discoverTaiwan: (json['discoverTaiwan'] as List<dynamic>)
          .map((e) => DiscoverTaiwan.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ExploreDataBeanToJson(ExploreDataBean instance) =>
    <String, dynamic>{
      'banner': instance.banner,
      'activityTimeInterval': instance.activityTimeInterval,
      'themeAdventure': instance.themeAdventure,
      'discoverTaiwan': instance.discoverTaiwan,
    };

BannerDataBean _$BannerDataBeanFromJson(Map<String, dynamic> json) =>
    BannerDataBean(
      bannerId: json['bannerId'] as int,
      bannerTitle: json['bannerTitle'] as String,
      bannerSubTitle: json['bannerSubTitle'] as String,
      bannerImageUrl: json['bannerImageUrl'] as String,
    );

Map<String, dynamic> _$BannerDataBeanToJson(BannerDataBean instance) =>
    <String, dynamic>{
      'bannerId': instance.bannerId,
      'bannerTitle': instance.bannerTitle,
      'bannerSubTitle': instance.bannerSubTitle,
      'bannerImageUrl': instance.bannerImageUrl,
    };

ActivityTimeInterval _$ActivityTimeIntervalFromJson(
        Map<String, dynamic> json) =>
    ActivityTimeInterval(
      activityId: json['activityId'] as int,
      activityTitle: json['activityTitle'] as String,
      activityImageUrl: json['activityImageUrl'] as String,
    );

Map<String, dynamic> _$ActivityTimeIntervalToJson(
        ActivityTimeInterval instance) =>
    <String, dynamic>{
      'activityId': instance.activityId,
      'activityTitle': instance.activityTitle,
      'activityImageUrl': instance.activityImageUrl,
    };

ThemeAdventure _$ThemeAdventureFromJson(Map<String, dynamic> json) =>
    ThemeAdventure(
      themeId: json['themeId'] as int,
      themeTitle: json['themeTitle'] as String,
      themeImageUrl: json['themeImageUrl'] as String,
    );

Map<String, dynamic> _$ThemeAdventureToJson(ThemeAdventure instance) =>
    <String, dynamic>{
      'themeId': instance.themeId,
      'themeTitle': instance.themeTitle,
      'themeImageUrl': instance.themeImageUrl,
    };

DiscoverTaiwan _$DiscoverTaiwanFromJson(Map<String, dynamic> json) =>
    DiscoverTaiwan(
      discoverPostId: json['discoverPostId'] as int,
      discoverPostCategory: json['discoverPostCategory'] as String,
      discoverPostTags: (json['discoverPostTags'] as List<dynamic>)
          .map((e) => DiscoverPostTags.fromJson(e as Map<String, dynamic>))
          .toList(),
      discoverPostImageUrl: json['discoverPostImageUrl'] as String,
    );

Map<String, dynamic> _$DiscoverTaiwanToJson(DiscoverTaiwan instance) =>
    <String, dynamic>{
      'discoverPostId': instance.discoverPostId,
      'discoverPostCategory': instance.discoverPostCategory,
      'discoverPostTags': instance.discoverPostTags,
      'discoverPostImageUrl': instance.discoverPostImageUrl,
    };

DiscoverPostTags _$DiscoverPostTagsFromJson(Map<String, dynamic> json) =>
    DiscoverPostTags(
      term_id: json['term_id'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$DiscoverPostTagsToJson(DiscoverPostTags instance) =>
    <String, dynamic>{
      'term_id': instance.term_id,
      'name': instance.name,
    };
