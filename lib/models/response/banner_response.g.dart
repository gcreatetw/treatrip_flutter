// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BannerResponse _$BannerResponseFromJson(Map<String, dynamic> json) =>
    BannerResponse(
      home: (json['home'] as List<dynamic>)
          .map((e) => HomeBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      food: (json['food'] as List<dynamic>)
          .map((e) => FoodBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      travel: (json['travel'] as List<dynamic>)
          .map((e) => TravelBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BannerResponseToJson(BannerResponse instance) =>
    <String, dynamic>{
      'home': instance.home,
      'food': instance.food,
      'travel': instance.travel,
    };

HomeBean _$HomeBeanFromJson(Map<String, dynamic> json) => HomeBean(
      ImageUrl: json['ImageUrl'] as String,
      linkUrl: json['linkUrl'] as String,
    );

Map<String, dynamic> _$HomeBeanToJson(HomeBean instance) => <String, dynamic>{
      'ImageUrl': instance.ImageUrl,
      'linkUrl': instance.linkUrl,
    };

FoodBean _$FoodBeanFromJson(Map<String, dynamic> json) => FoodBean(
      ImageUrl: json['ImageUrl'] as String,
      linkUrl: json['linkUrl'] as String,
    );

Map<String, dynamic> _$FoodBeanToJson(FoodBean instance) => <String, dynamic>{
      'ImageUrl': instance.ImageUrl,
      'linkUrl': instance.linkUrl,
    };

TravelBean _$TravelBeanFromJson(Map<String, dynamic> json) => TravelBean(
      ImageUrl: json['ImageUrl'] as String,
      linkUrl: json['linkUrl'] as String,
    );

Map<String, dynamic> _$TravelBeanToJson(TravelBean instance) =>
    <String, dynamic>{
      'ImageUrl': instance.ImageUrl,
      'linkUrl': instance.linkUrl,
    };
