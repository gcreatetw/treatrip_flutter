import 'app_response.dart';

typedef AppResponseCallback = void Function(AppResponse e);

class AppCallback<T> {
  final AppResponseCallback onError;
  final void Function(T r) onSuccess;

  AppCallback({
    required this.onError,
    required this.onSuccess,
  });
}
