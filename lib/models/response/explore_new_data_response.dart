import 'package:json_annotation/json_annotation.dart';

part 'explore_new_data_response.g.dart';

class ExploreNewDataResponse {
  List<GetExploreDataResponse>? exploreDataList;

  ExploreNewDataResponse({required this.exploreDataList});
}

@JsonSerializable()
class GetExploreDataResponse {
  int Id;
  String Name;
  String ImageUrl;
  List<ChildrenBean> Children;

  GetExploreDataResponse({
    required this.Id,
    required this.Name,
    required this.ImageUrl,
    required this.Children,
  });

  factory GetExploreDataResponse.fromJson(Map<String, dynamic> json) =>
      _$GetExploreDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetExploreDataResponseToJson(this);
}

@JsonSerializable()
class ChildrenBean {
  int Id;
  String? Name;

  ChildrenBean({
    required this.Id,
    required this.Name,
  });

  factory ChildrenBean.fromJson(Map<String, dynamic> json) =>
      _$ChildrenBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ChildrenBeanToJson(this);
}
