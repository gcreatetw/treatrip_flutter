import 'package:json_annotation/json_annotation.dart';

part 'explore_article_response.g.dart';

class ExploreArticleResponse {
  List<GetExploreArticleResponse> exploreArticleList;

  ExploreArticleResponse({required this.exploreArticleList});
}

@JsonSerializable()
class GetExploreArticleResponse {
  int? Id;
  String? Title;
  String? ImageUrl;
  String? linkUrl;

  GetExploreArticleResponse({
    required this.Id,
    required this.Title,
    required this.ImageUrl,
    required this.linkUrl,
  });

  factory GetExploreArticleResponse.fromJson(Map<String, dynamic> json) =>
      _$GetExploreArticleResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetExploreArticleResponseToJson(this);
}
