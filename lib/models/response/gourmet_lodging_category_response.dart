import 'package:json_annotation/json_annotation.dart';

part 'gourmet_lodging_category_response.g.dart';

class GourmetLodgingCategoryResponse {
  List<GetGourmetLodgingCategoryResponse> gourmetCategoryList;

  GourmetLodgingCategoryResponse({required this.gourmetCategoryList});
}

@JsonSerializable()
class GetGourmetLodgingCategoryResponse {
  int Id;
  String Name;
  String ImageUrl;
  //List<ChildrenBean> Children;

  GetGourmetLodgingCategoryResponse({
    required this.Id,
    required this.Name,
    required this.ImageUrl,
    //required this.Children,
  });

  factory GetGourmetLodgingCategoryResponse.fromJson(
          Map<String, dynamic> json) =>
      _$GetGourmetLodgingCategoryResponseFromJson(json);

  Map<String, dynamic> toJson() =>
      _$GetGourmetLodgingCategoryResponseToJson(this);
}
