import 'package:json_annotation/json_annotation.dart';

part 'banner_data_response.g.dart';

@JsonSerializable()
class BannerDataResponse {
  int error;
  BannerDataBean data;

  BannerDataResponse({
    required this.error,
    required this.data,
  });

  factory BannerDataResponse.fromJson(Map<String, dynamic> json) =>
      _$BannerDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BannerDataResponseToJson(this);
}

@JsonSerializable()
class BannerDataBean {
  String bannerImageUrl;
  String bannerTitle;
  String bannerSubTitle;
  List<Schedule> schedule;

  BannerDataBean({
    required this.bannerImageUrl,
    required this.bannerTitle,
    required this.bannerSubTitle,
    required this.schedule,
  });

  factory BannerDataBean.fromJson(Map<String, dynamic> json) =>
      _$BannerDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$BannerDataBeanToJson(this);
}

@JsonSerializable()
class Schedule {
  String postId;
  String postImageUrl;
  String postTitle;
  String postDate;

  Schedule({
    required this.postId,
    required this.postImageUrl,
    required this.postTitle,
    required this.postDate,
  });

  factory Schedule.fromJson(Map<String, dynamic> json) =>
      _$ScheduleFromJson(json);

  Map<String, dynamic> toJson() => _$ScheduleToJson(this);
}
