//全台美食單一內容頁
import 'package:json_annotation/json_annotation.dart';
part 'gourmet_lodging_detail_response.g.dart';

@JsonSerializable()
class GourmetLodgingDetailResponse {
  int Id;
  String Category;
  String Title;
  String Address;
  List<ImagesDetailBean> Images;
  String Rating;
  String User_ratings_total;
  String website;
  String phone;
  List<String>? opentime;
  String post_url;

  GourmetLodgingDetailResponse({
    required this.Id,
    required this.Category,
    required this.Title,
    required this.Address,
    required this.Images,
    required this.Rating,
    required this.User_ratings_total,
    required this.website,
    required this.phone,
    required this.opentime,
    required this.post_url,
  });

  factory GourmetLodgingDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$GourmetLodgingDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GourmetLodgingDetailResponseToJson(this);
}

@JsonSerializable()
class ImagesDetailBean {
  List<String> html_attributions;
  String url;

  ImagesDetailBean({required this.html_attributions, required this.url});

  factory ImagesDetailBean.fromJson(Map<String, dynamic> json) =>
      _$ImagesDetailBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ImagesDetailBeanToJson(this);
}
