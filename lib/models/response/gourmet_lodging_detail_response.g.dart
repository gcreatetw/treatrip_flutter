// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gourmet_lodging_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GourmetLodgingDetailResponse _$GourmetLodgingDetailResponseFromJson(
        Map<String, dynamic> json) =>
    GourmetLodgingDetailResponse(
      Id: json['Id'] as int,
      Category: json['Category'] as String,
      Title: json['Title'] as String,
      Address: json['Address'] as String,
      Images: (json['Images'] as List<dynamic>)
          .map((e) => ImagesDetailBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      Rating: json['Rating'] as String,
      User_ratings_total: json['User_ratings_total'] as String,
      website: json['website'] as String,
      phone: json['phone'] as String,
      opentime: (json['opentime'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      post_url: json['post_url'] as String,
    );

Map<String, dynamic> _$GourmetLodgingDetailResponseToJson(
        GourmetLodgingDetailResponse instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Category': instance.Category,
      'Title': instance.Title,
      'Address': instance.Address,
      'Images': instance.Images,
      'Rating': instance.Rating,
      'User_ratings_total': instance.User_ratings_total,
      'website': instance.website,
      'phone': instance.phone,
      'opentime': instance.opentime,
      'post_url': instance.post_url,
    };

ImagesDetailBean _$ImagesDetailBeanFromJson(Map<String, dynamic> json) =>
    ImagesDetailBean(
      html_attributions: (json['html_attributions'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      url: json['url'] as String,
    );

Map<String, dynamic> _$ImagesDetailBeanToJson(ImagesDetailBean instance) =>
    <String, dynamic>{
      'html_attributions': instance.html_attributions,
      'url': instance.url,
    };
