import 'dart:convert';
import 'package:json_annotation/json_annotation.dart';

part 'app_response.g.dart';

const int cancelCode = 1000;

@JsonSerializable()
class AppResponse {
  @JsonKey(ignore: true)
  int? statusCode;
  String? message;

  AppResponse({
    this.statusCode = 0,
    this.message = '',
  });

  factory AppResponse.success() => AppResponse(
        statusCode: 200,
        message: '成功',
      );

  factory AppResponse.cancel() => AppResponse(
        statusCode: cancelCode,
        message: '取消',
      );

  factory AppResponse.unknown() => AppResponse(
        statusCode: 999,
        message: '未知錯誤',
      );

  factory AppResponse.fromJson(Map<String, dynamic> json) =>
      _$AppResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AppResponseToJson(this);

  factory AppResponse.fromRawJson(String str) => AppResponse.fromJson(
        json.decode(str) as Map<String, dynamic>,
      );

  String toRawJson() => jsonEncode(toJson());
}
