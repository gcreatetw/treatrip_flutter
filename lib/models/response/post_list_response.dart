import 'package:json_annotation/json_annotation.dart';

part 'post_list_response.g.dart';

class PostListResponse {
  List<GetPostListResponse> postList;

  PostListResponse({required this.postList});
}

@JsonSerializable()
class GetPostListResponse {
  int Id;
  String Title;
  String ImageUrl;
  String linkUrl;

  GetPostListResponse({
    required this.Id,
    required this.Title,
    required this.ImageUrl,
    required this.linkUrl,
  });

  factory GetPostListResponse.fromJson(Map<String, dynamic> json) =>
      _$GetPostListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetPostListResponseToJson(this);
}
