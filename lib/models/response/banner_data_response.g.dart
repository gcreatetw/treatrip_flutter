// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BannerDataResponse _$BannerDataResponseFromJson(Map<String, dynamic> json) =>
    BannerDataResponse(
      error: json['error'] as int,
      data: BannerDataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BannerDataResponseToJson(BannerDataResponse instance) =>
    <String, dynamic>{
      'error': instance.error,
      'data': instance.data,
    };

BannerDataBean _$BannerDataBeanFromJson(Map<String, dynamic> json) =>
    BannerDataBean(
      bannerImageUrl: json['bannerImageUrl'] as String,
      bannerTitle: json['bannerTitle'] as String,
      bannerSubTitle: json['bannerSubTitle'] as String,
      schedule: (json['schedule'] as List<dynamic>)
          .map((e) => Schedule.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BannerDataBeanToJson(BannerDataBean instance) =>
    <String, dynamic>{
      'bannerImageUrl': instance.bannerImageUrl,
      'bannerTitle': instance.bannerTitle,
      'bannerSubTitle': instance.bannerSubTitle,
      'schedule': instance.schedule,
    };

Schedule _$ScheduleFromJson(Map<String, dynamic> json) => Schedule(
      postId: json['postId'] as String,
      postImageUrl: json['postImageUrl'] as String,
      postTitle: json['postTitle'] as String,
      postDate: json['postDate'] as String,
    );

Map<String, dynamic> _$ScheduleToJson(Schedule instance) => <String, dynamic>{
      'postId': instance.postId,
      'postImageUrl': instance.postImageUrl,
      'postTitle': instance.postTitle,
      'postDate': instance.postDate,
    };
