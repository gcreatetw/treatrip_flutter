import 'package:json_annotation/json_annotation.dart';

part 'discover_taiwan_response.g.dart';

@JsonSerializable()
class DiscoverTaiwanResponse {
  int error;
  DiscoverTaiwanDataBean data;

  DiscoverTaiwanResponse({
    required this.error,
    required this.data,
  });

  factory DiscoverTaiwanResponse.fromJson(Map<String, dynamic> json) =>
      _$DiscoverTaiwanResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverTaiwanResponseToJson(this);
}
@JsonSerializable()
class DiscoverTaiwanDataBean {
  List<DiscoverTaiwanPostList> discoverTaiwanPostList_scenic_spot;

  DiscoverTaiwanDataBean({
    required this.discoverTaiwanPostList_scenic_spot,
  });

  factory DiscoverTaiwanDataBean.fromJson(Map<String, dynamic> json) =>
      _$DiscoverTaiwanDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverTaiwanDataBeanToJson(this);
}
@JsonSerializable()
class DiscoverTaiwanPostList {
  int postId;
  String postImageUrl;
  String postTitle;
  String postPageUrl;

  DiscoverTaiwanPostList({
    required this.postId,
    required this.postImageUrl,
    required this.postTitle,
    required this.postPageUrl,
  });

  factory DiscoverTaiwanPostList.fromJson(Map<String, dynamic> json) =>
      _$DiscoverTaiwanPostListFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverTaiwanPostListToJson(this);
}
