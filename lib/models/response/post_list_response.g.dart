// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetPostListResponse _$GetPostListResponseFromJson(Map<String, dynamic> json) =>
    GetPostListResponse(
      Id: json['Id'] as int,
      Title: json['Title'] as String,
      ImageUrl: json['ImageUrl'] as String,
      linkUrl: json['linkUrl'] as String,
    );

Map<String, dynamic> _$GetPostListResponseToJson(
        GetPostListResponse instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Title': instance.Title,
      'ImageUrl': instance.ImageUrl,
      'linkUrl': instance.linkUrl,
    };
