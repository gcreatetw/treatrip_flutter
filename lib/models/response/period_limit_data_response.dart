import 'package:json_annotation/json_annotation.dart';

part 'period_limit_data_response.g.dart';

class PeriodLimitListResponse {
  List<GetPeriodLimitListResponse> periodLimitList;

  PeriodLimitListResponse({required this.periodLimitList});
}

@JsonSerializable()
class GetPeriodLimitListResponse {
  int id;
  String title;
  String linkUrl;
  String ImageUrl;

  GetPeriodLimitListResponse({
    required this.id,
    required this.title,
    required this.linkUrl,
    required this.ImageUrl,
  });

  factory GetPeriodLimitListResponse.fromJson(Map<String, dynamic> json) =>
      _$GetPeriodLimitListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetPeriodLimitListResponseToJson(this);
}
