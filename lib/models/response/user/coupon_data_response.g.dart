// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCouponDataResponse _$GetCouponDataResponseFromJson(
        Map<String, dynamic> json) =>
    GetCouponDataResponse(
      title: json['title'] as String,
      content: json['content'] as String,
      ImageUrl: json['ImageUrl'] as String,
      linkUrl: json['linkUrl'] as String,
    );

Map<String, dynamic> _$GetCouponDataResponseToJson(
        GetCouponDataResponse instance) =>
    <String, dynamic>{
      'title': instance.title,
      'content': instance.content,
      'ImageUrl': instance.ImageUrl,
      'linkUrl': instance.linkUrl,
    };
