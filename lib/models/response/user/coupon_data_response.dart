import 'package:json_annotation/json_annotation.dart';

part 'coupon_data_response.g.dart';

class CouponDataResponse {
  List<GetCouponDataResponse> getCouponDataList;

  CouponDataResponse({required this.getCouponDataList});
}

@JsonSerializable()
class GetCouponDataResponse {
  String title;
  String content;
  String ImageUrl;
  String linkUrl;

  GetCouponDataResponse({
    required this.title,
    required this.content,
    required this.ImageUrl,
    required this.linkUrl,
  });

  factory GetCouponDataResponse.fromJson(Map<String, dynamic> json) =>
      _$GetCouponDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetCouponDataResponseToJson(this);
}
