import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  String? message;
  String? cookie;
  String? cookie_name;
  UserBean? user;

  LoginResponse({
    required this.message,
    required this.cookie,
    required this.cookie_name,
    required this.user,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}

@JsonSerializable()
class UserBean {
  String display_name;
  String profile_image;
  String cover_image;

  UserBean(
      {required this.display_name,
      required this.profile_image,
      required this.cover_image});

  factory UserBean.fromJson(Map<String, dynamic> json) =>
      _$UserBeanFromJson(json);

  Map<String, dynamic> toJson() => _$UserBeanToJson(this);
}
