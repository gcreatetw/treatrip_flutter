import 'package:json_annotation/json_annotation.dart';

part 'update_phone_response.g.dart';

@JsonSerializable()
class UpdatePhoneResponse {
  String message;

  UpdatePhoneResponse({required this.message});

  factory UpdatePhoneResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdatePhoneResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UpdatePhoneResponseToJson(this);
}
