// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) =>
    LoginResponse(
      message: json['message'] as String?,
      cookie: json['cookie'] as String?,
      cookie_name: json['cookie_name'] as String?,
      user: json['user'] == null
          ? null
          : UserBean.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'cookie': instance.cookie,
      'cookie_name': instance.cookie_name,
      'user': instance.user,
    };

UserBean _$UserBeanFromJson(Map<String, dynamic> json) => UserBean(
      display_name: json['display_name'] as String,
      profile_image: json['profile_image'] as String,
      cover_image: json['cover_image'] as String,
    );

Map<String, dynamic> _$UserBeanToJson(UserBean instance) => <String, dynamic>{
      'display_name': instance.display_name,
      'profile_image': instance.profile_image,
      'cover_image': instance.cover_image,
    };
