// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'explore_new_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetExploreDataResponse _$GetExploreDataResponseFromJson(
        Map<String, dynamic> json) =>
    GetExploreDataResponse(
      Id: json['Id'] as int,
      Name: json['Name'] as String,
      ImageUrl: json['ImageUrl'] as String,
      Children: (json['Children'] as List<dynamic>)
          .map((e) => ChildrenBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetExploreDataResponseToJson(
        GetExploreDataResponse instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Name': instance.Name,
      'ImageUrl': instance.ImageUrl,
      'Children': instance.Children,
    };

ChildrenBean _$ChildrenBeanFromJson(Map<String, dynamic> json) => ChildrenBean(
      Id: json['Id'] as int,
      Name: json['Name'] as String?,
    );

Map<String, dynamic> _$ChildrenBeanToJson(ChildrenBean instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Name': instance.Name,
    };
