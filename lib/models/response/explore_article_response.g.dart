// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'explore_article_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetExploreArticleResponse _$GetExploreArticleResponseFromJson(
        Map<String, dynamic> json) =>
    GetExploreArticleResponse(
      Id: json['Id'] as int?,
      Title: json['Title'] as String?,
      ImageUrl: json['ImageUrl'] as String?,
      linkUrl: json['linkUrl'] as String?,
    );

Map<String, dynamic> _$GetExploreArticleResponseToJson(
        GetExploreArticleResponse instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Title': instance.Title,
      'ImageUrl': instance.ImageUrl,
      'linkUrl': instance.linkUrl,
    };
