import 'package:json_annotation/json_annotation.dart';

part 'recommend_lodging_post.g.dart';

class RecommendLodgingListResponse {
  List<GetRecommendLodgingResponse> recommendLodgingList;

  RecommendLodgingListResponse({required this.recommendLodgingList});
}

@JsonSerializable()
class GetRecommendLodgingResponse {
  int id;
  String title;
  String linkUrl;
  String ImageUrl;

  GetRecommendLodgingResponse(
      {required this.id,
      required this.title,
      required this.linkUrl,
      required this.ImageUrl});

  factory GetRecommendLodgingResponse.fromJson(Map<String, dynamic> json) =>
      _$GetRecommendLodgingResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetRecommendLodgingResponseToJson(this);
}
