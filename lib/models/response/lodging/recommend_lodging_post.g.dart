// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recommend_lodging_post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetRecommendLodgingResponse _$GetRecommendLodgingResponseFromJson(
        Map<String, dynamic> json) =>
    GetRecommendLodgingResponse(
      id: json['id'] as int,
      title: json['title'] as String,
      linkUrl: json['linkUrl'] as String,
      ImageUrl: json['ImageUrl'] as String,
    );

Map<String, dynamic> _$GetRecommendLodgingResponseToJson(
        GetRecommendLodgingResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'linkUrl': instance.linkUrl,
      'ImageUrl': instance.ImageUrl,
    };
