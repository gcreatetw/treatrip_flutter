// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_info_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArticleInfoDataResponse _$ArticleInfoDataResponseFromJson(
        Map<String, dynamic> json) =>
    ArticleInfoDataResponse(
      error: json['error'] as int,
      data: ArticleInfoDataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ArticleInfoDataResponseToJson(
        ArticleInfoDataResponse instance) =>
    <String, dynamic>{
      'error': instance.error,
      'data': instance.data,
    };

ArticleInfoDataBean _$ArticleInfoDataBeanFromJson(Map<String, dynamic> json) =>
    ArticleInfoDataBean(
      bloggerAvatar: json['bloggerAvatar'] as String,
      bloggerName: json['bloggerName'] as String,
      bloggerFollowCount: json['bloggerFollowCount'] as int,
      postImageUrl: json['postImageUrl'] as String,
      postTitle: json['postTitle'] as String,
      postDate: json['postDate'] as String,
      postPapularityCount: json['postPapularityCount'] as String,
      postContent: json['postContent'] as String,
    );

Map<String, dynamic> _$ArticleInfoDataBeanToJson(
        ArticleInfoDataBean instance) =>
    <String, dynamic>{
      'bloggerAvatar': instance.bloggerAvatar,
      'bloggerName': instance.bloggerName,
      'bloggerFollowCount': instance.bloggerFollowCount,
      'postImageUrl': instance.postImageUrl,
      'postTitle': instance.postTitle,
      'postDate': instance.postDate,
      'postPapularityCount': instance.postPapularityCount,
      'postContent': instance.postContent,
    };
