// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gourmet_lodging_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GourmetLodgingListResponse _$GourmetLodgingListResponseFromJson(
        Map<String, dynamic> json) =>
    GourmetLodgingListResponse(
      gourmetList: (json['food'] as List<dynamic>)
          .map((e) => GourmetDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      attractionsList: (json['attractions'] as List<dynamic>)
          .map((e) => AttractionsDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      lodgingList: (json['stay'] as List<dynamic>)
          .map((e) => LodgingDataBean.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GourmetLodgingListResponseToJson(
        GourmetLodgingListResponse instance) =>
    <String, dynamic>{
      'food': instance.gourmetList,
      'attractions': instance.attractionsList,
      'stay': instance.lodgingList,
    };

GourmetDataBean _$GourmetDataBeanFromJson(Map<String, dynamic> json) =>
    GourmetDataBean(
      Id: json['Id'] as int,
      Category: json['Category'] as String,
      Title: json['Title'] as String,
      Address: json['Address'] as String,
      Images: (json['Images'] as List<dynamic>)
          .map((e) => ImagesBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      Rating: json['Rating'] as String,
      User_ratings_total: json['User_ratings_total'] as String,
    );

Map<String, dynamic> _$GourmetDataBeanToJson(GourmetDataBean instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Category': instance.Category,
      'Title': instance.Title,
      'Address': instance.Address,
      'Images': instance.Images,
      'Rating': instance.Rating,
      'User_ratings_total': instance.User_ratings_total,
    };

AttractionsDataBean _$AttractionsDataBeanFromJson(Map<String, dynamic> json) =>
    AttractionsDataBean(
      Id: json['Id'] as int,
      Category: json['Category'] as String,
      Title: json['Title'] as String,
      Address: json['Address'] as String,
      Images: (json['Images'] as List<dynamic>)
          .map((e) => ImagesBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      Rating: json['Rating'] as String,
      User_ratings_total: json['User_ratings_total'] as String,
    );

Map<String, dynamic> _$AttractionsDataBeanToJson(
        AttractionsDataBean instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Category': instance.Category,
      'Title': instance.Title,
      'Address': instance.Address,
      'Images': instance.Images,
      'Rating': instance.Rating,
      'User_ratings_total': instance.User_ratings_total,
    };

LodgingDataBean _$LodgingDataBeanFromJson(Map<String, dynamic> json) =>
    LodgingDataBean(
      Id: json['Id'] as int,
      Category: json['Category'] as String,
      Title: json['Title'] as String,
      Address: json['Address'] as String,
      Images: (json['Images'] as List<dynamic>)
          .map((e) => ImagesBean.fromJson(e as Map<String, dynamic>))
          .toList(),
      Rating: json['Rating'] as String,
      User_ratings_total: json['User_ratings_total'] as String,
    );

Map<String, dynamic> _$LodgingDataBeanToJson(LodgingDataBean instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Category': instance.Category,
      'Title': instance.Title,
      'Address': instance.Address,
      'Images': instance.Images,
      'Rating': instance.Rating,
      'User_ratings_total': instance.User_ratings_total,
    };

ImagesBean _$ImagesBeanFromJson(Map<String, dynamic> json) => ImagesBean(
      html_attributions: (json['html_attributions'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      url: json['url'] as String,
    );

Map<String, dynamic> _$ImagesBeanToJson(ImagesBean instance) =>
    <String, dynamic>{
      'html_attributions': instance.html_attributions,
      'url': instance.url,
    };
