import 'package:json_annotation/json_annotation.dart';

part 'app_version_response.g.dart';

@JsonSerializable()
class AppVersionResponse {
  @JsonKey(name: 'Android_version')
  List<String> androidVersions;
  @JsonKey(name: 'IOS_version')
  List<String> iosVersions;

  AppVersionResponse({
    required this.androidVersions,
    required this.iosVersions,
  });

  factory AppVersionResponse.fromJson(Map<String, dynamic> json) =>
      _$AppVersionResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AppVersionResponseToJson(this);
}
