import 'package:json_annotation/json_annotation.dart';

part 'article_info_data_response.g.dart';

@JsonSerializable()
class ArticleInfoDataResponse {
  int error;
  ArticleInfoDataBean data;

  ArticleInfoDataResponse({
    required this.error,
    required this.data,
  });

  factory ArticleInfoDataResponse.fromJson(Map<String, dynamic> json) =>
      _$ArticleInfoDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleInfoDataResponseToJson(this);
}

@JsonSerializable()
class ArticleInfoDataBean {
  String bloggerAvatar;
  String bloggerName;
  int bloggerFollowCount;
  String postImageUrl;
  String postTitle;
  String postDate;
  String postPapularityCount;
  String postContent;

  ArticleInfoDataBean(
      {required this.bloggerAvatar,
      required this.bloggerName,
      required this.bloggerFollowCount,
      required this.postImageUrl,
      required this.postTitle,
      required this.postDate,
      required this.postPapularityCount,
      required this.postContent});

  factory ArticleInfoDataBean.fromJson(Map<String, dynamic> json) =>
      _$ArticleInfoDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleInfoDataBeanToJson(this);
}
