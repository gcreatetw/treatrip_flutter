// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gourmet_lodging_category_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetGourmetLodgingCategoryResponse _$GetGourmetLodgingCategoryResponseFromJson(
        Map<String, dynamic> json) =>
    GetGourmetLodgingCategoryResponse(
      Id: json['Id'] as int,
      Name: json['Name'] as String,
      ImageUrl: json['ImageUrl'] as String,
    );

Map<String, dynamic> _$GetGourmetLodgingCategoryResponseToJson(
        GetGourmetLodgingCategoryResponse instance) =>
    <String, dynamic>{
      'Id': instance.Id,
      'Name': instance.Name,
      'ImageUrl': instance.ImageUrl,
    };
