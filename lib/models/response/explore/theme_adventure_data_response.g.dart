// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'theme_adventure_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetThemeAdventureDataResponse _$GetThemeAdventureDataResponseFromJson(
        Map<String, dynamic> json) =>
    GetThemeAdventureDataResponse(
      id: json['id'] as int,
      title: json['title'] as String,
      linkUrl: json['linkUrl'] as String,
      ImageUrl: json['ImageUrl'] as String,
    );

Map<String, dynamic> _$GetThemeAdventureDataResponseToJson(
        GetThemeAdventureDataResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'linkUrl': instance.linkUrl,
      'ImageUrl': instance.ImageUrl,
    };
