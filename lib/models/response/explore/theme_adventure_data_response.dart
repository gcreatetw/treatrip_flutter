import 'package:json_annotation/json_annotation.dart';

part 'theme_adventure_data_response.g.dart';

class ThemeAdventureListResponse {
  List<GetThemeAdventureDataResponse> themeAdventureList;

  ThemeAdventureListResponse({required this.themeAdventureList});
}

@JsonSerializable()
class GetThemeAdventureDataResponse {
  int id;
  String title;
  String linkUrl;
  String ImageUrl;

  GetThemeAdventureDataResponse({
    required this.id,
    required this.title,
    required this.linkUrl,
    required this.ImageUrl,
  });

  factory GetThemeAdventureDataResponse.fromJson(Map<String, dynamic> json) =>
      _$GetThemeAdventureDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetThemeAdventureDataResponseToJson(this);
}
