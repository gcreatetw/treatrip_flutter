// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'discover_taiwan_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DiscoverTaiwanResponse _$DiscoverTaiwanResponseFromJson(
        Map<String, dynamic> json) =>
    DiscoverTaiwanResponse(
      error: json['error'] as int,
      data:
          DiscoverTaiwanDataBean.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DiscoverTaiwanResponseToJson(
        DiscoverTaiwanResponse instance) =>
    <String, dynamic>{
      'error': instance.error,
      'data': instance.data,
    };

DiscoverTaiwanDataBean _$DiscoverTaiwanDataBeanFromJson(
        Map<String, dynamic> json) =>
    DiscoverTaiwanDataBean(
      discoverTaiwanPostList_scenic_spot:
          (json['discoverTaiwanPostList_scenic_spot'] as List<dynamic>)
              .map((e) =>
                  DiscoverTaiwanPostList.fromJson(e as Map<String, dynamic>))
              .toList(),
    );

Map<String, dynamic> _$DiscoverTaiwanDataBeanToJson(
        DiscoverTaiwanDataBean instance) =>
    <String, dynamic>{
      'discoverTaiwanPostList_scenic_spot':
          instance.discoverTaiwanPostList_scenic_spot,
    };

DiscoverTaiwanPostList _$DiscoverTaiwanPostListFromJson(
        Map<String, dynamic> json) =>
    DiscoverTaiwanPostList(
      postId: json['postId'] as int,
      postImageUrl: json['postImageUrl'] as String,
      postTitle: json['postTitle'] as String,
      postPageUrl: json['postPageUrl'] as String,
    );

Map<String, dynamic> _$DiscoverTaiwanPostListToJson(
        DiscoverTaiwanPostList instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'postImageUrl': instance.postImageUrl,
      'postTitle': instance.postTitle,
      'postPageUrl': instance.postPageUrl,
    };
