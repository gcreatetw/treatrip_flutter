// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'period_limit_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetPeriodLimitListResponse _$GetPeriodLimitListResponseFromJson(
        Map<String, dynamic> json) =>
    GetPeriodLimitListResponse(
      id: json['id'] as int,
      title: json['title'] as String,
      linkUrl: json['linkUrl'] as String,
      ImageUrl: json['ImageUrl'] as String,
    );

Map<String, dynamic> _$GetPeriodLimitListResponseToJson(
        GetPeriodLimitListResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'linkUrl': instance.linkUrl,
      'ImageUrl': instance.ImageUrl,
    };
