import 'package:json_annotation/json_annotation.dart';

part 'explore_data_response.g.dart';

@JsonSerializable()
class ExploreDataResponse {
  int error;
  ExploreDataBean data;

  ExploreDataResponse({required this.error, required this.data,});

  factory ExploreDataResponse.fromJson(Map<String, dynamic> json) =>
      _$ExploreDataResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ExploreDataResponseToJson(this);
}
@JsonSerializable()
class ExploreDataBean {
  List<BannerDataBean> banner;
  List<ActivityTimeInterval> activityTimeInterval;
  List<ThemeAdventure> themeAdventure;
  List<DiscoverTaiwan> discoverTaiwan;

  ExploreDataBean(
      {required this.banner,
        required this.activityTimeInterval,
        required this.themeAdventure,
        required this.discoverTaiwan,});

  factory ExploreDataBean.fromJson(Map<String, dynamic> json) =>
      _$ExploreDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$ExploreDataBeanToJson(this);
}
@JsonSerializable()
class BannerDataBean {
  int bannerId;
  String bannerTitle;
  String bannerSubTitle;
  String bannerImageUrl;

  BannerDataBean(
      {required this.bannerId,
        required this.bannerTitle,
        required this.bannerSubTitle,
        required this.bannerImageUrl,});

  factory BannerDataBean.fromJson(Map<String, dynamic> json) =>
      _$BannerDataBeanFromJson(json);

  Map<String, dynamic> toJson() => _$BannerDataBeanToJson(this);
}
@JsonSerializable()
class ActivityTimeInterval {
  int activityId;
  String activityTitle;
  String activityImageUrl;

  ActivityTimeInterval(
      {required this.activityId, required this.activityTitle, required this.activityImageUrl,});

  factory ActivityTimeInterval.fromJson(Map<String, dynamic> json) =>
      _$ActivityTimeIntervalFromJson(json);

  Map<String, dynamic> toJson() => _$ActivityTimeIntervalToJson(this);
}
@JsonSerializable()
class ThemeAdventure {
  int themeId;
  String themeTitle;
  String themeImageUrl;

  ThemeAdventure({required this.themeId, required this.themeTitle, required this.themeImageUrl,});

  factory ThemeAdventure.fromJson(Map<String, dynamic> json) =>
      _$ThemeAdventureFromJson(json);

  Map<String, dynamic> toJson() => _$ThemeAdventureToJson(this);
}
@JsonSerializable()
class DiscoverTaiwan {
  int discoverPostId;
  String discoverPostCategory;
  List<DiscoverPostTags> discoverPostTags;
  String discoverPostImageUrl;

  DiscoverTaiwan(
      {required this.discoverPostId,
        required this.discoverPostCategory,
        required this.discoverPostTags,
        required this.discoverPostImageUrl,});

  factory DiscoverTaiwan.fromJson(Map<String, dynamic> json) =>
      _$DiscoverTaiwanFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverTaiwanToJson(this);
}
@JsonSerializable()
class DiscoverPostTags {
  int term_id;
  String name;

  DiscoverPostTags({required this.term_id, required this.name,});

  factory DiscoverPostTags.fromJson(Map<String, dynamic> json) =>
      _$DiscoverPostTagsFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverPostTagsToJson(this);
}
