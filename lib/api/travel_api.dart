part of 'package:treatrip/api/app_api.dart';

class TravelApi with AppApi {
  static TravelApi? _instance;

  static TravelApi get instance {
    return _instance ??= TravelApi();
  }

  // TODO 有切縣市區
  // Future<void> getRecommendLodgingData({
  //   required GeneralCallback<RecommendLodgingResponse> callback,
  //   required int per_page,
  //   required int page,
  // }) async {
  //   try {
  //     final Response<Map<String, dynamic>> response = await dio
  //         .post('https://www.treatrip.com/wp-json/treatrip/api/v2/Hotel',
  //             options: Options(
  //               headers: {
  //                 'Secret': 'gc_vic_test',
  //               },
  //             ),
  //             queryParameters: <String, dynamic>{
  //           'per_page': per_page,
  //           'page': page,
  //         });
  //
  //     if (response.isSuccessful) {
  //       final RecommendLodgingResponse data =
  //           RecommendLodgingResponse.fromJson(response.data!);
  //       callback.onSuccess(data);
  //     } else {
  //       callback.onError(response.generalResponse);
  //     }
  //   } on DioError catch (e) {
  //     callback.onError(e.generalResponse);
  //   }
  // }

  Future<void> getRecommendLodgingData({
    required GeneralCallback<RecommendLodgingListResponse> callback,
    required int per_page,
    required int page,
  }) async {
    try {
      final Response<List<dynamic>> response = await dio
          .get('https://www.treatrip.com/wp-json/treatrip/api/v2/Hotel',
              options: Options(
                headers: {
                  'Secret': 'gc_vic_test',
                },
              ),
              queryParameters: <String, dynamic>{
            'per_page': per_page,
            'page': page,
          });
      if (response.data != null) {
        final List<GetRecommendLodgingResponse> list =
            <GetRecommendLodgingResponse>[];
        for (final dynamic element in response.data!) {
          final q = element as Map<String, dynamic>;
          list.add(GetRecommendLodgingResponse.fromJson(q));
        }

        final RecommendLodgingListResponse data =
            RecommendLodgingListResponse(recommendLodgingList: list);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
      print(e.generalResponse);
    }
  }
}
