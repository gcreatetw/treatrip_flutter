part of 'package:treatrip/api/app_api.dart';

class UserApi with AppApi {
  static UserApi? _api;

  static UserApi get instance {
    return _api ??= _instance;
  }

  static final UserApi _instance = UserApi();

  //會員註冊
  Future<void> setRegister({
    required String username,
    required String password,
    required String display_name,
    required GeneralCallback<RegisterResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _pathRegister,
        data: {
          'username': username,
          'password': password,
          'display_name': display_name,
        },
      );
      if (response.isSuccessful) {
        final RegisterResponse data = RegisterResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //會員登入
  Future<void> userLogin({
    required String username,
    required String password,
    required GeneralCallback<LoginResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _pathLogin,
        data: {
          'username': username,
          'password': password,
        },
      );
      debugPrint('userLogin');
      if (response.isSuccessful) {
        final LoginResponse data = LoginResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //重設密碼
  Future<void> resetPassword({
    required String username,
    required String new_password,
    required GeneralCallback<ResetPasswordResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        _pathResetPassword,
        data: {
          'username': username,
          'new_password': new_password,
        },
      );

      if (response.isSuccessful) {
        final ResetPasswordResponse data =
            ResetPasswordResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      print(e.response);
      callback.onError(e.generalResponse);
    }
  }

  //更新個人資料
  Future<void> updateUserData({
    required String cookie,
    required String? display_name,
    required XFile? profile_image,
    required XFile? cover_image,
    required GeneralCallback<UpdatePhoneResponse> callback,
  }) async {
    try {
      final FormData formData = FormData.fromMap(<String, dynamic>{
        'cookie': cookie,
        if (display_name != null) 'display_name': display_name,
        if (profile_image != null)
          'profile_image': await MultipartFile.fromFile(profile_image.path),
        if (cover_image != null)
          'cover_image': await MultipartFile.fromFile(cover_image.path),
      });
      final Response<Map<String, dynamic>> response = await dio.post(
        _pathUpdateName,
        data: formData,
      );
      debugPrint('updateUserData');
      if (response.isSuccessful) {
        final UpdatePhoneResponse data =
            UpdatePhoneResponse.fromJson(response.data!);
        callback.onSuccess(data);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //Coupon
  Future<void> getCouponData(
      {required GeneralCallback<CouponDataResponse> callback}) async {
    try {
      final Response<List<dynamic>> response = await dio.get(
          'https://www.treatrip.com/wp-json/treatrip/api/v2/Coupons',
          options: Options(
            headers: {
              'Secret': 'gc_vic_test',
            },
          ));
      print(response);
      if (response.data != null) {
        final List<GetCouponDataResponse> list = <GetCouponDataResponse>[];
        for (final dynamic element in response.data!) {
          final q = element as Map<String, dynamic>;
          list.add(GetCouponDataResponse.fromJson(q));
        }
        final CouponDataResponse data =
            CouponDataResponse(getCouponDataList: list);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'get Coupon error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  ///版本檢查
  Future<void> appVersion({
    required GeneralCallback<AppVersionResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.get(
        '/gc_api/app_vertion',
      );
      if (response.isSuccessful) {
        final AppVersionResponse data =
            AppVersionResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }
}
