part of 'package:treatrip/api/app_api.dart';

class ExploreApi with AppApi {
  static ExploreApi? _instance;

  static ExploreApi get instance {
    return _instance ??= ExploreApi();
  }

  Future<void> getExploreData({
    required GeneralCallback<ExploreDataResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        'https://www.treatrip.com/wp-json/treatrip/api/v2/HomePage',
        data: {
          'secret': 'gc_vic_test',
        },
      );
      if (response.isSuccessful) {
        final ExploreDataResponse data =
            ExploreDataResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //(new)Banner
  Future<void> getAllBannerData({
    required GeneralCallback<BannerResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.get(
          'https://www.treatrip.com/wp-json/treatrip/api/v2/Banner',
          options: Options(
            headers: {
              'Secret': 'gc_vic_test',
            },
          ));
      if (response.isSuccessful) {
        final BannerResponse data = BannerResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //(new)探索台灣分類清單 (已改正式)
  Future<void> getExploreNewData({
    required GeneralCallback<ExploreNewDataResponse> callback,
  }) async {
    try {
      final Response<List<dynamic>> response = await dio.get(
          'https://www.treatrip.com/wp-json/treatrip/api/v2/DiscoverTWCategory',
          options: Options(
            headers: {
              'Secret': 'gc_vic_test',
            },
          ));
      if (response.data != null) {
        final List<GetExploreDataResponse> list = <GetExploreDataResponse>[];
        for (final dynamic element in response.data!) {
          final q = element as Map<String, dynamic>;
          list.add(GetExploreDataResponse.fromJson(q));
        }
        final ExploreNewDataResponse data =
            ExploreNewDataResponse(exploreDataList: list);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //(new)探索文章列表 (已改正式)
  Future<void> getExploreArticleData({
    required List<int> terms_id,
    required int per_page,
    required int page,
    required GeneralCallback<ExploreArticleResponse> callback,
  }) async {
    try {
      final Response<List<dynamic>> response = await dio.post(
          'https://www.treatrip.com/wp-json/treatrip/api/v2/DiscoverTaiwan',
          options: Options(
            headers: {
              'Secret': 'gc_vic_test',
            },
          ),
          data: <String, dynamic>{
            'terms_id': terms_id,
            'per_page': per_page,
            'page':page,
          });
      if (response.data != null) {
        final List<GetExploreArticleResponse> list =
            <GetExploreArticleResponse>[];
        for (final dynamic element in response.data!) {
          final q = element as Map<String, dynamic>;
          list.add(GetExploreArticleResponse.fromJson(q));
        }
        final ExploreArticleResponse data =
            ExploreArticleResponse(exploreArticleList: list);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //demo文章列表
  Future<void> getPostListData({
    required List<int> terms_id,
    required int per_page,
    required GeneralCallback<PostListResponse> callback,
  }) async {
    try {
      final Response<List<dynamic>> response =
          await dio.get(_pathPostList, queryParameters: <String, dynamic>{
        'terms_id': terms_id,
        'per_page': per_page,
      });
      if (response.data != null) {
        final List<GetPostListResponse> list = <GetPostListResponse>[];
        for (final dynamic element in response.data!) {
          final q = element as Map<String, dynamic>;
          list.add(GetPostListResponse.fromJson(q));
        }
        final PostListResponse data = PostListResponse(postList: list);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //主題冒險(正式)
  Future<void> getThemeAdventureData({
    required GeneralCallback<ThemeAdventureListResponse> callback,
    required int per_page,
    required int page,
  }) async {
    try {
      final Response<List<dynamic>> response = await dio.get(
          'https://www.treatrip.com/wp-json/treatrip/api/v2/ThemeAdventure',
          options: Options(
            headers: {
              'Secret': 'gc_vic_test',
            },
          ),
          queryParameters: <String, dynamic>{
            'per_page': per_page,
            'page': page,
          });
      if (response.data != null) {
        final List<GetThemeAdventureDataResponse> list =
            <GetThemeAdventureDataResponse>[];
        for (final dynamic element in response.data!) {
          final q = element as Map<String, dynamic>;
          list.add(GetThemeAdventureDataResponse.fromJson(q));
        }

        final ThemeAdventureListResponse data =
            ThemeAdventureListResponse(themeAdventureList: list);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
      print(e.generalResponse);
    }
  }

  //期間限定(正式)
  Future<void> getPeriodLimitData({
    required GeneralCallback<PeriodLimitListResponse> callback,
    required int page,
    required int per_page,
  }) async {
    try {
      final Response<List<dynamic>> response = await dio.get(
          'https://www.treatrip.com/wp-json/treatrip/api/v2/ActivityTimeInterval',
          options: Options(
            headers: {
              'Secret': 'gc_vic_test',
            },
          ),
          queryParameters: <String, dynamic>{
            'page': page,
            'per_page': per_page,
          });
      if (response.data != null) {
        final List<GetPeriodLimitListResponse> list =
            <GetPeriodLimitListResponse>[];
        for (final dynamic element in response.data!) {
          final q = element as Map<String, dynamic>;
          list.add(GetPeriodLimitListResponse.fromJson(q));
        }

        final PeriodLimitListResponse data =
            PeriodLimitListResponse(periodLimitList: list);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
      print(e.generalResponse);
    }
  }

  Future<void> getArticleInfoData({
    required GeneralCallback<ArticleInfoDataResponse> callback,
    required int postId,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        'https://www.treatrip.com/wp-json/treatrip/api/v2/PostInfo',
        data: {
          "secret": "gc_vic_test",
          "postId": postId,
        },
      );
      if (response.isSuccessful) {
        final ArticleInfoDataResponse data =
            ArticleInfoDataResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  Future<void> getDiscoverTaiwanData({
    required GeneralCallback<DiscoverTaiwanResponse> callback,
    required List<int> termsId,
    required int postNumber,
    required int page,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        'https://www.treatrip.com/wp-json/treatrip/api/v2/DiscoverTaiwan',
        data: {
          "secret": "gc_vic_test",
          "terms_id": termsId,
          "postNumber": postNumber,
          "page": page,
        },
      );
      if (response.isSuccessful) {
        final DiscoverTaiwanResponse data =
            DiscoverTaiwanResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }
}
