part of 'package:treatrip/api/app_api.dart';

class FoodApi with AppApi {
  static FoodApi? _instance;

  static FoodApi get instance {
    return _instance ??= FoodApi();
  }
}
