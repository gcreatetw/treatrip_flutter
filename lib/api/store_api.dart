part of 'package:treatrip/api/app_api.dart';

class StoreApi with AppApi {
  static StoreApi? _instance;

  static StoreApi get instance {
    return _instance ??= StoreApi();
  }

  //全台美食列表
  Future<void> getGourmetLodgingListData({
    required int terms_id,
    required String city,
    required double lng,
    required double lat,
    required int per_page,
    required GeneralCallback<GourmetLodgingListResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
          'https://www.treatrip.com/wp-json/treatrip/api/v2/TWFoodLodging',
          options: Options(
            headers: {
              'Secret': 'gc_vic_test',
            },
          ),
          data: <String, dynamic>{
            'terms_id': terms_id,
            'city': city,
            'lng': lng,
            'lat': lat,
            'per_page': per_page,
          });
      if (response.data != null) {
        final GourmetLodgingListResponse data =
            GourmetLodgingListResponse.fromJson(response.data!);
        callback.onSuccess(data);
      } else {
        callback.onError(GeneralResponse(message: 'error'));
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }

  //全台美食單一內容頁
  Future<void> getGourmetLodgingDetailData({
    required int post_id,
    required GeneralCallback<GourmetLodgingDetailResponse> callback,
  }) async {
    try {
      final Response<Map<String, dynamic>> response = await dio.post(
        'https://www.treatrip.com/wp-json/treatrip/api/v2/TWFoodLodgingPost',
        options: Options(
          headers: {
            'Secret': 'gc_vic_test',
          },
        ),
        data: <String, dynamic>{'post_id': post_id},
      );
      if (response.isSuccessful) {
        final GourmetLodgingDetailResponse data =
            GourmetLodgingDetailResponse.fromJson(response.data!);
        callback.onSuccess(data);
        print('0722 Api success');
      } else {
        callback.onError(response.generalResponse);
      }
    } on DioError catch (e) {
      callback.onError(e.generalResponse);
    }
  }
}
