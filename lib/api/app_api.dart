import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/response/app_version_response.dart';
import 'package:treatrip/models/response/article_info_data_response.dart';
import 'package:treatrip/models/response/banner_response.dart';
import 'package:treatrip/models/response/discover_taiwan_response.dart';
import 'package:treatrip/models/response/explore_article_response.dart';
import 'package:treatrip/models/response/explore_data_response.dart';
import 'package:treatrip/models/response/explore_new_data_response.dart';
import 'package:treatrip/models/response/general_response.dart';
import 'package:treatrip/models/response/gourmet_lodging_detail_response.dart';
import 'package:treatrip/models/response/gourmet_lodging_list_response.dart';
import 'package:treatrip/models/response/period_limit_data_response.dart';
import 'package:treatrip/models/response/post_list_response.dart';
import 'package:treatrip/models/response/user/login_response.dart';
import 'package:treatrip/models/response/user/register_response.dart';
import 'package:treatrip/models/response/user/reset_password_response.dart';
import 'package:treatrip/models/response/user/update_phone_response.dart';

import '../models/data/constants.dart';
import '../models/response/explore/theme_adventure_data_response.dart';
import '../models/response/lodging/recommend_lodging_post.dart';
import '../models/response/user/coupon_data_response.dart';
import '../utils/preferences.dart';

part 'package:treatrip/api/explore_api.dart';
part 'package:treatrip/api/food_api.dart';
part 'package:treatrip/api/travel_api.dart';
part 'package:treatrip/api/store_api.dart';
part 'package:treatrip/api/user_api.dart';

mixin AppApi {
  static const String _host = 'www.treatrip.com/wp-json';
  static Dio? _dio;

  Dio get dio {
    return _dio ??= _instance;
  }

  static final Dio _instance = Dio(
    BaseOptions(
      headers: {
        'Secret': 'gc_vic_test',
      },
      baseUrl: 'https://$_host',
      connectTimeout: 10000,
      receiveTimeout: 10000,
    ),
  );

  final String _pathVersionTwo = '/treatrip/api/v2';
  final String _pathWordPress = '/wp/v2';

  String get _pathBanner => '$_pathVersionTwo/Banner';
  String get _pathExplore => '$_pathVersionTwo/DiscoverTWCategory';
  String get _pathExploreArticle => '$_pathVersionTwo/DiscoverTaiwan';
  String get _pathArticleCollect => '$_pathWordPress/posts';
  String get _pathGourmetLodgingList => '$_pathVersionTwo/TWFoodLodging';
  String get _pathGourmetLodgingDetail => '$_pathVersionTwo/TWFoodLodgingPost';
  String get _pathGourmetDetail => '$_pathVersionTwo/TWFoodLodgingPost';
  String get _pathPostList => '$_pathVersionTwo/PostList';

  //user path
  String get _pathRegister => '/gc/user/api/register';
  String get _pathLogin => '/gc/user/api/login';
  String get _pathResetPassword => '/gc/user/api/reset_password';
  String get _pathUpdateName => '/gc/user/api/update';
}

extension ResponseExtension on Response<Map<String, dynamic>> {
  bool get isSuccessful => data != null;

  GeneralResponse get generalResponse => data != null
      ? GeneralResponse.fromJson(data!)
      : GeneralResponse(
          error: 0,
        );
}

extension DioErrorI18nExtension on DioError {
  GeneralResponse get generalResponse {
    switch (type) {
      case DioErrorType.other:
        return GeneralResponse(error: 1, message: '無網路連線');
      case DioErrorType.connectTimeout:
      case DioErrorType.receiveTimeout:
      case DioErrorType.sendTimeout:
        return GeneralResponse(error: 1, message: '連線逾時請稍後再試');
      case DioErrorType.response:
        if (response != null && response!.data is Map<String, dynamic>) {
          final Map<String, dynamic> json =
              response!.data as Map<String, dynamic>;
          final GeneralResponse data = GeneralResponse.fromJson(json);
          return data;
        } else {
          return GeneralResponse(error: 1, message: message);
        }
      case DioErrorType.cancel:
      default:
        return GeneralResponse(error: 1, message: '取消');
    }
  }
}
