part of 'resources.dart';

class AppButtonStyle {
  AppButtonStyle();

  ButtonStyle get login {
    return ElevatedButton.styleFrom(
      primary: AppColors.white,
      elevation: 5.0,
      padding: const EdgeInsets.all(12.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100.0),
      ),
    );
  }

  ButtonStyle get circularSelected {
    return ElevatedButton.styleFrom(
      primary: AppColors.themeColor,
      elevation: 0,
      padding: const EdgeInsets.all(8.0),
      shape: RoundedRectangleBorder(
        side: const BorderSide(color: AppColors.themeColor),
        borderRadius: BorderRadius.circular(100.0),
      ),
    );
  }

  //編輯用戶資訊
  ButtonStyle get editUserInfo {
    return ElevatedButton.styleFrom(
      primary: AppColors.editUser,
      elevation: 0,
      padding: const EdgeInsets.all(16.0),
      shape: RoundedRectangleBorder(
        side: const BorderSide(color: AppColors.transparent),
        borderRadius: BorderRadius.circular(6.0),
      ),
    );
  }

  static ButtonStyle circularPrimary() {
    return ElevatedButton.styleFrom(
      primary: AppColors.themeColor,
      onPrimary: AppColors.black,
      elevation: 0,
      onSurface: AppColors.themeColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100.0),
      ),
    );
  }

  static ButtonStyle circularOutline() {
    return ElevatedButton.styleFrom(
      primary: AppColors.white,
      elevation: 0,
      side: const BorderSide(
        color: AppColors.themeColor,
      ),
      onSurface: AppColors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100.0),
      ),
    );
  }

  static ButtonStyle radius({
    Color? primary,
    Color? onSurface,
  }) {
    return ElevatedButton.styleFrom(
      primary: primary ?? AppColors.themeColor,
      elevation: 0,
      onSurface: onSurface ?? primary?.withOpacity(0.5) ?? AppColors.themeColor,
      minimumSize: const Size(double.infinity, 44.0),
      padding: const EdgeInsets.symmetric(
        vertical: 12.0,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      textStyle: const TextStyle(
        color: Colors.white,
        fontSize: 14.0,
        fontFamily: 'NotoSansCJKtc',
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static ButtonStyle radiusOutline({
    Color? primary,
    Color? onSurface,
  }) {
    return ElevatedButton.styleFrom(
      primary: primary ?? AppColors.themeColor,
      elevation: 0,
      onSurface: onSurface ?? primary?.withOpacity(0.5) ?? AppColors.themeColor,
      minimumSize: const Size(double.infinity, 44.0),
      padding: const EdgeInsets.symmetric(
        vertical: 12.0,
      ),
      side: BorderSide(
        color: onSurface ?? AppColors.themeColor,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      textStyle: const TextStyle(
        color: Colors.white,
        fontSize: 14.0,
        fontFamily: 'NotoSansCJKtc',
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static ButtonStyle radiusWhite() {
    return ElevatedButton.styleFrom(
      primary: AppColors.white,
      elevation: 0,
      onSurface: AppColors.white12,
      padding: const EdgeInsets.only(
        top: 12.0,
        bottom: 12.0,
        left: 24.0,
        right: 16.0,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(36.0),
      ),
      textStyle: const TextStyle(
        color: AppColors.themeColor,
        fontSize: 14.0,
        fontFamily: 'NotoSansCJKtc',
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static ButtonStyle bottomButton(
    BuildContext context, {
    Color? primary,
    Color? onSurface,
  }) {
    return ElevatedButton.styleFrom(
      primary: primary ?? AppColors.themeColor,
      onSurface: primary ?? AppColors.themeColor,
      elevation: 0,
      minimumSize: const Size(double.infinity, 56.0),
      padding: EdgeInsets.only(
        top: 16.0,
        bottom: (MediaQuery.of(context).viewPadding.bottom == 0 ? 16.0 : 8.0) +
            MediaQuery.of(context).viewPadding.bottom,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
      ),
      textStyle: const TextStyle(
        color: AppColors.white,
      ),
    );
  }

  static ButtonStyle red() {
    return ButtonStyle(
      shape: MaterialStateProperty.resolveWith<OutlinedBorder?>(
        (Set<MaterialState> states) {
          return RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(26.0),
          );
        },
      ),
      foregroundColor: MaterialStateProperty.all<Color>(AppColors.white),
      backgroundColor: MaterialStateProperty.resolveWith<Color?>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.pressed)) {
            return AppColors.themeColor;
          } else if (states.contains(MaterialState.disabled)) {
            return AppColors.themeColor;
          }
          return AppColors.themeColor;
        },
      ),
    );
  }
}
