part of 'resources.dart';

class AppTextStyle {
  AppTextStyle._();

  static OutlineInputBorder get inputDefBorder {
    return const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(6.0)),
      borderSide: BorderSide(color: AppColors.themeColor, width: 1),
    );
  }

  static TextStyle getDefStyle({
    Color color = AppColors.black38,
    double fontSize = 14.0,
  }) {
    return TextStyle(
      color: color,
      fontSize: fontSize,
      fontFamily: 'NotoSansCJKtc',
      fontWeight: FontWeight.w700,
    );
  }

  static TextStyle getPinCode({
    Color color = AppColors.black38,
    double fontSize = 16.0,
    String fontFamily = 'PingFangTC',
    FontWeight fontWeight = FontWeight.w500,
  }) {
    return TextStyle(
      color: color,
      fontSize: fontSize,
      fontFamily: fontFamily,
      fontWeight: fontWeight,
    );
  }

  static const TextStyle subTitle = TextStyle(
    color: AppColors.black38,
    fontSize: 13,
    fontFamily: 'NotoSansCJKtc',
  );

  static const TextStyle dialogTitle = TextStyle(
    color: AppColors.black87,
    fontSize: 16,
    fontFamily: 'NotoSansCJKtc',
    fontWeight: FontWeight.w500,
  );

  static const TextStyle dialogContent = TextStyle(
    color: AppColors.black54,
    fontSize: 14,
    fontFamily: 'NotoSansCJKtc',
    fontWeight: FontWeight.w500,
  );
}
