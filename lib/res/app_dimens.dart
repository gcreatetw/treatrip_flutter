part of 'resources.dart';

class AppDimens {
  static const double screenPadding = 16.0;
  static const double buttonMargin = 8.0;
  static const double generateMargin = 8.0;
  static const double largeMargin = 24.0;

  static const double inputPadding = 12.0;

  static const double fontContent = 14.0;
  static const double fontNormal = 15.0;
  static const double fontErrorHint = 12.0;
  static const double fontHint = 14.0;
  static const double fontTitle = 16.0;
  static const double fontTitleSmall = 13.0;
  static const double fontTip = 13.0;
}
