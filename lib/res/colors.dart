part of 'resources.dart';

class AppColors {
  AppColors._();
  static const Color themeColor = Color(0xffed7465);
  static const Color pinCodeColor = Color(0xffFBDFDC);
  static const Color nameFieldColor = Color(0xfffdefee);
  static const Color themeColorFix = Color(0xffed7466);
  static const Color starColor = Color(0xffe7711b);
  static const Color titleBlue = Color(0xff4293ee);
  static const Color lightCoral = Color(0xfffefafa);

  static const Color transparent = Colors.transparent;
  static const Color white0 = Color(0x00ffffff);
  static const Color white = Colors.white;
  static const Color white12 = Color(0x1Fffffff);
  static const Color white38 = Color(0x61ffffff);
  static const Color white50 = Color(0x7fffffff);
  static const Color white70 = Color(0xb3ffffff);
  static const Color black = Colors.black;
  static const Color black16 = Color(0x28000000);
  static const Color black26 = Color(0x42000000);
  static const Color black30 = Color(0x4c000000);
  static const Color black38 = Color(0x61000000);
  static const Color black50 = Color(0x80000000);
  static const Color black54 = Color(0x8a000000);
  static const Color black87 = Color(0xDE000000);
  static const Color editUser = Color(0xfff0f0f0);
  static const Color greyDishBrown12 = Color(0x1f474747);

  static const Color softBlue = Color(0xff5990fa);
  static const Color tomato = Color(0xffea4335);
}
