import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:provider/src/provider.dart';
import 'package:treatrip/provider/app_provider.dart';
import 'package:treatrip/provider/user_page_provider.dart';
import 'package:treatrip/provider/user_provider.dart';
import 'package:treatrip/res/resources.dart';
import 'package:treatrip/ui/welcome_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:treatrip/utils/firebase_analytics_utils.dart';
import 'package:treatrip/utils/firebase_message_utils.dart';
import 'package:treatrip/utils/preferences.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:treatrip/utils/time_ago_custom_messages.dart';

import 'firebase_options.dart';
import 'models/data/constants.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preferences.init(key: Constants.key, iv: Constants.iv);
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  timeago.setLocaleMessages('zh-TW', CustomMessages());
  timeago.setLocaleMessages('en-US', timeago.EnMessages());

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FirebaseMessagingUtils.handleBackground();

  if (kDebugMode) {
  } else {
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  }

  // await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
  //   alert: true,
  //   badge: true,
  //   sound: true,
  // );

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  UserProvider userProvider = UserProvider();
  UserPageProvider userPageData = UserPageProvider();
  AppProvider appProvider = AppProvider();

  @override
  void initState() {
    Future<void>.microtask(() {
      userProvider.init();
      FirebaseMessagingUtils.instance.init(
        context,
        onNotificationClick: (RemoteMessage message) {},
      );
      if (userProvider.isLogin) {
        userPageData.fetchUserData(
          phone: Preferences.getString(Constants.phone, ''),
          password: Preferences.getString(Constants.password, ''),
        );
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //去除半透明狀態欄遮罩
    if (Theme.of(context).platform == TargetPlatform.android) {
      SystemUiOverlayStyle _style =
          const SystemUiOverlayStyle(statusBarColor: Colors.transparent);
      SystemChrome.setSystemUIOverlayStyle(_style);
    }

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>.value(value: userProvider),
        ChangeNotifierProvider<AppProvider>.value(value: appProvider),
        ChangeNotifierProvider<UserPageProvider>.value(value: userPageData),
      ],
      child: MaterialApp(
        title: 'TreaTrip',
        theme: ThemeData(
          appBarTheme: const AppBarTheme(
            systemOverlayStyle: SystemUiOverlayStyle.dark,
            backgroundColor: Colors.transparent,
            centerTitle: false,
            elevation: 0,
            titleTextStyle: TextStyle(
              color: AppColors.themeColor,
              fontSize: 24.0,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w700,
            ),
          ),
          primaryColor: AppColors.themeColor,
          unselectedWidgetColor: AppColors.white,
          colorScheme:
              ThemeData().colorScheme.copyWith(secondary: AppColors.themeColor),
          bottomNavigationBarTheme:
              const BottomNavigationBarThemeData(backgroundColor: Colors.white),
        ),
        onGenerateRoute: (RouteSettings settings) {
          switch (settings.name) {
          }
        },
        // navigatorObservers: <NavigatorObserver>[
        //   if (FirebaseAnalyticsUtils.isSupported)
        //     FirebaseAnalyticsObserver(
        //       analytics: FirebaseAnalyticsUtils.instance.analytics!,
        //     ),
        // ],
        debugShowCheckedModeBanner: false,
        // 去除右上debug標誌
        home: const WelcomePage(),
      ),
    );
  }
}
