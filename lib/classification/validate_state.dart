import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:treatrip/res/resources.dart';

enum ValidateState {
  empty,
  processing,
  done,
}

extension ValidateStateEx on ValidateState {
  String get title {
    switch (this) {
      case ValidateState.empty:
        return '開始驗證';
      case ValidateState.processing:
        return '驗證中';
      case ValidateState.done:
        return '已驗證';
    }
  }

  Color get color {
    switch (this) {
      case ValidateState.empty:
        return AppColors.themeColor;
      case ValidateState.processing:
        return Colors.white;
      case ValidateState.done:
        return Colors.amber;
    }
  }
}
