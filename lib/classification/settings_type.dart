enum SettingsType {
  signUp,
  signUpWith3Party,
  settingsPassword,
  resetPassword,
  forgetPassword,
  settingsProfile,
  settingsNickName,
}
