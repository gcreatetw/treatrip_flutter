import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class FirebaseMessagingUtils {
  static FirebaseMessagingUtils? _instance;

  static FirebaseMessagingUtils get instance {
    return _instance ??= FirebaseMessagingUtils();
  }

  static bool get isSupported =>
      (kIsWeb || Platform.isAndroid || Platform.isIOS) &&
      FirebaseMessaging.instance.isSupported();

  FirebaseMessagingUtils() {
    if (isSupported) {
      messaging = FirebaseMessaging.instance;
    }
  }

  FirebaseMessaging? messaging;

  Future<void> init(
    BuildContext context, {
    Function(RemoteMessage)? onNotificationClick,
    Function(RemoteMessage)? onForegroundMessage,
  }) async {
    print(FirebaseMessagingUtils.isSupported);
    if (!FirebaseMessagingUtils.isSupported) return;
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      if (message.notification != null) {
        debugPrint('onMessage: $message');
        print('onMessage == $message');
      }
      print('onMessage == ??');
      onForegroundMessage?.call(message);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      debugPrint('onMessageOpenedApp: $message');
      await navigateToItemDetail(message, onNotificationClick);
      onNotificationClick?.call(message);
    });

    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {
      debugPrint('onBackgroundMessage: $message');
      await navigateToItemDetail(message, onNotificationClick);
      onNotificationClick?.call(message);
    });

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) async {
      if (message != null) {
        await navigateToItemDetail(
          message,
          onNotificationClick,
        );
      }
    });
    messaging?.requestPermission();
    getToken();
  }

  static void handleBackground() {
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  }

  static Future<void> _firebaseMessagingBackgroundHandler(
    RemoteMessage message,
  ) async {
    // If you're going to use other Firebase services in the background,
    // such as Firestore,
    // make sure you call `initializeApp` before using other Firebase services.
    await Firebase.initializeApp();
    if (kDebugMode) {
      debugPrint('Handling a background message: ${message.messageId}');
    }
  }

  Future<String?> getToken() async {
    final String? token = await messaging?.getToken();
    if (token != null) {
      if (kDebugMode) debugPrint('Push Messaging token: $token');
    }
    //print(token);
    return token;
  }

  Future<void> navigateToItemDetail(
    RemoteMessage message,
    Function(RemoteMessage)? onClick,
  ) async {
    onClick?.call(message);
  }
}
