import 'dart:convert';
import 'dart:math';

import 'package:treatrip/utils/validator.dart';

class StringUtils {
  static const String _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';

  String parsePhone(String phone) {
    if (!Validator().isPhone(phone)) {
      throw 'Format of phone number not validate';
    }
    return phone.replaceFirst('09', '+8869');
  }

  static String randomString(int len) {
    final Random r = Random(DateTime.now().microsecondsSinceEpoch);
    final List<int> values = List<int>.generate(len, (int i) => r.nextInt(255));
    return base64UrlEncode(values);
  }
}
