import 'package:timeago/timeago.dart';

/// Chinese messages
class CustomMessages implements LookupMessages {
  @override
  String prefixAgo() => '';

  @override
  String prefixFromNow() => '';

  @override
  String suffixAgo() => '前';

  @override
  String suffixFromNow() => '後';

  @override
  String lessThanOneMinute(int seconds) => '少於1分鐘';

  @override
  String aboutAMinute(int minutes) => '約1分鐘';

  @override
  String minutes(int minutes) => '$minutes分鐘';

  @override
  String aboutAnHour(int minutes) => '1小時';

  @override
  String hours(int hours) => '$hours小時';

  @override
  String aDay(int hours) => '1天';

  @override
  String days(int days) => '$days天';

  @override
  String aboutAMonth(int days) => '一個月';

  @override
  String months(int months) => '${months.chineseNumber}個月';

  @override
  String aboutAYear(int year) => '一年';

  @override
  String years(int years) => '${years.chineseNumber}年';

  @override
  String wordSeparator() => '';
}

extension ChineseNumberEx on int {
  String get chineseNumber {
    switch (this) {
      case 1:
        return '一';
      case 2:
        return '兩';
      case 3:
        return '三';
      case 4:
        return '四';
      case 5:
        return '五';
      case 6:
        return '六';
      case 7:
        return '七';
      case 8:
        return '八';
      case 9:
        return '九';
      case 10:
        return '十';
      default:
        return toString();
    }
  }
}
