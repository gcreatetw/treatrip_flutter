class Validator {
  bool isEmail(String? email) {
    if (email == null || email.isEmpty) {
      return false;
    }

    const String pattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@([\w-]+\.)+[\w-]{2,4}$";
    final RegExp regExp = RegExp(pattern);

    if (!regExp.hasMatch(email)) {
      return false;
    }
    return true;
  }

  bool isPhone(String? phone) {
    if (phone == null || phone.isEmpty) {
      return false;
    }

    const String pattern = r'^((?=(09))[0-9]{10})$';
    final RegExp regExp = RegExp(pattern);

    if (!regExp.hasMatch(phone)) {
      return false;
    }
    return true;
  }
}
