import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:image_picker/image_picker.dart';

//import 'package:image_cropper/image_cropper.dart';
import 'package:treatrip/res/resources.dart';

extension _ImageSourceEx on ImageSource {
  String get text {
    switch (this) {
      case ImageSource.camera:
        return '使用相機拍攝';
      case ImageSource.gallery:
        return '從相簿挑選';
    }
  }

  IconData get icon {
    switch (this) {
      case ImageSource.camera:
        return Icons.camera;
      case ImageSource.gallery:
        return Icons.folder;
    }
  }
}

class Utils {
  static String fixHtml(String rawHtml) {
    String blogHtmlFix = rawHtml.replaceAll('\n', '<br>');
    while (blogHtmlFix.contains('[')) {
      int first = blogHtmlFix.indexOf('[');
      int last = blogHtmlFix.indexOf(']') + 1;
      String removeLine = blogHtmlFix.substring(first, last);
      blogHtmlFix = blogHtmlFix.replaceAll(removeLine, '');
    }
    return blogHtmlFix;
  }

  static String fixTitle(String rawTitle) {
    return HtmlUnescape().convert(rawTitle);
  }

  static void pickImageAndCrop(
    BuildContext context, {
    required void Function(List<XFile>) onSuccess,
    required bool isProfile,
    bool isBackground = false,
  }) {
    showModalBottomSheet(
      backgroundColor: AppColors.white,
      context: context,
      builder: (_) => ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: 90 + MediaQuery.of(context).viewInsets.bottom,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            for (ImageSource source in ImageSource.values)
              Expanded(
                  child: Center(
                child: TextButton(
                  style: TextButton.styleFrom(
                      minimumSize: Size(MediaQuery.of(context).size.width / 2,
                          double.infinity)),
                  child: Column(
                    children: <Widget>[
                      Icon(
                        source.icon,
                        color: AppColors.themeColor,
                      ),
                      const SizedBox(height: 8.0),
                      Text(
                        source.text,
                        style: const TextStyle(
                            fontSize: 16, color: AppColors.themeColor),
                      ),
                    ],
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                    final ImagePicker _picker = ImagePicker();

                    // if (isProfile) {
                    //   final XFile? pickedImage = await _picker.pickImage(
                    //       source: source, maxHeight: 1024, maxWidth: 1024);
                    //   if (pickedImage != null) {
                    //     final File? croppedFile =
                    //         await ImageCropper().cropImage(
                    //       sourcePath: pickedImage.path,
                    //       aspectRatio: isProfile && !isBackground
                    //           ? CropAspectRatio(ratioX: 1, ratioY: 1)
                    //           : null,
                    //       maxHeight: 1024,
                    //       maxWidth: 1024,
                    //       aspectRatioPresets: isProfile && !isBackground
                    //           ? <CropAspectRatioPreset>[
                    //               CropAspectRatioPreset.square
                    //             ]
                    //           : const <CropAspectRatioPreset>[
                    //               CropAspectRatioPreset.original,
                    //               CropAspectRatioPreset.square,
                    //               CropAspectRatioPreset.ratio3x2,
                    //               CropAspectRatioPreset.ratio4x3,
                    //               CropAspectRatioPreset.ratio16x9
                    //             ],
                    //       cropStyle: isProfile && !isBackground
                    //           ? CropStyle.circle
                    //           : CropStyle.rectangle,
                    //       androidUiSettings: const AndroidUiSettings(
                    //         toolbarTitle: '裁剪照片',
                    //         toolbarColor: AppColors.white,
                    //         toolbarWidgetColor: AppColors.themeColor,
                    //         initAspectRatio: CropAspectRatioPreset.original,
                    //         lockAspectRatio: false,
                    //       ),
                    //       iosUiSettings: const IOSUiSettings(
                    //         title: '裁剪照片',
                    //         doneButtonTitle: '完成',
                    //         cancelButtonTitle: '取消',
                    //       ),
                    //     );
                    //     if (croppedFile != null) {
                    //       onSuccess.call(<XFile>[XFile(croppedFile.path)]);
                    //     }
                    //   } else {
                    //     return;
                    //   }
                    //   return;
                    // }
                    if (source == ImageSource.camera) {
                      final XFile? pickedImage = await _picker.pickImage(
                        source: source,
                        maxHeight: 1600,
                        maxWidth: 1600,
                      );
                      if (pickedImage != null) {
                        onSuccess.call(<XFile>[pickedImage]);
                      }
                    } else {
                      final XFile? pickedImages = await _picker.pickImage(
                        maxHeight: 1600,
                        maxWidth: 1600,
                        source: source,
                      );
                      print(123);
                      print(pickedImages);
                      if (pickedImages != null) {
                        onSuccess.call([pickedImages]);
                      }
                    }
                  },
                ),
              ))
          ],
        ),
      ),
    );
  }
}
