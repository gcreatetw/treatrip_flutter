import 'package:geocoding/geocoding.dart' as geocoding;
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';

export 'package:geocoding/geocoding.dart';

class LocationUtil {
  static LocationUtil? _instance;

  // ignore: prefer_constructors_over_static_methods
  static LocationUtil get instance {
    return _instance ??= LocationUtil();
  }

  Location location = Location();

  Future<bool> requestPermission() async {
    final bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      return false;
    }
    PermissionStatus permission = await location.hasPermission();
    if (permission == PermissionStatus.denied) {
      permission = await location.requestPermission();
    }
    if (permission == PermissionStatus.granted ||
        permission == PermissionStatus.grantedLimited) {
      return true;
    }
    return false;
  }

  Future<LatLng?> getAddressLocation(
    String address, {
    String? locale,
  }) async {
    final List<geocoding.Location> locations =
        await geocoding.locationFromAddress(
      address,
      localeIdentifier: locale ?? 'zh_TW',
    );
    if (locations.isEmpty) {
      return null;
    } else {
      return LatLng(
        locations.first.latitude,
        locations.first.longitude,
      );
    }
  }

  Future<geocoding.Placemark?> getLocationPlace({String? locale}) async {
    final PermissionStatus permission = await location.hasPermission();
    if (permission == PermissionStatus.granted ||
        permission == PermissionStatus.grantedLimited) {
      final LocationData data = await location.getLocation();
      if (data.latitude != null && data.longitude != null) {
        final List<geocoding.Placemark> placeMarks =
            await geocoding.placemarkFromCoordinates(
          data.latitude!,
          data.longitude!,
          localeIdentifier: locale ?? 'zh_TW',
        );
        if (placeMarks.isEmpty) {
          return null;
        } else {
          return placeMarks.first;
        }
      }
    }
    return null;
  }
}
