import 'package:flutter/cupertino.dart';

import '../api/app_api.dart';
import '../models/general_callback.dart';
import '../models/response/explore_new_data_response.dart';
import '../models/response/general_response.dart';
import '../models/response/lodging/recommend_lodging_post.dart';
import '../widget/state_controller.dart';

//TODO 調整每次加載顯示數量
const int _postNumber = 5;
const int _page = 0;

class TravelBannerPageProvider extends ChangeNotifier {
  String? bannerImageUrl;
  String? title;
  String? smallTitle;
  List<ChildrenBean>? chipData;
  List<TravelArticleListData> articleList = [];

  GeneralState state = GeneralState.loading;

  int postNumber = _postNumber;
  int page = _page;

  bool canLoadMore = false;

  TravelBannerPageProvider({
    this.bannerImageUrl,
    this.title,
    this.smallTitle,
    this.chipData,
    //this.articleList,
  });

  Future<void> fetch({
    int? bannerId,
    List<ChildrenBean>? termsId,
    String? exploreTaiwanImageUrl,
    String? exploreTaiwanTitle,
    bool append = false,
  }) async {
    if (!append) {
      state = GeneralState.loading;
      articleList.clear();
      page = _page;
    } else if (canLoadMore) {
      page++;
      notifyListeners();
      canLoadMore = false;
      debugPrint('now page : $page');
    } else {
      return;
    }
    final List<int> termsIdFix = [];
    for (ChildrenBean element in termsId ?? []) {
      termsIdFix.add(element.Id);
    }
    TravelApi.instance.getRecommendLodgingData(
      page: 1,
      per_page: page,
      callback: GeneralCallback<RecommendLodgingListResponse>(
        onError: (GeneralResponse e) {
          debugPrint(e.message);
        },
        onSuccess: (RecommendLodgingListResponse r) {
          for (final element in r.recommendLodgingList) {
            articleList.add(
              TravelArticleListData(
                imageUrl: element.ImageUrl,
                id: element.id,
                title: element.title,
                postUrl: element.linkUrl,
              ),
            );
          }
          //TODO 先拿掉否則會無限加載，James 06/22
          // canLoadMore =
          //     r.postList.length == postNumber;
          state = GeneralState.finish;
          notifyListeners();
        },
      ),
    );
  }
}

class TravelArticleListData {
  final int? id;
  final String imageUrl;
  final String? title;
  final String? date;
  final String? postUrl;

  TravelArticleListData({
    this.id,
    required this.imageUrl,
    this.title,
    this.date,
    this.postUrl,
  });
}
