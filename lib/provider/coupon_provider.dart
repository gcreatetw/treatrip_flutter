import 'package:flutter/material.dart';
import 'package:treatrip/models/general_callback.dart';

import '../api/app_api.dart';
import '../models/response/user/coupon_data_response.dart';

class CouponProvider extends ChangeNotifier {
  final UserApi _api = UserApi.instance;

  List<CouponDataModel> couponList = [];

  Future<void> fetchCouponData() async {
    _api.getCouponData(
      callback: GeneralCallback(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (CouponDataResponse r) {
          for (GetCouponDataResponse element in r.getCouponDataList) {
            couponList.add(CouponDataModel(
              title: element.title,
              content: element.content,
              imageUrl: element.ImageUrl,
              linkUrl: element.linkUrl,
            ));
          }
          notifyListeners();
        },
      ),
    );
  }
}

class CouponDataModel {
  String title;
  String content;
  String imageUrl;
  String linkUrl;

  CouponDataModel({
    this.title = '',
    this.content = '',
    this.imageUrl = '',
    this.linkUrl = '',
  });
}
