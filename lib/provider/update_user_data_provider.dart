import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/utils/preferences.dart';

import '../models/response/user/update_phone_response.dart';
import '../models/user_model.dart';
import '../widget/state_controller.dart';

class UpdateUserDataProvider extends ChangeNotifier {
  final UserApi _api = UserApi.instance;
  GeneralState state = GeneralState.finish;

  //final UserModel userModel = UserModel();

  Future<void> init() async {
    state = GeneralState.finish;
    notifyListeners();
  }

  Future<void> updateData(
      {String? name, XFile? avatar, XFile? background}) async {
    state = GeneralState.loading;
    notifyListeners();
    await _api.updateUserData(
      cookie: Preferences.getString(Constants.cookie, ''),
      display_name: name,
      profile_image: avatar,
      cover_image: background,
      callback: GeneralCallback<UpdatePhoneResponse>(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (UpdatePhoneResponse r) {
          notifyListeners();
          print(r.message);
        },
      ),
    );
    state = GeneralState.finish;
    notifyListeners();
    print('0801 Api is Work?');
  }
}
