import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/response/app_version_response.dart';
import 'package:treatrip/models/response/general_response.dart';
import 'package:treatrip/utils/preferences.dart';

class AppProvider extends ChangeNotifier {
  late int currentIndex;

  bool get isLogin => Preferences.getString(Constants.cookie, '').isNotEmpty;

  AppProvider() {
    currentIndex = 0;
  }

  List<String> versions = [];

  void reLoad() {
    if (!isLogin) {
      currentIndex = 0;
    }
    notifyListeners();
  }

  Future<void> getAppVersions() async {
    await UserApi.instance.appVersion(
      callback: GeneralCallback<AppVersionResponse>(
        onError: (GeneralResponse e) {},
        onSuccess: (AppVersionResponse r) {
          versions = r.androidVersions;
          notifyListeners();
        },
      ),
    );
  }
}
