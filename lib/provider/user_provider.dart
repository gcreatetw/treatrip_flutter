import 'package:flutter/material.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/response/general_response.dart';
import 'package:treatrip/models/response/user/login_response.dart';
import 'package:treatrip/utils/preferences.dart';
import 'package:treatrip/utils/validator.dart';
import 'package:treatrip/widget/state_controller.dart';
import 'package:treatrip/api/app_api.dart';

import '../models/user_model.dart';

class UserProvider extends ChangeNotifier {
  final UserApi _api = UserApi.instance;

  GeneralState state = GeneralState.loading;
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  String? hint;
  String message = '';
  bool hasInput = false;

  bool get isLogin => Preferences.getString(Constants.cookie, '').isNotEmpty;

  void checkInput() {
    hasInput =
        phoneController.text.isNotEmpty && passwordController.text.isNotEmpty;
    notifyListeners();
  }

  UserModel userModel = UserModel();

  UserProvider() {
    userModel = UserModel(
      phone: '0912345678',
      cookie: 'cookie',
      userId: 0,
      name: '',
      avatarPhoto:
          'https://www.treatrip.com/wp-content/uploads/OICity_OICityGI_Image03.jpg',
      backgroundImage:
          'https://www.treatrip.com/wp-content/uploads/OICity_OICityGI_Image03.jpg',
      // cellPhoneNumberState: ValidateState.processing,
    );
  }

  Future<void> init() async {
    state = GeneralState.finish;
    notifyListeners();
  }

  Future<void> login(
    GeneralResponseCallback? onError,
    VoidCallback? onSuccess,
  ) async {
    final String phone = phoneController.text;
    final String password = passwordController.text;

    if (!Validator().isPhone(phone)) {
      message = '帳號格式錯誤';
      notifyListeners();
      return;
    }
    state = GeneralState.loading;
    message = '登入中';
    notifyListeners();

    _api.userLogin(
      username: phone,
      password: password,
      callback: GeneralCallback<LoginResponse>(
        onError: (GeneralResponse e) {
          state = GeneralState.finish;
          message = '帳號或是密碼有誤。';
          e.message = message;
          notifyListeners();
        },
        onSuccess: (LoginResponse response) async {
          message = 'Login Api success';
          await Preferences.setString(Constants.cookie, response.cookie!);
          await Preferences.setString(Constants.phone, phone);
          await Preferences.setString(Constants.password, password);
          userModel.cookie = response.cookie!;
          userModel.phone = phone;
          userModel.name = response.user!.display_name;
          phoneController.text = '';
          passwordController.text = '';

          state = GeneralState.finish;
          notifyListeners();
          onSuccess!.call();
          //isLogin = true;
        },
      ),
    );
  }
}
