import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/data/constants.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/utils/preferences.dart';

import '../models/response/user/login_response.dart';
import '../models/user_model.dart';
import '../widget/state_controller.dart';

class UserPageProvider extends ChangeNotifier {
  final UserModel userModel = UserModel();

  GeneralState state = GeneralState.finish;

  Future<void> init() async {
    // state = GeneralState.finish;
    // notifyListeners();
  }

  Future<void> fetchUserData(
      {required String phone, required String password}) async {
    state = GeneralState.loading;
    notifyListeners();
    await UserApi.instance.userLogin(
      username: phone,
      password: password,
      callback: GeneralCallback<LoginResponse>(
        onError: (e) async {
          debugPrint(e.message);
          await Preferences.setString(Constants.cookie, '');
          state = GeneralState.finish;
          notifyListeners();
        },
        onSuccess: (LoginResponse r) {
          userModel.name = r.user?.display_name ?? '用戶的暱稱';
          userModel.avatarPhoto = r.user?.profile_image ?? '';
          userModel.backgroundImage = r.user?.cover_image ?? '';
          state = GeneralState.finish;
          notifyListeners();
          print('0802 success');
        },
      ),
    );
    notifyListeners();
  }
}
