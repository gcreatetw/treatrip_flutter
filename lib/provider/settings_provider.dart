import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/response/general_response.dart';
import 'package:treatrip/provider/user_provider.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../classification/settings_type.dart';
import '../models/data/constants.dart';
import '../models/response/user/register_response.dart';
import '../../utils/preferences.dart';
import '../models/response/user/reset_password_response.dart';

class SettingsProvider extends ChangeNotifier {
  final UserApi _api = UserApi();
  GeneralState state = GeneralState.finish;
  SettingsType type = SettingsType.signUp;

  String? message;

  final PageController pageController = PageController();
  int pageLength = 0;
  bool isLastPage = false;

  void setPage(SettingsType type) {
    isLastPage = pageController.page?.toInt() == pageLength - 1;
    this.type = type;
    notifyListeners();
  }

  final TextEditingController passwordController = TextEditingController();
  final TextEditingController rePasswordController = TextEditingController();
  final TextEditingController nickNameController = TextEditingController();
  final FocusNode passwordFocusNode = FocusNode();
  final FocusNode rePasswordFocusNode = FocusNode();
  final FocusNode nickNameFocusNode = FocusNode();

  String _errorText = '';

  String get errorText => _errorText;
  final ValueNotifier<bool> isPasswordEmpty = ValueNotifier<bool>(true);

  void checkPasswordEmpty() {
    isPasswordEmpty.value = !(passwordController.text.isNotEmpty &&
        rePasswordController.text.isNotEmpty &&
        passwordController.text == rePasswordController.text);
  }

  bool _checkPasswordInput() {
    final bool isValid = passwordController.text == rePasswordController.text;
    if (!isValid) {
      _errorText = '密碼不符。';
      notifyListeners();
    }
    return isValid;
  }

  String _nickNameErrorText = '';

  String get nickNameErrorText => _nickNameErrorText;

  set nickNameErrorText(String text) {
    _nickNameErrorText = text;
    notifyListeners();
  }

  bool checkNickName() {
    const bool isValid = true; //TODO: 還不知道字符規則
    _nickNameErrorText = '';
    notifyListeners();
    return isValid;
  }

  void init() {
    state = GeneralState.finish;
  }

  @override
  void dispose() {
    passwordController.dispose();
    rePasswordController.dispose();
    nickNameController.dispose();
    super.dispose();
  }

  String get title {
    switch (type) {
      case SettingsType.signUp:
        return '成為會員';
      case SettingsType.settingsNickName:
      case SettingsType.settingsProfile:
        return '個人化設定';
      case SettingsType.forgetPassword:
      case SettingsType.resetPassword:
        return '重設密碼';
      case SettingsType.settingsPassword:
        return '設定密碼';
      case SettingsType.signUpWith3Party:
        return '成為會員';
    }
  }

  Future<void> doSignUp(
    BuildContext context, {
    required String account,
    VoidCallback? onSuccess,
  }) async {
    state = GeneralState.loading;
    notifyListeners();
    // Future<void>.delayed(const Duration(milliseconds: 1000), () {
    //   if (onSuccess != null) {
    //     onSuccess.call();
    //   }
    // });
    await UserApi.instance.setRegister(
      username: account,
      password: passwordController.text,
      display_name: nickNameController.text,
      callback: GeneralCallback<RegisterResponse>(
        onError: (GeneralResponse e) {
          state = GeneralState.error;
          message = e.message;
          notifyListeners();
          debugPrint(e.message);
        },
        onSuccess: (RegisterResponse r) {
          Preferences.setString(Constants.cookie, r.cookie ?? '');
          Preferences.setInt(Constants.userId, r.user_id ?? 0);
          Preferences.setString(Constants.phone, account);
          Preferences.setString(Constants.name, nickNameController.text);

          final UserProvider userProvider =
              Provider.of<UserProvider>(context, listen: false);
          userProvider.userModel.cookie = r.cookie ?? '';
          userProvider.userModel.userId = r.user_id ?? 0;
          //userProvider.isLogin = true;
          state = GeneralState.finish;
          notifyListeners();
          if (onSuccess != null) {
            onSuccess.call();
          }
        },
      ),
    );
  }

  Future<void> doForgetPassword({
    required String account,
    VoidCallback? onSuccess,
  }) async {
    if (!_checkPasswordInput()) {
      return;
    }

    state = GeneralState.loading;
    notifyListeners();

    _api.resetPassword(
      username: account,
      new_password: passwordController.text,
      callback:
          GeneralCallback<ResetPasswordResponse>(onError: (GeneralResponse e) {
        state = GeneralState.error;
        notifyListeners();
        debugPrint(e.message);
        _errorText = e.message ?? '未知錯誤';
      }, onSuccess: (ResetPasswordResponse response) async {
        if (onSuccess != null) {
          state = GeneralState.finish;
          notifyListeners();
          await Future<void>.delayed(const Duration(seconds: 3));
          onSuccess.call();
          return;
        }

        state = GeneralState.finish;
        // _errorText = response.message ?? '未知錯誤';
        notifyListeners();
      }),
    );
  }
}
