import 'package:flutter/material.dart';
import 'package:treatrip/api/app_api.dart';
import 'package:treatrip/models/general_callback.dart';
import 'package:treatrip/models/image_slider_data.dart';
import 'package:treatrip/models/response/gourmet_lodging_list_response.dart';
import 'package:treatrip/models/store_information_title_data.dart';
import 'package:treatrip/widget/state_controller.dart';

import '../models/response/lodging/recommend_lodging_post.dart';

class TravelPageData extends ChangeNotifier {
  List<String> bannerImageList;
  List<ImageSliderBlogData> blogList;
  List<ImageSliderSmallData> lodgingList;
  List<StoreInformationTitleData> hotelList;
  GeneralState state = GeneralState.loading;

  TravelPageData({
    required this.bannerImageList,
    required this.blogList,
    required this.lodgingList,
    required this.hotelList,
  });

  factory TravelPageData.init() {
    return TravelPageData(
      bannerImageList: [],
      blogList: [],
      lodgingList: [],
      hotelList: [],
    );
  }

  Future<void> fetchAll()async {
    state = GeneralState.loading;
    notifyListeners();

    await Future.wait([
      fetch(),
      fetchLodging(),
    ]);
    state = GeneralState.finish;
    notifyListeners();
  }

  Future<void> fetch() async {
    //_loadSampleData();
    fetchRecommendLodgingData();

  }

  Future<void> fetchLodging() async {
    await StoreApi.instance.getGourmetLodgingListData(
      terms_id: 0,
      city: '新竹市',
      lng: 0.0,
      lat: 0.0,
      per_page: 30,
      callback: GeneralCallback<GourmetLodgingListResponse>(
        onError: (e) {
          debugPrint(e.message);
        },
        onSuccess: (GourmetLodgingListResponse r) {
          hotelList.clear();
          for (LodgingDataBean element in r.lodgingList) {
            final List<String> list = [];
            //final e = element.Images[0];
            for (final imageElement in element.Images) {
              list.add(imageElement.url);
            }
            if (element.Category == '住宿') {
              hotelList.add(StoreInformationTitleData(
                id: element.Id,
                title: element.Title,
                address: element.Address,
                imageUrls: list,
                star: element.Rating,
                comments: element.User_ratings_total,
                price: 3,
                type: element.Category,
              ));
            }
          }

        },
      ),
    );
  }

  Future<void> fetchRecommendLodgingData() async {
    await TravelApi.instance.getRecommendLodgingData(
      per_page: 5,
      page: 1,
      callback: GeneralCallback(
        onError: (e) {
          debugPrint(e.message);
          state = GeneralState.finish;
          notifyListeners();
        },
        onSuccess: (RecommendLodgingListResponse r) {
          for (GetRecommendLodgingResponse element in r.recommendLodgingList) {
            lodgingList.add(
              ImageSliderSmallData(
                text: element.title,
                imgUrl: element.ImageUrl,
                id: element.id,
              ),
            );
          }
        },
      ),
    );
  }

  //目前沒用到 08/17
  void _loadSampleData() {
    bannerImageList.clear();
    bannerImageList.addAll(<String>[
      'https://www.treatrip.com/wp-content/uploads/%E6%B3%B0%E5%AE%89%E8%A7%80%E6%AD%A2%E6%BA%AB%E6%B3%89%E6%9C%83%E9%A4%A8-1.jpg',
      'https://www.treatrip.com/wp-content/uploads/%E6%B3%B0%E5%AE%89%E8%A7%80%E6%AD%A2%E6%BA%AB%E6%B3%89%E6%9C%83%E9%A4%A8-1.jpg',
      'https://www.treatrip.com/wp-content/uploads/%E6%B3%B0%E5%AE%89%E8%A7%80%E6%AD%A2%E6%BA%AB%E6%B3%89%E6%9C%83%E9%A4%A8-1.jpg',
      'https://www.treatrip.com/wp-content/uploads/%E6%B3%B0%E5%AE%89%E8%A7%80%E6%AD%A2%E6%BA%AB%E6%B3%89%E6%9C%83%E9%A4%A8-1.jpg',
    ]);
    blogList.clear();
    blogList.addAll(<ImageSliderBlogData>[
      ImageSliderBlogData(
        profileUrl:
            'https://www.treatrip.com/wp-content/uploads/image_bloger_Milu.jpg',
        name: '趣趣部落客˙Ronnie Morgan',
        imageUrl: 'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
        title: '水岸海景渡假旅店',
        content:
            '花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.',
        imagePaths: [
          'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1009532603-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1455867993-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760382-2029064651-g.jpg',
        ],
      ),
      ImageSliderBlogData(
        profileUrl:
            'https://www.treatrip.com/wp-content/uploads/image_bloger_Milu.jpg',
        name: '趣趣部落客˙Ronnie Morgan',
        imageUrl: 'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
        title: '慕谷慕魚生態廊道',
        content:
            '花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.',
        imagePaths: [
          'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1009532603-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1455867993-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760382-2029064651-g.jpg',
        ],
      ),
      ImageSliderBlogData(
        profileUrl:
            'https://www.treatrip.com/wp-content/uploads/image_bloger_Milu.jpg',
        name: '趣趣部落客˙Ronnie Morgan',
        imageUrl: 'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
        title: '慕谷慕魚生態廊道',
        content:
            '花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.',
        imagePaths: [
          'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1009532603-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1455867993-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760382-2029064651-g.jpg',
        ],
      ),
      ImageSliderBlogData(
        profileUrl:
            'https://www.treatrip.com/wp-content/uploads/image_bloger_Milu.jpg',
        name: '趣趣部落客˙Ronnie Morgan',
        imageUrl: 'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
        title: '慕谷慕魚生態廊道',
        content:
            '花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.',
        imagePaths: [
          'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1009532603-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1455867993-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760382-2029064651-g.jpg',
        ],
      ),
      ImageSliderBlogData(
        profileUrl:
            'https://www.treatrip.com/wp-content/uploads/image_bloger_Milu.jpg',
        name: '趣趣部落客˙Ronnie Morgan',
        imageUrl: 'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
        title: '慕谷慕魚生態廊道',
        content:
            '花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.花蓮深度知性旅遊的好夥伴~慕谷慕魚民宿位於花蓮市西南西郊區(花東縱谷最北端)提供幽默風趣的太魯閣族文史解說.',
        imagePaths: [
          'https://pic.pimg.tw/s2905074/1633760381-5675472-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1009532603-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760381-1455867993-g.jpg',
          'https://pic.pimg.tw/s2905074/1633760382-2029064651-g.jpg',
        ],
      ),
    ]);
    hotelList.clear();
    hotelList.addAll(<StoreInformationTitleData>[
      // StoreInformationTitleData(
      //     title: 'HOOTERS美式餐廳',
      //     address: '台北市松山區民生東路四段56巷3弄10號',
      //     imageUrls: <String>[
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      //     ],
      //     star: '3.8',
      //     comments: '127',
      //     price: 3,
      //     type: '義大利菜'),
      // StoreInformationTitleData(
      //     title: 'HOOTERS美式餐廳',
      //     address: '台北市松山區民生東路四段56巷3弄10號',
      //     imageUrls: <String>[
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      //     ],
      //     star: '3.8',
      //     comments: '127',
      //     price: 3,
      //     type: '義菜'),
      // StoreInformationTitleData(
      //     title: 'HOOTERS美式餐廳',
      //     address: '台北市松山區民生東路四段56巷3弄10號',
      //     imageUrls: <String>[
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy1.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy2.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy3.jpg',
      //       'https://www.treatrip.com/wp-content/uploads/taichung-dinner-delicacy4.jpg',
      //     ],
      //     star: '3.8',
      //     comments: '127',
      //     price: 3,
      //     type: '義大利菜'),
    ]);
  }
}
